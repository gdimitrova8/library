package $package$;

$imports$
import com.library.domain.message.Message;
import com.library.domain.validation.ValidationUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Generated class.
 * Template : DomainEntityRenderer.class
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class $entityName$ extends $entityParent$ {
$props$
$constructors$
$accessors$
    @Override
    public List<Message> validate() {
        List<Message> errors = new ArrayList<>();
        return errors;
    }
}
