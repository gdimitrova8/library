package code.generator.templates;

import code.generator.plugin.utils.FileUtils;
import java.io.StringWriter;
import java.util.Map;

/**
 *
 * @author gdimitrova
 */
public class ResourceTemplateRenderer {

    public final static ResourceTemplateRenderer INSTANCE = new ResourceTemplateRenderer();

    private ResourceTemplateRenderer() {
        //Singleton
    }

    public void render(String templateResource, Map<String, String> newContent,
            String destination, String entityType, String fileExtention, StringWriter writer) throws Exception {
        render(templateResource, newContent, destination, entityType.replaceAll("\\.", "/") + "." + fileExtention, writer);
    }

    public void render(String templateResource, Map<String, String> newContent,
            String destination, String file, StringWriter writer) throws Exception {
        render(templateResource, getClass(), newContent, destination, file, writer);
    }

    public void render(String templateResource, Class pkgClass, Map<String, String> newContent,
            String destination, String file, StringWriter writer) throws Exception {
        print(templateResource, pkgClass, newContent, writer);
        FileUtils.createFile(destination, file, writer);
    }

    public void print(String templateResource, Map<String, String> newContent, StringWriter writer) throws Exception {
        print(templateResource, getClass(), newContent, writer);
    }

    public void print(String templateResource, Class pkgClass, Map<String, String> newContent, StringWriter writer) throws Exception {
        String content = FileUtils.loadContent(pkgClass, templateResource, true);
        for (Map.Entry<String, String> e : newContent.entrySet()) {
            content = content.replaceAll(e.getKey(), e.getValue());
        }
        writer.write(content);
    }

}
