package code.generator.templates;

import code.generator.plugin.mojo.renderer.context.Meta;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Method;
import code.generator.plugin.mojo.renderer.context.Property;
import code.generator.templates.AbstractTemplateRenderer;
import code.generator.templates.utils.Utils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public abstract class JavaTemplateRenderer extends AbstractTemplateRenderer {

    public final static Map<String, String> COLL_TYPES = new HashMap<>() {
        {
            put(List.class.getName(), ArrayList.class.getName());
        }
    };

    protected void printImports(EntityMapping m, RenderingContext ctx) {
        printImports(m, ctx, "parent", "implements", null);
    }

    protected void printImports(EntityMapping m, RenderingContext ctx, String parentMeta, String implMeta, String propTypeMeta) {
        printImports(m, ctx, parentMeta, implMeta, propTypeMeta, s -> !s.startsWith("java.lang."));
    }

    protected void printImports(EntityMapping m, RenderingContext ctx, String parentMeta, String implMeta, String propTypeMeta, Predicate<String> filter) {
        Meta meta = m.meta;
        Set<String> types = resolveTypes(m.props.values(), ctx, propTypeMeta);
        String parent = meta.get(parentMeta);
        if (!areEqualPackages(m.entityType, parent)) {
            types.add(parent);
        }
        String implementInterface = meta.get(implMeta);
        if (!areEqualPackages(m.entityType, implementInterface)) {
            types.add(implementInterface);
        }
        Set<String> imports = types.stream().filter(t -> t != null).filter(filter)
                .map(t -> "import " + t).collect(Collectors.toSet());
        if (!imports.isEmpty()) {
            print(String.join(";\n", imports).concat(";\n"));
        }
    }

    protected boolean areEqualPackages(String entityType, String otherEntityType) {
        if (otherEntityType == null) {
            return true;
        }
        return entityType != null && getPackage(entityType).equals(getPackage(otherEntityType));
    }

    protected Set<String> resolveTypes(Collection<Property> props, RenderingContext ctx, String propTypeMeta) {
        Set<String> types = new HashSet<>();
        props.stream().forEach(p -> {
            if (p.collectionType != null) {
                types.add(p.collectionType);
                types.add(COLL_TYPES.get(p.collectionType));
            }
            String type = p.meta.get(propTypeMeta);
            if (type != null) {
                types.add(type);
                return;
            }
            EntityMapping m = ctx.mappings.get(p.type);
            types.add(m != null ? m.meta.get(propTypeMeta) : p.type);
        });
        return types;
    }

    protected void printDeclaration(Meta meta, String simpleName) {
        print("public " + getType(meta) + " " + simpleName + " {\n");
    }

    protected void printDeclaration(Meta meta, String simpleName, String parentMetaKey, String implMetaKey) {
        Boolean isAbstract = meta.getOrDefault("is.abstract", false);
        print("public " + (isAbstract ? "abstract " : "") + getType(meta) + " " + simpleName);
        String parent = meta.get(parentMetaKey);
        if (parent != null) {
            print(" extends " + Utils.getSimpleName(parent));
        }
        String implement = meta.get(implMetaKey);
        if (implement != null) {
            print(" implements " + Utils.getSimpleName(implement));
        }
        print(" {\n");
    }

    protected void printProps(Collection<Property> props) {
        props.forEach((prop) -> {
            printProp(prop);
        });
    }

    protected void printProps(Collection<Property> props, Consumer<Property> print) {
        props.forEach((prop) -> {
            print.accept(prop);
        });
    }

    protected void printProp(Property prop) {
        String propType = getSimpleName(prop);
        if (prop.collectionType != null) {
            printCollectionProp(prop, propType);
            return;
        }
        Meta meta = prop.meta;
        String mod = meta.get("modifier");
        Boolean isFinal = meta.getOrDefault("final", false);
        Boolean isStatic = meta.getOrDefault("static", false);
        printLine(offset
                + (mod != null ? mod + " " : "")
                + (isFinal ? "final " : "")
                + (isStatic ? "static " : "")
                + propType + " "
                + prop.name + ";\n");
    }

    protected void printCollectionProp(Property prop) {
        printCollectionProp(prop, getSimpleName(prop));
    }

    protected void printCollectionProp(Property p, String propType) {
        Meta meta = p.meta;
        String mod = meta.get("modifier");
        Boolean isFinal = meta.getOrDefault("final", false);
        Boolean isStatic = meta.getOrDefault("static", false);
        String collType = getSimpleName(p.collectionType);
        print(offset
                + (mod != null ? mod + " " : "")
                + (isFinal ? "final " : "")
                + (isStatic ? "static " : "")
                + collType + "<" + propType + "> " + p.name + " ");
        if (collType.equals("List")) {
            String collTypeImpl = COLL_TYPES.get(p.collectionType);
            print("= new " + getSimpleName(collTypeImpl) + "<>();\n");
            printLine();
        }
    }

    protected void printArgs(Collection<Property> props, RenderingContext ctx, String typeMeta) {
        List<String> args = props.stream().map(p -> {
            String type = p.meta.get(typeMeta);
            if (type != null) {
                if (p.collectionType != null) {
                    return getSimpleName(p.collectionType) + "<" + getSimpleName(type) + "> " + p.name;
                }
                return getSimpleName(type) + " " + p.name;
            }
            EntityMapping m = ctx.mappings.get(p.type);
            type = m != null ? m.meta.get(typeMeta) : null;
            type = type != null ? getSimpleName(type) : getSimpleName(p);
            if (p.collectionType != null) {
                return getSimpleName(p.collectionType) + "<" + type + "> " + p.name;
            }
            return type + " " + p.name;
        }).collect(Collectors.toList());
        print(String.join(", ", args));
    }

    protected void printPropsValue(Collection<Property> props) {
        List<String> values = props.stream().map(p -> p.name).collect(Collectors.toList());
        print(String.join(", ", values));
    }

    protected void printSuper(EntityMapping... parents) {
        printSuper(offset2, parents);
    }

    protected void printSuper(String offset, EntityMapping... parents) {
        if (parents.length < 1) {
            return;
        }
        List<Property> argNames = new ArrayList<>();
        for (EntityMapping parent : parents) {
            argNames.addAll(parent.props.values());
        }

        print(offset + "super(");
        printPropsValue(argNames);
        print(");\n");
    }

    protected List<EntityMapping> getParents(RenderingContext ctx, EntityMapping m) {
        List<EntityMapping> parents = new ArrayList<>();
        String parentType = m.meta.get("parent");
        EntityMapping parent = ctx.mappings.get(parentType);
        if (parentType == null || parent == null) {
            return parents;
        }
        parents.add(parent);
        parents.addAll(getParents(ctx, parent));
        return parents;
    }

    protected void printDefaultConstructor(String simpleName) {
        printLine(offset + "public " + simpleName + "(){");
        printLine(offset + "}\n");
    }

    protected void printDefaultConstructor(EntityMapping e) {
        printDefaultConstructor(getSimpleName(e));
    }

    protected void printConstructor(EntityMapping e, RenderingContext ctx, String typeMeta, EntityMapping... parents) {
        printConstructor(getSimpleName(e), ctx, typeMeta, e.props.values(), parents);
    }

    protected void printConstructor(String simpleName, RenderingContext ctx, String typeMeta, Collection<Property> props, EntityMapping... parents) {
        List<Property> args = new ArrayList<>();
        if (parents.length > 0) {
            for (EntityMapping parent : parents) {
                args.addAll(parent.props.values());
            }
        }
        args.addAll(props);

        print(offset + "public " + simpleName + "(");
        printArgs(args, ctx, typeMeta);
        print(") {\n");
        printSuper(offset2, parents);
        props.forEach((prop) -> {
            printLine(offset2 + "this." + prop.name + " = " + prop.name + ";");
        });
        printLine(offset + "}\n");
    }

    protected void printAccessors(Collection<Property> props, RenderingContext ctx) {
        props.forEach((prop) -> {
            printAccessor(prop, ctx, "simple.name");
        });
    }

    protected void printAccessors(Collection<Property> props, RenderingContext ctx, String type) {
        props.forEach((prop) -> {
            printAccessor(prop, ctx, type);
        });
    }

    protected void printAccessor(Property prop, RenderingContext ctx, String type) {
        String accessor = prop.meta.get("accessor");
        if (accessor == null) {
            return;
        }
        String name = prop.name;
        String simpleName = getPropSimpleName(prop, ctx, type);
        if (prop.collectionType != null) {
            simpleName = getSimpleName(prop.collectionType) + "<" + simpleName + ">";
        }
        switch (accessor) {
            case "setter": {
                printSetter(simpleName, name);
                break;
            }
            case "getter": {
                printGetter(simpleName, name);
                break;
            }
            case "both": {
                printBoth(simpleName, name);
                break;
            }
        }
    }

    protected void printSetter(String simpleName, String name) {
        printLine(offset + "public void set" + Utils.makeCamelName(name) + "(" + simpleName + " " + name + ") {");
        printLine(offset2 + "this." + name + " = " + name + ";");
        printLine(offset + "}\n");
    }

    protected void printGetter(String simpleName, String name) {
        printLine(offset + "public " + simpleName + " get" + Utils.makeCamelName(name) + "() {");
        printLine(offset2 + "return this." + name + ";");
        printLine(offset + "}\n");
    }

    protected void printBoth(String simpleName, String name) {
        printSetter(simpleName, name);
        printGetter(simpleName, name);
    }

    protected void printMethods(EntityMapping mapping, Consumer<Method> printMethod) {
        Collection<Property> props = mapping.props.values();
        for (Method m : mapping.methods) {
            if (m.name.equals("hashCode")) {
                printHashCode(m, props);
                return;
            }
            if (m.name.equals("equals")) {
                printEquals(getSimpleName(mapping), props);
                return;
            }
            printMethod.accept(m);
        }

    }

    protected void printHashCode(Method m, Collection<Property> props) {
        printLine(offset + "@Override");
        printLine(offset + "public int hashCode() {");
        printLine(offset2 + "int hash = " + m.meta.get("hash") + ";");
        props.forEach(p -> {
            print(offset2 + "hash = " + m.meta.get("hash.number"));
            print(" * hash + Objects.hashCode(this." + p.name + ");\n");
        });
        printLine(offset2 + "return hash;");
        printLine(offset + "}\n");
    }

    protected void printEquals(EntityMapping mapping, Collection<Property> props) {
        printEquals(getSimpleName(mapping), props);
    }

    protected void printEquals(String entitySimpleName, Collection<Property> props) {
        printEquals(entitySimpleName, props.stream().collect(Collectors.toList()));
    }

    protected void printEquals(String entitySimpleName, List<Property> props) {
        printLine(offset + "@Override");
        printLine(offset + "public boolean equals(Object obj) {");

        printLine(offset2 + "if (this == obj) {");
        printLine(offset3 + "return true;");
        printLine(offset2 + "}");

        printLine(offset2 + "if (obj == null) {");
        printLine(offset3 + "return false;");
        printLine(offset2 + "}");

        printLine(offset2 + "if (getClass() != obj.getClass()) {");
        printLine(offset3 + "return false;");
        printLine(offset2 + "}");

        printLine(offset2 + "final " + entitySimpleName + " other = (" + entitySimpleName + ") obj;");
        if (props.isEmpty()) {
            printLine(offset2 + "return super.equals(other);");
            printLine(offset + "}\n");
            return;
        }
        String name;
        Property prop;
        for (int i = 0; i < props.size() - 1; i++) {
            prop = props.get(i);
            name = prop.name;

            printLine(offset2 + "if (!Objects.equals(this." + name + ", other." + name + ")) {");
            printLine(offset3 + "return false;");
            printLine(offset2 + "}");
        }
        name = props.get(props.size() - 1).name;
        printLine(offset2 + "return Objects.equals(this." + name + ", other." + name + ");");
        printLine(offset + "}\n");
    }

    @Override
    protected void saveFile(String destination, String entityType) throws Exception {
        super.saveFile(destination, entityType.replaceAll("\\.", "/") + ".java");
    }
}
