package code.generator.templates.dto;

import code.generator.plugin.mojo.renderer.context.Meta;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Property;
import code.generator.templates.JavaTemplateRenderer;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class DtoRenderer extends JavaTemplateRenderer {

    public final static String DTO_PACKAGE = "com.library.dto";

    public final static String DTO_TYPE_META = "dto.type";

    private final static String RELATIONSHIP_META = "dto.prop.relationship";

    private final static Integer COLUMN_NAME = 0;

    private final static Integer PROP_NAME_CONSTANT = 1;

    private final static Integer COLUMN_CONSTANT = 2;

    private final static Integer MODIFIER = 3;

    private final static Integer PROP_TYPE = 4;

    private final static Integer PROP_TYPE_NAME = 5;

    private final static Integer PROP_NAME = 6;

    private Map<String, Map<Integer, String>> makeCache(RenderingContext ctx, Collection<Property> props) {
        Map<String, Map<Integer, String>> cache = new HashMap<>();
        props.forEach(p -> {
            String mod = p.meta.get("modifier");
            String columnName = p.name.replaceAll("([A-Z])", "_$1").toLowerCase();
            String propNameConstant = columnName.toUpperCase();
            String columnConstant = propNameConstant + "_COLUMN";

            String propType = getPropType(p, ctx, DTO_TYPE_META);
            String propTypeName = getSimpleName(propType);

            if (hasMeta(p, RELATIONSHIP_META)) {
                boolean isManyToOne = p.meta.get(RELATIONSHIP_META).equals("ManyToOne");
                columnName = isManyToOne ? columnName.concat("_id") : null;
            }

            Map<Integer, String> mappingCache = new HashMap<>();
            mappingCache.put(COLUMN_NAME, columnName);
            mappingCache.put(PROP_NAME_CONSTANT, propNameConstant);
            mappingCache.put(PROP_NAME, p.name);
            mappingCache.put(COLUMN_CONSTANT, columnConstant);
            mappingCache.put(PROP_TYPE, propType);
            mappingCache.put(MODIFIER, mod != null ? mod + " " : "");
            mappingCache.put(PROP_TYPE_NAME, propTypeName);
            cache.put(p.name, mappingCache);
        });
        return cache;
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!mapping.meta.getOrDefault("generate.dto", true)) {
            return;
        }
        if (mapping.meta.getOrDefault("is.class", false)) {
            printDto(ctx, mapping, destination);
            return;
        }
        if (mapping.meta.getOrDefault("is.enum", false)) {
            printEnum(mapping, destination);
        }

    }

    private void printDto(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {

        String tableName = getTableName(mapping);
        String propType = mapping.meta.get(DTO_TYPE_META);
        String dtoName = getSimpleName(propType);
        Boolean isAbstract = mapping.meta.getOrDefault("is.abstract", false);
        Collection<Property> props = mapping.props.values();
        EntityMapping[] filteredParents = filterParents(getParents(ctx, mapping));
        Map<String, Map<Integer, String>> caches = makeCache(ctx, props);

        info("\t\tGenerating Dto : " + propType);

        printLine("package " + DTO_PACKAGE + ";");
        printLine();
        printImports(props, ctx, isAbstract);
        printLine();
        printInfo(this.getClass());
        if (isAbstract) {
            printLine("@MappedSuperclass");
        } else {
            printLine("@Entity(name = \"" + tableName + "\")");
            printLine("@Table(name = \"" + tableName + "\")");
        }
        printDeclaration(mapping.meta, dtoName, "dto.parent", null);
        printLine();
        caches.values().forEach(c -> {
            printLine(offset + "public final static String " + c.get(PROP_NAME_CONSTANT) + " = \"" + c.get(PROP_NAME) + "\";");
            printLine();
            printLine(offset + "public final static String " + c.get(COLUMN_CONSTANT) + " = \"" + c.get(COLUMN_NAME) + "\";");
            printLine();
        });
        printProps(props, p -> printProperty(p, caches.get(p.name), tableName, ctx));

        printLine(offset + "public " + dtoName + "() {");
        printLine(offset2 + "// Hibernate");
        printLine(offset + "}");
        printLine();
        printConstructor(dtoName, ctx, DTO_TYPE_META, props, filteredParents);
        printAccessors(props, ctx, DTO_TYPE_META);
        printLine("}");

        saveFile(destination, propType);
    }

    private void printEnum(EntityMapping mapping, String destination) throws Exception {
        String propType = mapping.meta.get(DTO_TYPE_META);
        info("\t\tGenerating Dto enum : " + propType);
        info("");

        Collection<Property> props = mapping.props.values();
        printLine("package " + getPackage(propType) + ";\n");

        printInfo(this.getClass());
        printDeclaration(mapping.meta, getSimpleName(propType));
        printLine();
        printLine(offset + String.join(", ", props.stream().map(p -> p.name).collect(Collectors.toSet())));
        printLine();
        printLine("}");

        saveFile(destination, propType);
    }

    private void printImports(Collection<Property> props, RenderingContext ctx, Boolean isAbstract) {
        List<Property> calendarsType = filter(props, p -> isType(p, "java.util.Calendar"));
        List<Property> enumsFound = filter(props, p -> isEnum(p, ctx));
        List<Property> relFound = filter(props, p -> hasMeta(p, RELATIONSHIP_META));
        if (!calendarsType.isEmpty() || props.size() - calendarsType.size() != enumsFound.size() + relFound.size()) {
            printLine("import javax.persistence.Column;");
        }
        if (!calendarsType.isEmpty()) {
            printLine("import java.util.Calendar;");
            printLine("import javax.persistence.Basic;");
            printLine("import javax.persistence.Temporal;");
            printLine("import javax.persistence.TemporalType;");
        }
        if (!enumsFound.isEmpty()) {
            printLine("import javax.persistence.EnumType;");
            printLine("import javax.persistence.Enumerated;");
        }
        if (!relFound.isEmpty()) {
            Set<String> imports = new HashSet<>();
            for (Property p : relFound) {
                String relMeta = p.meta.get(RELATIONSHIP_META);
                if (relMeta.equals("ManyToOne")) {
                    imports.add("import javax.persistence.ManyToOne;");
                    imports.add("import javax.persistence.JoinColumn;");
                    continue;
                }
                imports.add("import javax.persistence.ManyToMany;");
            }
            printLine(String.join("\n", imports));
        }
        if (isAbstract) {
            printLine("import javax.persistence.MappedSuperclass;");
        } else {
            printLine("import javax.persistence.Entity;");
            printLine("import javax.persistence.Table;");
        }
    }

    private void printProperty(Property prop, Map<Integer, String> cache, String tableName, RenderingContext ctx) {
        Meta meta = prop.meta;
        Boolean unique = meta.getOrDefault("unique", false);
        Boolean nullable = meta.getOrDefault("nullable", false);
        if (hasMeta(prop, RELATIONSHIP_META)) {
            printRelationshipAnnotations(prop, tableName, cache, unique, nullable);
            printPropDeclaration(cache);
            return;
        }
        if (isEnum(prop, ctx)) {
            printLine(offset + "@Enumerated(EnumType.STRING)");
            printColumnAnnotation(cache, unique, nullable);
            printPropDeclaration(cache);
            return;
        }
        if (isType(prop, "java.util.Calendar")) {
            printColumnAnnotation(cache, unique, nullable);
            printLine(offset + "@Basic");
            printLine(offset + "@Temporal(TemporalType.TIMESTAMP)");
            printPropDeclaration(cache);
            return;
        }
        printColumnAnnotation(cache, unique, nullable);
        printPropDeclaration(cache);
    }

    private void printRelationshipAnnotations(Property prop, String tableName, Map<Integer, String> cache, Boolean unique, Boolean nullable) {
        String relationship = prop.meta.get(RELATIONSHIP_META);
        if (relationship.equals("ManyToMany")) {
            printLine(offset + "@ManyToMany(mappedBy = \"" + tableName + "\")");
            return;
        }
        if (!relationship.equals("ManyToOne")) {
            throw new RuntimeException("Not logic for relationship " + relationship);
        }
        printLine(offset + "@ManyToOne()");
        printLine(offset + "@JoinColumn(name = " + cache.get(COLUMN_CONSTANT) + ", unique = " + unique + ", nullable = " + nullable + ")");
    }

    private void printColumnAnnotation(Map<Integer, String> cache, Boolean unique, Boolean nullable) {
        printLine(offset + "@Column(name = " + cache.get(COLUMN_CONSTANT) + ", unique = " + unique + ", nullable = " + nullable + ")");
    }

    private void printPropDeclaration(Map<Integer, String> cache) {
        printLine(offset
                + cache.get(MODIFIER)
                + cache.get(PROP_TYPE_NAME) + " "
                + cache.get(PROP_NAME) + ";\n");
    }

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add(DTO_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    private String getTableName(EntityMapping mapping) {
        String entityName = getSimpleName(mapping.entityType);
        return mapping.meta.getOrDefault("table.name",
                entityName.replaceAll("([a-z])([A-Z])", "$1_$2").toLowerCase().concat("s")
        );
    }

}
