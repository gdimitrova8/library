package code.generator.templates;

import code.generator.plugin.mojo.renderer.AbstractRenderer;
import code.generator.plugin.mojo.renderer.context.Meta;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Method;
import code.generator.plugin.mojo.renderer.context.Property;
import code.generator.plugin.utils.FileUtils;
import code.generator.plugin.utils.SearchUtils;
import code.generator.templates.utils.Utils;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public abstract class AbstractTemplateRenderer extends AbstractRenderer {

    private final static Logger LOGGER = Logger.getLogger(AbstractTemplateRenderer.class.getName());

    protected String offset = "   ";

    protected String offset2 = offset.repeat(2);

    protected String offset3 = offset.repeat(3);

    protected void printLine() {
        printLine("");
    }

    protected void printLine(String text) {
        print(text + "\n");
    }

    protected void print(String text) {
        writer.write(text);
    }

    protected void printInfo(Class clazz) {
        printLine("/**");
        printLine(" *");
        printLine(" * Generated file. Template : " + clazz.getName() + ".java");
        printLine(" *");
        printLine(" * @author Gergana Kuleva Dimitrova");
        printLine(" *");
        printLine(" */");
    }

    @Override
    public void render(RenderingContext ctx, String destination) throws Exception {
        List<EntityMapping> mappings = filter(ctx.mappings.values());
        for (EntityMapping m : mappings) {
            this.render(ctx, m, destination);
        }
    }

    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> containsRequiredMeta(m)).collect(Collectors.toList());
    }

    protected boolean containsRequiredMeta(EntityMapping mapping) {
        return mapping.meta.keySet().containsAll(getRequiredMeta());
    }

    protected boolean containsAll(EntityMapping mapping, Set<String> metaKeys) {
        return mapping.meta.keySet().containsAll(metaKeys);
    }

    protected String getSimpleName(EntityMapping m) {
        return getSimpleName(m.meta, m.entityType);
    }

    protected String getSimpleName(Property prop) {
        return getSimpleName(prop.meta, prop.type);
    }

    protected String getSimpleName(Meta meta, String type) {
        return meta.getOrDefault("simple.name", Utils.getSimpleName(type));
    }

    protected String getSimpleName(String type) {
        return Utils.getSimpleName(type);
    }

    protected String getSimpleName(Meta meta, String metaKey, String type) {
        return meta.getOrDefault(metaKey, Utils.getSimpleName(type));
    }

    protected String getPackage(String entityType) {
        return Utils.getPackage(entityType);
    }

    protected String getType(Meta meta) {
        if (meta.getOrDefault("is.class", false)) {
            return "class";
        }
        if (meta.getOrDefault("is.enum", false)) {
            return "enum";
        }
        if (meta.getOrDefault("is.interface", false)) {
            return "interface";
        }
        throw new RuntimeException("Expected class, enum or interface! Missing boolean meta key! Entity: "
                + meta.get("package") + " " + meta.get("simple.name"));
    }

    protected String getPropType(Property p, RenderingContext ctx, String entityTypeMeta) {
        String propType = p.meta.get(entityTypeMeta);
        if (propType == null) {
            EntityMapping m = ctx.mappings.get(p.type);
            propType = m != null ? m.meta.get(entityTypeMeta) : propType;
        }
        return propType != null ? propType : p.type;
    }

    protected String getPropSimpleName(Property p, RenderingContext ctx, String entityTypeMeta) {
        return getSimpleName(getPropType(p, ctx, entityTypeMeta));
    }

    protected List<EntityMapping> filter(List<EntityMapping> list, Predicate<EntityMapping> criteria) {
        return SearchUtils.filter(list, criteria);
    }

    protected List<Property> filter(EntityMapping m, Predicate<Property> criteria) {
        return filter(m.props.values(), criteria);
    }

    protected List<Property> filter(Collection<Property> props, Predicate<Property> criteria) {
        return SearchUtils.filter(props, criteria);
    }

    public static List<String> startsWith(Collection<String> coll, String prefix) {
        return SearchUtils.startsWith(coll, prefix);
    }

    public static Method findMethod(EntityMapping m, String name) {
        return find(m.methods, me -> me.name.equals(name));
    }

    public static Property findProperty(EntityMapping m, String name) {
        return find(m.props.values(), me -> me.name.equals(name));
    }

    public static Property findProperty(EntityMapping m, Predicate<Property> criteria) {
        return find(m.props.values(), criteria);
    }

    public static <T> T find(Collection<T> list, Predicate<T> criteria) {
        return SearchUtils.find(list, criteria);
    }

    public static boolean hasMeta(Property prop, String metaKey) {
        return prop.meta.get(metaKey) != null;
    }

    public static boolean isType(Property p, String type) {
        return p.type.equals(type);
    }

    public static boolean isEnum(Property p, RenderingContext ctx) {
        EntityMapping m = ctx.mappings.get(p.type);
        if (m == null) {
            return false;
        }
        return m.meta.getOrDefault("is.enum", false);
    }

    protected EntityMapping[] filterParents(List<EntityMapping> parents) {
        List<EntityMapping> collected = filter(parents, p -> p.meta.getOrDefault("has.constructors", true));
        EntityMapping[] f = new EntityMapping[collected.size()];
        for (int i = 0; i < f.length; i++) {
            f[i] = collected.get(i);
        }
        return f;
    }

    protected void saveFile(String destination, String file, String fileExt) throws Exception {
        saveFile(destination, file.replaceAll("\\.", "/") + "." + fileExt);
    }

    protected void saveFile(String destination, String file) throws Exception {
        FileUtils.createFile(destination, file, writer);
    }

    protected void info(String text) {
        LOGGER.log(Level.INFO, "\n{0}\n", text);
    }

    protected void warn(String text) {
        LOGGER.log(Level.WARNING, "\n{0}\n", text);
    }

    protected void error(String text) {
        LOGGER.log(Level.SEVERE, "\n{0}\n", text);
    }
}
