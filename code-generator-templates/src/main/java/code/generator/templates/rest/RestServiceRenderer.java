package code.generator.templates.rest;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.templates.JavaTemplateRenderer;
import static code.generator.templates.rest.VoRenderer.VO_LIST_PACKAGE;
import static code.generator.templates.rest.VoRenderer.VO_TYPE_META;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author gdimitrova
 */
public class RestServiceRenderer extends JavaTemplateRenderer {

    public final static String REST_SERVICE_PACKAGE = "com.library.rest.api.service";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add(VO_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!mapping.meta.getOrDefault("generate.rest.service", true)
                || !mapping.meta.getOrDefault("is.class", false)
                || mapping.meta.getOrDefault("is.abstract", false)) {
            return;
        }

        String entityName = getSimpleName(mapping.entityType);
        String voType = mapping.meta.get("vo.type");
        String voSimpleName = getSimpleName(voType);
        String listVoSimpleName = voSimpleName.replace("Vo", "").concat("sListVo");
        String listVoType = VO_LIST_PACKAGE + "." + listVoSimpleName;
        String serviceName = entityName + "RestService";
        String serviceType = REST_SERVICE_PACKAGE + "." + serviceName;

        info("\t\tGenerating Rest service interface : " + serviceType);

        printLine("package " + REST_SERVICE_PACKAGE + ";");
        printLine();
        printLine("import com.library.rest.api.SearchRestService;");
        printLine("import " + voType + ";");
       // printLine("import " + VoRenderer.makeFilterType(entityName) + ";");
        printLine("import " + listVoType + ";");
        printLine();
        printInfo(this.getClass());
        printLine("public interface " + serviceName 
                + " extends SearchRestService<" 
               // + VoRenderer.makeFilterName(entityName) + ", " 
                + voSimpleName + ", " 
                + listVoSimpleName + "> {");
        printLine("}");

        saveFile(destination, serviceType);
    }
}
