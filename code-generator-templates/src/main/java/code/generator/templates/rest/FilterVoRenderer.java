package code.generator.templates.rest;

import code.generator.templates.JavaTemplateRenderer;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.templates.ResourceTemplateRenderer;
import static code.generator.templates.filter.SearchFieldsEnumRenderer.REQUIRED_META;
import static code.generator.templates.rest.VoRenderer.makeFilterName;
import static code.generator.templates.rest.VoRenderer.makeFilterType;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class FilterVoRenderer extends JavaTemplateRenderer {

    public final static String TEMPLATE_RESOURCE = "FilterVoTemplate.txt";

    public final static String ENTITY = "com.library.rest.api.vo.filter.FilterVo";

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (!m.meta.getOrDefault("is.class", false) || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    public void render(RenderingContext ctx, String destination) throws Exception {
        info("\t\tGenerating FilterVo implementation ");

        List<EntityMapping> filtered = filter(ctx.mappings.values());

        Map<String, String> map = new HashMap<>();
        map.put("%IMPORTS%", make(filtered,
                (m) -> "import "
                + makeFilterType(getSimpleName(m.entityType))
                + ";\n")
        );
        String types = make(filtered,
                (m) -> {
                    String name = getSimpleName(m.entityType);
                    return "@JsonSubTypes.Type(value="
                    + makeFilterName(name)
                    + ".class, name=\"" + name.toLowerCase() + "filter\"),\n\t";
                });

        map.put("%TYPES%", types.substring(0, types.lastIndexOf(",")));
        ResourceTemplateRenderer.INSTANCE.render(
                TEMPLATE_RESOURCE,
                map,
                destination,
                ENTITY,
                "java",
                writer
        );
    }

    private String make(List<EntityMapping> filtered, Function<EntityMapping, String> print) {
        StringBuilder sb = new StringBuilder();
        filtered.forEach(m -> {
            sb.append(print.apply(m));
        });
        return sb.toString();
    }

    @Override
    public void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        //nothing
    }
}
