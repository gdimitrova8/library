package code.generator.templates.rest;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.templates.JavaTemplateRenderer;
import static code.generator.templates.rest.RestServiceRenderer.REST_SERVICE_PACKAGE;
import static code.generator.templates.rest.VoRenderer.VO_TYPE_META;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class RootResourceRenderer extends JavaTemplateRenderer {

    public final static String ROOT_RESOURCE_PACKAGE = "com.library.rest.api";

    public final static String ROOT_RESOURCE_NAME = "RootResource";

    public final static String ROOT_RESOURCE_TYPE = ROOT_RESOURCE_PACKAGE + "." + ROOT_RESOURCE_NAME;

    private final static Integer ENTITY_NAME = 0;

    private final static Integer SERVICE_NAME = 1;

    private final static Integer SERVICE_TYPE = 2;

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add(VO_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    private Map<String, Map<Integer, String>> makeCache(List<EntityMapping> filteredMappings) {
        Map<String, Map<Integer, String>> cache = new HashMap<>();
        filteredMappings.forEach(m -> {
            String entitySimpleName = getSimpleName(m.entityType);
            String serviceName = entitySimpleName + "RestService";
            String serviceType = REST_SERVICE_PACKAGE + "." + serviceName;

            Map<Integer, String> mappingCache = new HashMap<>();
            mappingCache.put(ENTITY_NAME, entitySimpleName);
            mappingCache.put(SERVICE_NAME, serviceName);
            mappingCache.put(SERVICE_TYPE, serviceType);
            cache.put(m.entityType, mappingCache);
        });
        return cache;
    }

    @Override
    public void render(RenderingContext ctx, String destination) throws Exception {
        info("\t\tGenerating Root resource interface : " + ROOT_RESOURCE_TYPE);
        List<EntityMapping> filteredMappings = filter(ctx.mappings.values());
        Map<String, Map<Integer, String>> cache = makeCache(filteredMappings);
        printLine("package " + ROOT_RESOURCE_PACKAGE + ";");
        printLine();
        cache.values().forEach(mCache -> {
            printLine("import " + mCache.get(SERVICE_TYPE) + ";");
        });
        printLine("import javax.ws.rs.Consumes;");
        printLine("import javax.ws.rs.Path;");
        printLine("import javax.ws.rs.Produces;");
        printLine();
        printInfo(this.getClass());
        printLine("@Path(\"/\")");
        printLine("@Produces(\"application/json\")");
        printLine("@Consumes(\"application/json\")");
        printLine("public interface " + ROOT_RESOURCE_NAME + " {\n");
        cache.values().forEach(mCache -> {
            String entityName = mCache.get(ENTITY_NAME);
            String path = makePath(entityName);
            printLine(offset + "@Path(\"" + path + "\")");
            printLine(offset + mCache.get(SERVICE_NAME) + " get" + entityName + "sRestService();\n");
        });
        printLine("}");
        saveFile(destination, ROOT_RESOURCE_TYPE);
    }

    public static String makePath(String entityName) {
        return entityName.replaceAll("([A-Z])", "/$1").toLowerCase();
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (m.meta.getOrDefault("skipped.rest.service", false)
                    || !m.meta.getOrDefault("is.class", false)
                    || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        // nothing
    }

}
