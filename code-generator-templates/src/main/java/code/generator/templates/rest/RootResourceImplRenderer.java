package code.generator.templates.rest;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.templates.JavaTemplateRenderer;
import static code.generator.templates.rest.RestServiceImplRenderer.REST_SERVICE_IMPL_PACKAGE;
import static code.generator.templates.rest.RestServiceRenderer.REST_SERVICE_PACKAGE;
import static code.generator.templates.rest.RootResourceRenderer.ROOT_RESOURCE_NAME;
import static code.generator.templates.rest.RootResourceRenderer.ROOT_RESOURCE_TYPE;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class RootResourceImplRenderer extends JavaTemplateRenderer {

    public final static String ROOT_RESOURCE_IMPL_PACKAGE = "com.library.bl.rest.impl";

    public final static String ROOT_RESOURCE_IMPL_NAME = "RootResourceImpl";

    public final static String ROOT_RESOURCE_IMPL_TYPE = ROOT_RESOURCE_IMPL_PACKAGE + "." + ROOT_RESOURCE_IMPL_NAME;

    private final static Integer ENTITY_NAME = 0;

    private final static Integer SERVICE_NAME = 1;

    private final static Integer SERVICE_TYPE = 2;

    private final static Integer SERVICE_NAME_IMPL = 3;

    private final static Integer SERVICE_TYPE_IMPL = 4;

    private Map<String, Map<Integer, String>> makeCache(List<EntityMapping> filteredMappings) {
        Map<String, Map<Integer, String>> cache = new HashMap<>();
        filteredMappings.forEach(m -> {
            String entitySimpleName = getSimpleName(m.entityType);
            String serviceName = entitySimpleName + "RestService";
            String serviceType = REST_SERVICE_PACKAGE + "." + serviceName;
            String serviceImplName = serviceName + "Impl";
            String serviceImplType = REST_SERVICE_IMPL_PACKAGE + "." + serviceImplName;

            Map<Integer, String> mappingCache = new HashMap<>();
            mappingCache.put(ENTITY_NAME, entitySimpleName);
            mappingCache.put(SERVICE_NAME, serviceName);
            mappingCache.put(SERVICE_TYPE, serviceType);
            mappingCache.put(SERVICE_NAME_IMPL, serviceImplName);
            mappingCache.put(SERVICE_TYPE_IMPL, serviceImplType);
            cache.put(m.entityType, mappingCache);
        });
        return cache;
    }

    @Override
    public void render(RenderingContext ctx, String destination) throws Exception {
        info("\t\tGenerating Root resource implementation : " + ROOT_RESOURCE_IMPL_TYPE);
        List<EntityMapping> filteredMappings = filter(ctx.mappings.values());
        Map<String, Map<Integer, String>> cache = makeCache(filteredMappings);
        printLine("package " + ROOT_RESOURCE_IMPL_PACKAGE + ";");
        printLine();
        cache.values().forEach(mCache -> {
            printLine("import " + mCache.get(SERVICE_TYPE_IMPL) + ";");
        });
        printLine("import com.library.dao.DaoRegistryFactory;");
        printLine("import " + ROOT_RESOURCE_TYPE + ";");
        cache.values().forEach(mCache -> {
            printLine("import " + mCache.get(SERVICE_TYPE) + ";");
        });
        printLine();
        printInfo(this.getClass());
        printLine("public class " + ROOT_RESOURCE_IMPL_NAME + " implements " + ROOT_RESOURCE_NAME + " {");
        printLine();
        printLine(offset + "private final DaoRegistryFactory daoRegistryFactory;");
        printLine();
        printLine(offset + "public " + ROOT_RESOURCE_IMPL_NAME + "(DaoRegistryFactory daoRegistryFactory) {");
        printLine(offset2 + "this.daoRegistryFactory = daoRegistryFactory;");
        printLine(offset + "}");
        printLine();
        cache.values().forEach(mCache -> {
            String entityName = mCache.get(ENTITY_NAME);
            printLine(offset + "@Override");
            printLine(offset + "public " + mCache.get(SERVICE_NAME) + " get" + entityName + "sRestService(){");
            printLine(offset2 + "return new " + mCache.get(SERVICE_NAME_IMPL) + "(daoRegistryFactory);");
            printLine(offset + "}");
            printLine();
        });
        printLine("}");
        saveFile(destination, ROOT_RESOURCE_IMPL_TYPE);
    }

    @Override
    protected Set<String> getRequiredMeta() {
        return new HashSet() {
            {
                add("vo.type");
            }
        };
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (m.meta.getOrDefault("skipped.rest.service", false)
                    || !m.meta.getOrDefault("is.class", false)
                    || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        // nothing
    }

}
