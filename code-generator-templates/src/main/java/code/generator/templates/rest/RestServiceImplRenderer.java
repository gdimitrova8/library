package code.generator.templates.rest;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import static code.generator.templates.dao.DaoRegistryRenderer.DAO_REGISTRY_NAME;
import static code.generator.templates.dao.DaoRegistryRenderer.DAO_REGISTRY_TYPE;
import static code.generator.templates.dao.DaoRenderer.DAO_PACKAGE;
import static code.generator.templates.exchanger.VoExchangerRenderer.VO_EXCHANGER_TYPE_META;
import code.generator.templates.JavaTemplateRenderer;
import code.generator.templates.domain.DomainEntityListRenderer;
import code.generator.templates.filter.FilterRenderer;
import code.generator.templates.filter.SearchConfigRenderer;
import static code.generator.templates.rest.RestServiceRenderer.REST_SERVICE_PACKAGE;
import static code.generator.templates.rest.VoRenderer.VO_TYPE_META;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author gdimitrova
 */
public class RestServiceImplRenderer extends JavaTemplateRenderer {

    public final static String REST_SERVICE_IMPL_PACKAGE = "com.library.bl.rest.impl.service";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add(VO_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!mapping.meta.getOrDefault("generate.dao", true)
                || !mapping.meta.getOrDefault("is.class", false)
                || mapping.meta.getOrDefault("is.abstract", false)) {
            return;
        }

        String entityName = getSimpleName(mapping.entityType);
        String voType = mapping.meta.get(VO_TYPE_META);
        String voSimpleName = getSimpleName(voType);
        String voExchangerType = mapping.meta.get(VO_EXCHANGER_TYPE_META);
        String voExchangerTypeName = getSimpleName(voExchangerType);
        String listVoSimpleName = VoRenderer.makeListName(voSimpleName);
        String listVoType = VoRenderer.makeListType(voSimpleName);
        String filterType = VoRenderer.makeFilterType(entityName);
        String filterVo = VoRenderer.makeFilterName(entityName);
        String serviceName = entityName + "RestService";
        String serviceType = REST_SERVICE_PACKAGE + "." + serviceName;
        String serviceImplName = serviceName + "Impl";
        String serviceImplType = REST_SERVICE_IMPL_PACKAGE + "." + serviceImplName;
        String daoName = entityName + "Dao";
        String daoType = DAO_PACKAGE + "." + daoName;
        String filterName = FilterRenderer.makeName(entityName);
        String searchCfgName = SearchConfigRenderer.makeName(entityName);
        String listType=DomainEntityListRenderer.makeType(entityName);
        String listTypeName=getSimpleName(listType);

        info("\t\tGenerating Rest service implementation: " + serviceImplType);

        printLine("package " + REST_SERVICE_IMPL_PACKAGE + ";");
        printLine();
        printLine("import com.library.bl.rest.impl.SearchRestServiceImpl;");
        printLine("import " + DAO_REGISTRY_TYPE + ";");
        printLine("import com.library.dao.DaoRegistryFactory;");
        printLine("import " + daoType + ";");
        printLine("import " + mapping.entityType + ";");
        printLine("import " + serviceType + ";");
        printLine("import " + voType + ";");
        printLine("import " + listVoType + ";");
        printLine("import " + voExchangerType + ";");
//        printLine("import " + SearchConfigRenderer.makeType(entityName) + ";");
//        printLine("import " + FilterRenderer.makeType(entityName) + ";");
        printLine("import " + listType + ";");
//        printLine("import " + filterType + ";");
//        printLine("import com.library.rest.api.vo.filter.SearchCfgVo;");
        
        printLine();
        printInfo(this.getClass());
        printLine("public class " + serviceImplName + " extends SearchRestServiceImpl<\n "+ offset2
                + entityName + ",\n " + offset2
//                + filterName + ",\n " + offset2
//                + searchCfgName + ",\n " + offset2
                + listTypeName + ",\n " + offset2
                + daoName + ",\n " + offset2
//                + filterVo + ",\n " + offset2
                + voSimpleName + ",\n " + offset2
                + listVoSimpleName + ",\n " + offset2
                + voExchangerTypeName + ">");
        printLine(offset + " implements " + serviceName + " {");
        printLine();
        printLine(offset + "public " + serviceImplName + "(DaoRegistryFactory factory) {");
        printLine(offset2 + "super(factory,");
//        printLine(offset3 + filterName + "::new,");
//        printLine(offset3 + searchCfgName + "::new,");
        printLine(offset3 + listVoSimpleName + "::new,");
        printLine(offset3 + DAO_REGISTRY_NAME + "::get" + daoName + ",");
        printLine(offset3 + voExchangerTypeName + ".INSTANCE");
//        printLine(offset3 + "(" + filterVo + " filterVo) -> {");
//        printLine(offset3 + "  " + filterName + " filter = new " + filterName + "();");
//        printLine(offset3 + "  " + searchCfgName + " cfg = new " + searchCfgName + "();");
//        printLine(offset3 + "  SearchCfgVo cfgVo = filterVo.getCfg();");
//        printLine(offset3 + "  cfg.setText(cfgVo.getText());");
//        printLine(offset3 + "  cfg.setOrderBy(cfgVo.getOrderBy());");
//        printLine(offset3 + "  cfg.setSearchBy(cfgVo.getSearchBy());");
//        printLine(offset3 + "  filter.setStartIndex(filterVo.getStartIndex());");
//        printLine(offset3 + "  filter.setMaxResults(filterVo.getMaxResults());");
//        printLine(offset3 + "  filter.setSearchCfg(cfg);");
//        printLine(offset3 + "  return filter;");
//        printLine(offset3 + "}");
        printLine(offset2 + ");");
        printLine(offset + "}");
        printLine();
        printLine("}");

        saveFile(destination, serviceImplType);
    }

}
