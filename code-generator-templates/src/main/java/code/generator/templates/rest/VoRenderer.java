package code.generator.templates.rest;

import code.generator.plugin.mojo.renderer.context.Meta;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Method;
import code.generator.plugin.mojo.renderer.context.Property;
import static code.generator.templates.domain.DomainEntityRenderer.DOMAIN_PACKAGE_PREFIX;
import code.generator.templates.JavaTemplateRenderer;
import code.generator.templates.ResourceTemplateRenderer;
import code.generator.templates.filter.FilterFieldsResolver;
import static code.generator.templates.filter.SearchConfigRenderer.printSearchCfg;
import static code.generator.templates.utils.Utils.makeEnumValues;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class VoRenderer extends JavaTemplateRenderer {

    public final static String VO_LIST_PACKAGE = "com.library.rest.api.vo.list";

    public final static String FILTER_VO_PACKAGE = "com.library.rest.api.vo.filter";

    public final static String VO_TYPE_META = "vo.type";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add(VO_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!mapping.meta.getOrDefault("generate.vo", true)) {
            return;
        }
        if (mapping.meta.getOrDefault("is.enum", false)) {
            printEnum(mapping, destination);
            return;
        }
        if (mapping.meta.getOrDefault("is.class", false)) {
            printVoEntity(ctx, mapping, destination);
            if (!mapping.meta.getOrDefault("is.abstract", false)) {
                printListVo(mapping, destination);
                printSearchFieldsVo(mapping, destination);
                // printSearchConfigVo(mapping, destination);
                //printFilterVo(mapping, destination);
            }
        }
    }

    private void printVoEntity(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        String voType = mapping.meta.get(VO_TYPE_META);
        info("\t\tGenerating VO : " + voType);
        info("");

        Collection<Property> props = mapping.props.values();
        List<EntityMapping> parents = getParents(ctx, mapping);
        EntityMapping[] filteredParents = filterParents(parents);
        String simpleName = getSimpleName(voType);
        String voTypePackage = getPackage(voType);
        Method equalsMethod = findMethod(mapping, "equals");
        Method hashCode = findMethod(mapping, "hashCode");

        printLine("package " + voTypePackage + ";\n");

        if (hashCode != null) {
            printLine("import java.util.Objects;");
        }
        printImports(mapping, ctx, "vo.parent", "vo.implements", VO_TYPE_META,
                t -> !t.startsWith("java.lang.") && !t.startsWith(DOMAIN_PACKAGE_PREFIX)
                && !voTypePackage.equals(getPackage(t))
        );
        printInfo(this.getClass());
        printDeclaration(mapping.meta, simpleName, "vo.parent", "vo.implements");
        printLine();

        printProps(props, p -> printProperty(p, ctx));
        if (mapping.meta.getOrDefault("has.constructors", true)) {
            printDefaultConstructor(simpleName);
            printConstructor(simpleName, ctx, VO_TYPE_META, props, filteredParents);
        }
        printAccessors(props, ctx, VO_TYPE_META);
        if (hashCode != null) {
            printHashCode(hashCode, props);
        }
        if (equalsMethod != null) {
            printEquals(simpleName, props);
        }

        printLine("}");

        saveFile(destination, voType);
    }

    protected void printProperty(Property prop, RenderingContext ctx) {
        Meta meta = prop.meta;
        String mod = meta.get("modifier");
        String voType = getPropSimpleName(prop, ctx, VO_TYPE_META);
        if (prop.collectionType != null) {
            printCollectionProp(prop, voType);
            return;
        }
        printLine(offset
                + (mod != null ? mod + " " : "")
                + voType + " " + prop.name + ";\n");
    }

    private final static String LIST_TEMPLATE_RESOURCE = "EntityListTemplate.txt";

    private void printListVo(EntityMapping mapping, String destination) throws Exception {
        String voType = mapping.meta.get(VO_TYPE_META);
        String voSimpleName = getSimpleName(voType);
        String listVoSimpleName = makeListName(voSimpleName);
        String listVoType = makeListType(voSimpleName);
        info("\t\tGenerating List VO : " + listVoType);
        info("");

        Map<String, String> map = new HashMap<>();
        map.put("%LIST_PKG%", VO_LIST_PACKAGE);
        map.put("%PARENT_LIST_PKG%", "com.library.rest.api.vo.EntityListVo");
        map.put("%ENTITY_PKG%", voType);
        map.put("%RENDERER%", this.getClass().getName() + ".java");
        map.put("%LIST_TYPE_NAME%", listVoSimpleName);
        map.put("%PARENT_LIST%", "EntityListVo");
        map.put("%ENTITY_NAME%", voSimpleName);

        ResourceTemplateRenderer.INSTANCE.render(
                LIST_TEMPLATE_RESOURCE,
                map,
                destination,
                listVoType,
                "java",
                writer
        );
    }

    public static String makeListType(String voName) {
        return VO_LIST_PACKAGE + "." + makeListName(voName);
    }

    public static String makeListName(String voName) {
        return voName.replace("Vo", "").concat("sListVo");
    }

    public void printFilterVo(EntityMapping mapping, String destination) throws Exception {
        String entityName = getSimpleName(mapping.entityType);

        String entityType = makeFilterType(entityName);
        info("\t\tGenerating filter entity : " + entityType);
        info("");

        String searchCfgName = "SearchCfgVo";//entityName + "SearchConfigVo";

        printLine("package " + FILTER_VO_PACKAGE + ";\n");
        printLine();
        printLine("import com.fasterxml.jackson.annotation.JsonTypeName;");
        printLine("import com.fasterxml.jackson.annotation.JsonIgnoreProperties;");
        printLine("import " + FILTER_VO_PACKAGE + "." + searchCfgName + ";");

        printInfo(this.getClass());
        printLine("@JsonTypeName(\"" + entityName.toLowerCase() + "filter\")");
        printLine("@JsonIgnoreProperties(ignoreUnknown = true)");
        printLine("public class " + entityName + "sFilterVo extends FilterVo {");
        printLine();
        printDefaultConstructor(entityName + "sFilterVo");
        printLine(offset + "public " + entityName + "sFilterVo(Integer startIndex, Integer maxResults, " + searchCfgName + " cfg) {");
        printLine(offset2 + "super(startIndex, maxResults, cfg);");
        printLine(offset + "}");
        printLine();
        printLine("}");

        saveFile(destination, entityType);
    }

    public static String makeFilterType(String entityName) {
        return FILTER_VO_PACKAGE + "." + makeFilterName(entityName);
    }

    public static String makeFilterName(String entityName) {
        return entityName + "sFilterVo";
    }

    public void printSearchConfigVo(EntityMapping mapping, String destination) throws Exception {

        String entityName = getSimpleName(mapping.entityType);
        String entityType = FILTER_VO_PACKAGE + "." + entityName + "SearchConfigVo";

        info("\t\tGenerating SearchConfig Vo entity : " + entityType);
        info("");

        Method m = findMethod(mapping, "hashCode");

        Map<String, String> map = new HashMap<>();
        map.put("%PACKAGE%", FILTER_VO_PACKAGE);
        map.put("%IMPORTS%", "import com.fasterxml.jackson.annotation.JsonTypeName;");
        map.put("%ANNOTATION%", "@JsonTypeName(\"" + entityName.toLowerCase() + "\")");
        map.put("%ENTITY_S_CFG%", makeSearchConfigVoName(entityName));
        map.put("%ENTITY_S_FIELDS%", entityName + "SearchFieldsVo");
        map.put("%CLASS_OR_INTERFACE%", "extends SearchCfgVo");
        map.put("%HASH%", m.meta.get("hash").toString());
        map.put("%HASH_NUM%", m.meta.get("hash.number").toString());

        printSearchCfg(map, writer);
        saveFile(destination, entityType);
    }

    public static String makeSearchConfigVoName(String entityName) {
        return entityName + "SearchConfigVo";
    }

    private void printSearchFieldsVo(EntityMapping mapping, String destination) throws Exception {
        String entityName = getSimpleName(mapping.entityType);
        String entityType = FILTER_VO_PACKAGE + "." + entityName + "SearchFieldsVo";
        info("\t\tGenerating SearchFieldsVo enum : " + entityType);
        info("");

        List<String> fields = FilterFieldsResolver.INSTANCE.resolve(mapping);
        printLine("package " + FILTER_VO_PACKAGE + ";\n");

        printInfo(this.getClass());
        print("public enum " + entityName + "SearchFieldsVo {\n");
        printLine();
        printLine(offset + "NONE, " + String.join(", ", makeEnumValues(fields)));
        printLine();
        printLine("}");

        saveFile(destination, entityType);
    }

    private void printEnum(EntityMapping mapping, String destination) throws Exception {
        String voType = mapping.meta.get(VO_TYPE_META);
        info("\t\tGenerating VO enum : " + voType);
        info("");

        Collection<Property> props = mapping.props.values();
        printLine("package " + getPackage(voType) + ";\n");

        printInfo(this.getClass());
        printDeclaration(mapping.meta, getSimpleName(voType));
        printLine();
        printLine(offset + String.join(", ", props.stream().map(p -> p.name).collect(Collectors.toSet())));
        printLine();
        printLine("}");

        saveFile(destination, voType);
    }

}
