package code.generator.templates.test;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.utils.FileUtils;
import code.generator.templates.AbstractTemplateRenderer;
import static code.generator.templates.dto.DtoRenderer.DTO_TYPE_META;
import static code.generator.templates.exchanger.DtoExchangerRenderer.DTO_EXCHANGER_TYPE_META;
import static code.generator.templates.exchanger.VoExchangerRenderer.VO_EXCHANGER_TYPE_META;
import code.generator.templates.JavaTemplateRenderer;
import static code.generator.templates.rest.VoRenderer.VO_TYPE_META;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class EntityExchangerTestCaseRenderer extends JavaTemplateRenderer {

    public final static String TEMPLATE_RESOURCE = "EntityExchangerTestCaseTemplate.txt";

    public final static String ENTITY_EXCHANGER_TEST_CASE_PACKAGE = "com.library.domain.exchanger";
    
    public final static String EXCHANGER_PARENT_TYPE_META = "exchanger.parent";

    private final Set<String> voMetaKeys = new HashSet() {
        {
            add(VO_TYPE_META);
            add(VO_EXCHANGER_TYPE_META);
        }
    };

    private final Set<String> dtoMetaKeys = new HashSet() {
        {
            add(DTO_TYPE_META);
            add(DTO_EXCHANGER_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return null;
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (!m.meta.getOrDefault("is.class", false) || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsAll(m, voMetaKeys) || containsAll(m, dtoMetaKeys);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        String entityName = getSimpleName(mapping.entityType);
        String entityExchangerTestCaseName = entityName + "ExchangerTestCase";
        String entityExchangerTestCaseType = ENTITY_EXCHANGER_TEST_CASE_PACKAGE + "." + entityExchangerTestCaseName;
        info("\t\tGenerating Entity exchanger Test Case: " + entityExchangerTestCaseType);
        info("");
        String content = FileUtils.loadContent(AbstractTemplateRenderer.class, TEMPLATE_RESOURCE, true);
       String parent= mapping.meta.getOrDefault(EXCHANGER_PARENT_TYPE_META, "EntityExchanger");
        content = content.replaceAll("#EXCHANGER#", parent);
        content = content.replaceAll("#TEST_CASE#", entityExchangerTestCaseName);
        content = content.replaceAll("#ENTITY_TYPE#", mapping.entityType);
        content = content.replaceAll("#ENTITY_NAME#", entityName);
        writer.write(content);
        saveFile(destination, entityExchangerTestCaseType);
    }
}
