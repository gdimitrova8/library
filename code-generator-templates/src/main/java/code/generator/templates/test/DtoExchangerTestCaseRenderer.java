package code.generator.templates.test;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.utils.FileUtils;
import code.generator.templates.AbstractTemplateRenderer;
import static code.generator.templates.dto.DtoRenderer.DTO_TYPE_META;
import static code.generator.templates.exchanger.DtoExchangerRenderer.DTO_EXCHANGER_TYPE_META;
import code.generator.templates.JavaTemplateRenderer;
import code.generator.templates.ResourceTemplateRenderer;
import static code.generator.templates.test.EntityExchangerTestCaseRenderer.ENTITY_EXCHANGER_TEST_CASE_PACKAGE;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class DtoExchangerTestCaseRenderer extends JavaTemplateRenderer {

    public final static String TEMPLATE_RESOURCE = "DtoExchangerTestCaseTemplate.txt";

    public final static String TEST_PACKAGE = "com.library.dto.exchanger";

    private final Set<String> dtoMetaKeys = new HashSet() {
        {
            add(DTO_TYPE_META);
            add(DTO_EXCHANGER_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return dtoMetaKeys;
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (!m.meta.getOrDefault("is.class", false) || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        String entityName = getSimpleName(mapping.entityType);
        String entityExchangerTestCaseName = entityName + "ExchangerTestCase";
        String entityExchangerTestCaseType = ENTITY_EXCHANGER_TEST_CASE_PACKAGE + "." + entityExchangerTestCaseName;
        String dtoType = mapping.meta.get(DTO_TYPE_META);
        String dtoExchangerType = mapping.meta.get(DTO_EXCHANGER_TYPE_META);
        String dtoExchangerName = getSimpleName(dtoExchangerType);
        String dtoExchangerTestCase = TEST_PACKAGE + "." + dtoExchangerName + "TestCase";
        info("\t\tGenerating Entity exchanger Test Case: " + dtoExchangerTestCase);
        info("");
        String content = FileUtils.loadContent(AbstractTemplateRenderer.class, TEMPLATE_RESOURCE, true);
        content = content.replaceAll("#ENTITY_EXCHANGER_TEST_TYPE#", entityExchangerTestCaseType);
        content = content.replaceAll("#ENTITY_EXCHANGER_TEST_NAME#", entityExchangerTestCaseName);
        // content = content.replaceAll("#ENTITY_TYPE#", mapping.entityType);
        content = content.replaceAll("#DTO_EXCHANGER_TYPE#", dtoExchangerType);
        content = content.replaceAll("#DTO_EXCHANGER_NAME#", dtoExchangerName);
        content = content.replaceAll("#DTO_TYPE#", dtoType);
        content = content.replaceAll("#DTO_NAME#", getSimpleName(dtoType));
        writer.write(content);
        saveFile(destination, dtoExchangerTestCase);
    }
}
