package code.generator.templates.test;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.utils.FileUtils;
import code.generator.templates.AbstractTemplateRenderer;
import static code.generator.templates.exchanger.VoExchangerRenderer.VO_EXCHANGER_TYPE_META;
import code.generator.templates.JavaTemplateRenderer;
import static code.generator.templates.rest.VoRenderer.VO_TYPE_META;
import static code.generator.templates.test.EntityExchangerTestCaseRenderer.ENTITY_EXCHANGER_TEST_CASE_PACKAGE;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class VoExchangerTestCaseRenderer extends JavaTemplateRenderer {

    public final static String TEMPLATE_RESOURCE = "VoExchangerTestCaseTemplate.txt";

    public final static String TEST_PACKAGE = "com.library.vo.exchanger";

    private final Set<String> voMetaKeys = new HashSet() {
        {
            add(VO_TYPE_META);
            add(VO_EXCHANGER_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return voMetaKeys;
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (!m.meta.getOrDefault("is.class", false) || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        String entityName = getSimpleName(mapping.entityType);
        String entityExchangerTestCaseName = entityName + "ExchangerTestCase";
        String entityExchangerTestCaseType = ENTITY_EXCHANGER_TEST_CASE_PACKAGE + "." + entityExchangerTestCaseName;
        String voType = mapping.meta.get(VO_TYPE_META);
        String voExchangerType = mapping.meta.get(VO_EXCHANGER_TYPE_META);
        String voExchangerName = getSimpleName(voExchangerType);
        String voExchangerTestCase = TEST_PACKAGE + "." + voExchangerName + "TestCase";
        info("\t\tGenerating Entity exchanger Test Case: " + voExchangerTestCase);
        info("");
        String content = FileUtils.loadContent(AbstractTemplateRenderer.class, TEMPLATE_RESOURCE, true);
        content = content.replaceAll("#ENTITY_EXCHANGER_TEST_TYPE#", entityExchangerTestCaseType);
        content = content.replaceAll("#ENTITY_EXCHANGER_TEST_NAME#", entityExchangerTestCaseName);
       // content = content.replaceAll("#ENTITY_TYPE#", mapping.entityType);
        content = content.replaceAll("#VO_EXCHANGER_TYPE#", voExchangerType);
        content = content.replaceAll("#VO_EXCHANGER_NAME#", voExchangerName);
        content = content.replaceAll("#VO_TYPE#", voType);
        content = content.replaceAll("#VO_NAME#", getSimpleName(voType));
        writer.write(content);
        saveFile(destination, voExchangerTestCase);
    }
}
