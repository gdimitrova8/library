package code.generator.templates.dao;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Property;
import code.generator.templates.JavaTemplateRenderer;
import code.generator.templates.domain.DomainEntityListRenderer;
import static code.generator.templates.dto.DtoRenderer.DTO_TYPE_META;
import static code.generator.templates.exchanger.DtoExchangerRenderer.DTO_EXCHANGER_TYPE_META;
import code.generator.templates.filter.FilterFieldsResolver;
import static code.generator.templates.filter.SearchFieldsEnumRenderer.FILTER_PKG;
import code.generator.templates.utils.Utils;
import static code.generator.templates.utils.Utils.makeEnumValues;
import edu.emory.mathcs.backport.java.util.Arrays;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author gdimitrova
 */
public class DaoImplRenderer extends JavaTemplateRenderer {

    public final static String DAO_PACKAGE = "com.library.dao";

    public final static String DAO_IMPL_META = "dao.impl.type";

    public final static String DAO_IMPL_PROPS_META = "dao.impl.props";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add("is.class");
            add(DAO_IMPL_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!mapping.meta.getOrDefault("generate.dao.impl", true)
                || !mapping.meta.getOrDefault("is.class", false)
                || mapping.meta.getOrDefault("is.abstract", false)) {
            return;
        }

        String entityName = getSimpleName(mapping.entityType);
        String daoImplType = mapping.meta.get(DAO_IMPL_META);
        String daoImplName = getSimpleName(daoImplType);

        String dtoType = mapping.meta.get(DTO_TYPE_META);
        String listType = DomainEntityListRenderer.makeType(entityName);
        String dtoName = getSimpleName(dtoType);
        String dtoExchangerType = mapping.meta.get(DTO_EXCHANGER_TYPE_META);
        String dtoExchangerName = getSimpleName(dtoExchangerType);
        String listTypeName = getSimpleName(listType);

        info("\t\tGenerating Dao implementation: " + daoImplType);

        printLine("package " + getPackage(daoImplType) + ";");
        printLine();
        printLine("import " + DAO_PACKAGE + ".AbstractSearchDao;");
        printLine("import " + mapping.entityType + ";");
//        printLine("import " + FilterRenderer.makeType(entityName) + ";");
        printLine("import " + dtoType + ";");
        printLine("import " + listType + ";");
        printLine("import " + DaoRenderer.makeType(entityName) + ";");
        printLine("import " + FILTER_PKG + "." + entityName + "SearchFields;");
        printLine("import " + dtoExchangerType + ";");
        printLine("import java.util.HashMap;");
        printLine("import java.util.Map;");
        printLine("import javax.persistence.EntityManager;");
        printLine();
        printInfo(this.getClass());
        printLine("public class " + daoImplName + " extends AbstractSearchDao<\n" + offset2
                //                + FilterRenderer.makeName(entityName) + ",\n" + offset2
                + dtoName + ",\n" + offset2
                + entityName + ",\n" + offset2
                + listTypeName + ",\n" + offset2
                + dtoExchangerName + ">\n" + offset2
                + "implements " + DaoRenderer.makeName(entityName) + "{ ");
        printLine();
        printLine(offset + "public " + daoImplName + "(EntityManager em) {");
        printLine(offset2 + "super(em, "
                + listTypeName + "::new, "
                + entityName + ".class, "
                + dtoName + ".class, "
                + dtoExchangerName + ".INSTANCE);");
        printLine(offset + "}");
        printLine();
        printLine(offset + "@Override");
        printLine(offset + "protected Map<String, Object> loadProperties(" + dtoName + " newOne) {");
        printLine(offset2 + "Map<String, Object> props = new HashMap<>();");
        printMappedProps(ctx, mapping);
        printLine(offset2 + "return props;");
        printLine(offset + "}");
        printLine();
        printLine(offset + "Map<String, String> mappedPropNameByEnumValue = new HashMap<>() {");
        printLine(offset2 + "{");
        Set<String> propNames = makeEnumValues(FilterFieldsResolver.INSTANCE.resolve(mapping));
        propNames.forEach((propName) -> {
            printLine(offset3 + "put(" + entityName + "SearchFields."
                    + propName + ".name(), "
                    + entityName + "Dto." + propName + ");");
        });
        printLine(offset2 + "}");
        printLine(offset + "};");
        printLine();
        printLine(offset + "@Override");
        printLine(offset + "public String resolvePropertyName(String enumValue) {");
        printLine(offset2 + "if(enumValue == null");
        printLine(offset3 + "|| enumValue.equals(\"NONE\") ");
        printLine(offset3 + "|| mappedPropNameByEnumValue.values().isEmpty()");
        printLine(offset2 + "){");
        printLine(offset3 + "return null;");
        printLine(offset2 + "}");
        printLine(offset2 + "return mappedPropNameByEnumValue.get(enumValue);");
        printLine(offset + "}");
        printLine();
        printLine("}");

        saveFile(destination, daoImplType);
    }

    private void printMappedProps(RenderingContext ctx, EntityMapping m) {
        String propMeta = m.meta.get(DAO_IMPL_PROPS_META);
        if (propMeta == null || propMeta.equals("")) {
            return;
        }
        if (propMeta.contains(",")) {
            Set<String> set = new HashSet<>(Arrays.asList(propMeta.split(",")));
            printProps(filter(m, p -> set.contains(p.name)));
            return;
        }
        if (propMeta.equals("*")) {
            ArrayList<Property> props = new ArrayList<>(m.props.values());

            EntityMapping[] parents = filterParents(getParents(ctx, m));
            for (EntityMapping parent : parents) {
                props.addAll(parent.props.values());
            }
            printProps(props);
        }
    }

    private void printProps(List<Property> props) {
        props.forEach((prop) -> {
            printLine(offset2 + "props.put(\"" + prop.name + "\", newOne.get" + Utils.makeCamelName(prop.name) + "());");
        });
        // printLine(offset2 + "props.put(AuthorDto.BIOGRAPHY, newOne.getBiography());");
    }

}
