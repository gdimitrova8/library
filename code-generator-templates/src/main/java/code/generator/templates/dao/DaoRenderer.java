package code.generator.templates.dao;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.templates.JavaTemplateRenderer;
import code.generator.templates.domain.DomainEntityListRenderer;
import code.generator.templates.filter.FilterRenderer;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author gdimitrova
 */
public class DaoRenderer extends JavaTemplateRenderer {

    public final static String DAO_PACKAGE = "com.library.dao";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add("is.class");
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!mapping.meta.getOrDefault("generate.dao", true)
                || !mapping.meta.getOrDefault("is.class", false)
                || mapping.meta.getOrDefault("is.abstract", false)) {
            return;
        }

        String entityName = getSimpleName(mapping.entityType);
        String daoType = makeType(entityName);

        info("\t\tGenerating Dao interface: " + daoType);

        printLine("package " + DAO_PACKAGE + ";");
        printLine();
        printLine("import " + mapping.entityType + ";");
        //printLine("import " + FilterRenderer.makeType(entityName) + ";");
        printLine("import " + DomainEntityListRenderer.makeType(entityName) + ";");
        printLine();
        printInfo(this.getClass());
        printLine("public interface " + makeName(entityName)
                + " extends SearchDao<"
               // + FilterRenderer.makeName(entityName) + ", "
                + entityName + ", "
                + DomainEntityListRenderer.makeName(entityName)
                + "> {");
        // printLine(offset + "public interface " + daoName + " extends CrudDao<" + entityName + "> {");
        printLine();
        printLine("}");

        saveFile(destination, daoType);
    }

    public static String makeType(String entityName) {
        return DAO_PACKAGE + "." + makeName(entityName);
    }

    public static String makeName(String entityName) {
        return entityName + "Dao";
    }

}
