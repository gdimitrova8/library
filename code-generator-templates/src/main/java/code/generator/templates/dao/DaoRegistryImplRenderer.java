package code.generator.templates.dao;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import static code.generator.templates.dao.DaoRegistryRenderer.DAO_REGISTRY_NAME;
import static code.generator.templates.dao.DaoRenderer.DAO_PACKAGE;
import code.generator.templates.JavaTemplateRenderer;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class DaoRegistryImplRenderer extends JavaTemplateRenderer {

    public final static String DAO_REGISTRY_IMPL_PACKAGE = "com.library.dao";

    public final static String DAO_REGISTRY_IMPL_NAME = DAO_REGISTRY_NAME + "Impl";

    public final static String DAO_REGISTRY_IMPL_TYPE = DAO_REGISTRY_IMPL_PACKAGE + "." + DAO_REGISTRY_IMPL_NAME;

    private final static Integer FIELD_NAME = 0;

    private final static Integer DAO_NAME = 1;

    private final static Integer DAO_TYPE = 2;

    private final static Integer DAO_NAME_IMPL = 3;

    private final static Integer DAO_TYPE_IMPL = 4;

    private Map<String, Map<Integer, String>> makeCache(List<EntityMapping> filteredMappings) {
        Map<String, Map<Integer, String>> cache = new HashMap<>();
        filteredMappings.forEach(m -> {
            String entityName = getSimpleName(m.entityType);
            String daoName = entityName + "Dao";
            String daoType = DAO_PACKAGE + "." + daoName;
            String daoImplType = m.meta.get("dao.impl.type");
            String daoImplName = getSimpleName(daoImplType);

            Map<Integer, String> mappingCache = new HashMap<>();
            mappingCache.put(FIELD_NAME, entityName.toLowerCase() + "Dao");
            mappingCache.put(DAO_NAME, daoName);
            mappingCache.put(DAO_TYPE, daoType);
            mappingCache.put(DAO_NAME_IMPL, daoImplName);
            mappingCache.put(DAO_TYPE_IMPL, daoImplType);
            cache.put(m.entityType, mappingCache);
        });
        return cache;
    }

    @Override
    public void render(RenderingContext ctx, String destination) throws Exception {
        info("\t\tGenerating DaoRegistry implementation : " + DAO_REGISTRY_IMPL_TYPE);
        List<EntityMapping> filteredMappings = filter(ctx.mappings.values());
        Map<String, Map<Integer, String>> cache = makeCache(filteredMappings);
        printLine("package " + DAO_REGISTRY_IMPL_PACKAGE + ";");
        printLine();
        cache.values().forEach(mCache -> {
            printLine("import " + mCache.get(DAO_TYPE_IMPL) + ";");
        });
        printLine("import javax.persistence.EntityManager;");
        printInfo(this.getClass());
        printLine("public class " + DAO_REGISTRY_IMPL_NAME + " implements " + DAO_REGISTRY_NAME + " {");
        printLine();
        printLine(offset + "private final EntityManager em;");
        printLine();
        cache.values().forEach(mCache -> {
            printLine(offset + "private final " + mCache.get(DAO_NAME) + " " + mCache.get(FIELD_NAME) + ";");
            printLine();
        });
        printLine(offset + "public " + DAO_REGISTRY_IMPL_NAME + "(EntityManager em) {");
        printLine(offset2 + "this.em = em;");
        cache.values().forEach(mCache -> {
            printLine(offset2 + mCache.get(FIELD_NAME) + " = new " + mCache.get(DAO_NAME_IMPL) + "(em);");
        });
        printLine(offset + "}");
        printLine();
        cache.values().forEach(mCache -> {
            printLine(offset + "@Override");
            printLine(offset + "public " + mCache.get(DAO_NAME) + " get" + mCache.get(DAO_NAME) + "() {");
            printLine(offset2 + "return " + mCache.get(FIELD_NAME) + ";");
            printLine(offset + "}");
            printLine();
        });
        printLine(offset + "@Override");
        printLine(offset + "public void beginTransaction() {");
        printLine(offset2 + "if (!em.getTransaction().isActive()) {");
        printLine(offset3 + "em.getTransaction().begin();");
        printLine(offset2 + "}");
        printLine(offset + "}");
        printLine();
        printLine(offset + "@Override");
        printLine(offset + "public void rollbackTransaction() {");
        printLine(offset2 + "if (em.getTransaction().isActive()) {");
        printLine(offset3 + "em.getTransaction().rollback();");
        printLine(offset2 + "}");
        printLine(offset + "}");
        printLine();
        printLine(offset + "@Override");
        printLine(offset + "public void commitTransaction() {");
        printLine(offset2 + "if (em.getTransaction().isActive()) {");
        printLine(offset3 + "em.getTransaction().commit();");
        printLine(offset2 + "}");
        printLine(offset + "}");
        printLine();
        printLine(offset + "@Override");
        printLine(offset + "public void close() throws Exception {");
        printLine(offset2 + "em.close();");
        printLine(offset + "}");
        printLine();
        printLine("}");

        saveFile(destination, DAO_REGISTRY_IMPL_TYPE);
    }

    @Override
    protected Set<String> getRequiredMeta() {
        return new HashSet() {
            {
                add("dao.impl.type");
            }
        };
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (!m.meta.getOrDefault("is.class", false) || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        // nothing
    }

}
