package code.generator.templates.dao;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import static code.generator.templates.dao.DaoRenderer.DAO_PACKAGE;
import code.generator.templates.JavaTemplateRenderer;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class DaoRegistryRenderer extends JavaTemplateRenderer {

    public final static String DAO_REGISTRY_NAME = "DaoRegistry";

    public final static String DAO_REGISTRY_TYPE = DAO_PACKAGE + "." + DAO_REGISTRY_NAME;

    @Override
    public void render(RenderingContext ctx, String destination) throws Exception {
        info("\t\tGenerating DaoRegistry interface : " + DAO_REGISTRY_TYPE);
        List<EntityMapping> filteredMappings = filter(ctx.mappings.values());
        printLine("package " + DAO_PACKAGE + ";");
        printLine();
        printInfo(this.getClass());
        printLine("public interface " + DAO_REGISTRY_NAME + " extends AutoCloseable {");
        printLine();
        for (EntityMapping m : filteredMappings) {
            String entityName = getSimpleName(m.entityType);
            printLine(offset + " public " + entityName + "Dao get" + entityName + "Dao();");
            printLine();
        }
        printLine(offset + "public void beginTransaction();");
        printLine();
        printLine(offset + "public void rollbackTransaction();");
        printLine();
        printLine(offset + "public void commitTransaction();");
        printLine();
        printLine("}");

        saveFile(destination, DAO_REGISTRY_TYPE);
    }

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add("is.class");
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (m.meta.getOrDefault("skipped.dao", false) 
                    ||!m.meta.getOrDefault("is.class", false) 
                    || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        // nothing
    }

}
