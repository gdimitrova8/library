package code.generator.templates.i18n;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Property;
import code.generator.templates.AbstractTemplateRenderer;
import static code.generator.templates.domain.DomainEntityRenderer.HAS_MSG_META;
import static code.generator.templates.domain.DomainEntityRenderer.MSG_KEY_PREFIX_META;
import code.generator.templates.utils.Utils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class I18nPropertiesFileRenderer extends AbstractTemplateRenderer {

    public final static String PROPERTIES_FILE = "generated_i18n_EN.properties";

    @Override
    public void render(RenderingContext ctx, String destination) throws Exception {
        List<EntityMapping> mappings = filter(ctx.mappings.values());
        Map<String, String> i18n = new HashMap<>();
        mappings.forEach((m) -> {
            String type = m.entityType;
            List<Property> props = getMessageProps(m);
            for (Property p : props) {
                String keyPrefix = p.meta.get(MSG_KEY_PREFIX_META);
                i18n.put(type + "." + keyPrefix, Utils.makeCamelName(keyPrefix.replaceAll("\\.", " ")));
            }
        });
        ArrayList<String> sortedKeys = new ArrayList<>(i18n.keySet());
        Collections.sort(sortedKeys);
        sortedKeys.forEach((k) -> {
            printLine(k + "=" + i18n.get(k));
        });
        saveFile(destination, PROPERTIES_FILE);
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> hasMessageProps(m)).collect(Collectors.toList());
    }

    private boolean hasMessageProps(EntityMapping mapping) {
        return getMessageProps(mapping).isEmpty() == false;
    }

    private List<Property> getMessageProps(EntityMapping mapping) {
        return filter(mapping, HAS_MSG_META);
    }

    @Override
    public void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        //nothing
    }

    @Override
    protected Set<String> getRequiredMeta() {
        return new HashSet() {
            {
                add("is.class");
            }
        };
    }
}
