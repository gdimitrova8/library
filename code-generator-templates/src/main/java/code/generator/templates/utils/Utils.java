package code.generator.templates.utils;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class Utils {

    public static String getSimpleName(String entityName) {
        return getSimpleName(entityName, entityName.lastIndexOf("."));
    }

    public static String getSimpleName(String entityName, Integer dotIndex) {
        return entityName.substring(dotIndex + 1);
    }

    public static String getPackage(String entityName) {
        return getPackage(entityName, entityName.lastIndexOf("."));
    }

    public static String getPackage(String entityName, Integer dotIndex) {
        return entityName.substring(0, dotIndex);
    }

    public static String makeCamelName(String name) {
        return name.substring(0, 1).toUpperCase().concat(name.substring(1));
    }

    public static String makeLowerFirstLetter(String name) {
        return name.substring(0, 1).toLowerCase().concat(name.substring(1));
    }

    public static String makeLettersByRegex(String entityName, String regex, String replacement) {
        return (entityName.substring(0, 1) + entityName.substring(1).replaceAll(regex, replacement));
    }

    public static EntityMapping find(RenderingContext ctx, String entityName) {
        return ctx.mappings.get(entityName);
    }

    public static Set<String> resolvePropsTypes(EntityMapping m) {
        return m.props.values().stream().map(p -> p.type).collect(Collectors.toSet());
    }

    public static Set<String> makeEnumValues(List<String> fields) {
        return fields.stream().map(
                p -> p.replaceAll("([A-Z])", "_$1").toUpperCase())
                .collect(Collectors.toSet());
    }
}
