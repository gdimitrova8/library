package code.generator.templates.exchanger;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import static code.generator.templates.dto.DtoRenderer.DTO_TYPE_META;
import static code.generator.templates.exchanger.DtoExchangerRenderer.DTO_EXCHANGER_TYPE_META;
import code.generator.templates.JavaTemplateRenderer;
import code.generator.templates.utils.Utils;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class DtoExchangerRegistryRenderer extends JavaTemplateRenderer {

    public final static String DTO_EXCHANGER_REGISTRY_TYPE_PACKAGE = "com.library.dto.exchanger";

    public final static String DTO_EXCHANGER_REGISTRY_NAME = "DtoExchangerRegistry";

    public final static String DTO_EXCHANGER_REGISTRY_TYPE = DTO_EXCHANGER_REGISTRY_TYPE_PACKAGE + "." + DTO_EXCHANGER_REGISTRY_NAME;

    private final static Integer DTO_EXCHANGER_TYPE = 0;

    private final static Integer DTO_EXCHANGER_NAME = 1;

    private final static Integer DTO_EXCHANGER_CAMEL_NAME = 2;

    private Map<String, Map<Integer, String>> makeCache(List<EntityMapping> filteredMappings) {
        Map<String, Map<Integer, String>> cache = new HashMap<>();
        filteredMappings.forEach(m -> {
            String dtoExchangerType = m.meta.get(DTO_EXCHANGER_TYPE_META);
            String dtoExchangerName = getSimpleName(dtoExchangerType);
            String camelName = Utils.makeCamelName(dtoExchangerName);
            Map<Integer, String> mappingCache = new HashMap<>();
            mappingCache.put(DTO_EXCHANGER_TYPE, dtoExchangerType);
            mappingCache.put(DTO_EXCHANGER_NAME, dtoExchangerName);
            mappingCache.put(DTO_EXCHANGER_CAMEL_NAME, camelName);
            cache.put(m.entityType, mappingCache);
        });
        return cache;
    }

    @Override
    protected Set<String> getRequiredMeta() {
        return new HashSet() {
            {
                add(DTO_TYPE_META);
                add(DTO_EXCHANGER_TYPE_META);
            }
        };
    }

    @Override
    public void render(RenderingContext ctx, String destination) throws Exception {
        info("\t\tGenerating Dto exchanger registry: " + DTO_EXCHANGER_REGISTRY_TYPE);
        info("");
        List<EntityMapping> mappings = filter(ctx.mappings.values());
        Map<String, Map<Integer, String>> cache = makeCache(mappings);

        printLine("package " + getPackage(DTO_EXCHANGER_REGISTRY_TYPE) + ";\n");
//        cache.values().forEach(mCache -> {
//            printLine("import " + mCache.get(DTO_EXCHANGER_TYPE) + ";");
//        });
        printInfo(this.getClass());
        printLine("public class " + DTO_EXCHANGER_REGISTRY_NAME + " {");
        printLine();
        cache.values().forEach(mCache -> {
            String name = mCache.get(DTO_EXCHANGER_NAME);
            printLine(offset + "public " + name + " get" + mCache.get(DTO_EXCHANGER_CAMEL_NAME) + "() {");
            printLine(offset2 + "return " + name + ".INSTANCE;");
            printLine(offset + "}");
            printLine();
        });
        printLine("}");
        saveFile(destination, DTO_EXCHANGER_REGISTRY_TYPE);
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (!m.meta.getOrDefault("is.class", false) || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        // nothing
    }

}
