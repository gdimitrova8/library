package code.generator.templates.exchanger;

import code.generator.plugin.mojo.renderer.context.Meta;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Property;
import static code.generator.templates.dto.DtoRenderer.DTO_TYPE_META;
import code.generator.templates.JavaTemplateRenderer;
import code.generator.templates.utils.Utils;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class DtoExchangerRenderer extends JavaTemplateRenderer {

    public final static String MAIN_DTO_EXCHANGER_TYPE = "com.library.dto.exchanger.DtoEntityExchanger";

    public final static String DTO_EXCHANGER_TYPE_META = "dto.exchanger";

    public final static String DTO_EXCHANGER_PARENT_TYPE_META = "dto.exchanger.parent";

    public final static String DTO_PACKAGE = "com.library.dto";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add(DTO_TYPE_META);
            add(DTO_EXCHANGER_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!mapping.meta.getOrDefault("generate.dto.exchanger", true)) {
            return;
        }
        String dtoExchangerType = mapping.meta.get(DTO_EXCHANGER_TYPE_META);
        info("\t\tGenerating Dto exchanger: " + dtoExchangerType);
        info("");

        Collection<Property> props = mapping.props.values();
        String exchangerTypeName = getSimpleName(dtoExchangerType);
        String parentType = mapping.meta.getOrDefault(DTO_EXCHANGER_PARENT_TYPE_META, MAIN_DTO_EXCHANGER_TYPE);
        String parentTypeName = getSimpleName(parentType);
        String dtoType = mapping.meta.get(DTO_TYPE_META);
        String dtoTypeName = getSimpleName(dtoType);
        String entityType = mapping.entityType;
        String entityTypeName = getSimpleName(entityType);
        boolean isAbstract = mapping.meta.getOrDefault("is.abstract", false);

        printLine("package " + getPackage(dtoExchangerType) + ";\n");
        printLine("import " + dtoType + ";");
        printLine("import " + entityType + ";");
        printImports(props, ctx, s -> !s.startsWith("java."));

        printInfo(this.getClass());

        printDeclaration(isAbstract, exchangerTypeName, parentTypeName, dtoTypeName, entityTypeName);
        printLine();

        Boolean isMainParent = parentType.equals(MAIN_DTO_EXCHANGER_TYPE);
        if (!isAbstract) {
            printLine(offset + "public final static " + exchangerTypeName + " INSTANCE = new " + exchangerTypeName + "();");
            printLine();
            printLine(offset + "public " + exchangerTypeName + "() {");
            printLine(offset2 + "// Singleton");
            printLine(offset + "}");
            printLine();
        }
        if (isAbstract) {
            printExchange("E", "V", ctx, props, false, isMainParent);
            printLine();
            printExchange("V", "E", ctx, props, true, isMainParent);
            printLine();
        } else {
            printExchangeMethod(entityTypeName, dtoTypeName, ctx, props, false, isMainParent);
            printLine();
            printExchangeMethod(dtoTypeName, entityTypeName, ctx, props, true, isMainParent);
            printLine();
        }
        printLine("}");

        saveFile(destination, dtoExchangerType);
    }

    private void printImports(Collection<Property> props, RenderingContext ctx, Predicate<String> filter) {
        Set<String> types = new HashSet<>();
        for (Property p : props) {
            EntityMapping m = ctx.mappings.get(p.type);
            if (m == null) {
                continue;
            }
            Meta meta = m.meta;
            if (meta.getOrDefault("is.enum", false)) {
                types.add(p.type);
                types.add(meta.get(DTO_TYPE_META));
                continue;
            }
            types.add(meta.get(DTO_EXCHANGER_TYPE_META));
        }

        Set<String> imports = types.stream().filter(filter)
                .map(t -> "import " + t).collect(Collectors.toSet());
        if (!imports.isEmpty()) {
            print(String.join(";\n", imports).concat(";\n"));
        }
    }

    private void printDeclaration(Boolean isAbstract, String exchangerTypeName, String parent, String otherType, String domainType) {
        if (isAbstract) {
            printLine("public abstract class "
                    + exchangerTypeName + "<V extends " + otherType + ",E extends " + domainType
                    + "> extends " + parent + "<V,E> {");
            return;
        }
        printLine("public class " + exchangerTypeName + " extends " + parent + "<" + otherType + ", " + domainType + "> {");
    }

    private void printExchange(String toType, String fromType, RenderingContext ctx,
            Collection<Property> props, Boolean fromMeta, Boolean isMainParent) {
        printLine(offset + "protected void exchange(" + toType + " to, " + fromType + " from) {");
        printExchanges(ctx, props, fromMeta, isMainParent);
        printLine(offset + "}");

    }

    private void printExchangeMethod(String toType, String fromType, RenderingContext ctx,
            Collection<Property> props, Boolean fromMeta, Boolean isMainParent) {
        printLine(offset + "@Override");
        printLine(offset + "protected " + toType + " exchangeFrom(" + fromType + " from) {");
        printLine(offset2 + toType + " to = new " + toType + "();");
        printExchanges(ctx, props, fromMeta, isMainParent);
        printLine(offset2 + "return to;");
        printLine(offset + "}");

    }

    private void printExchanges(RenderingContext ctx, Collection<Property> props, Boolean fromMeta, Boolean isMainParent) {
        if (!isMainParent) {
            printLine(offset2 + "super.exchange(to, from);");
        }
        props.forEach((p) -> {
            printPropertyExchange(p, ctx, fromMeta);
        });
    }

    private void printPropertyExchange(Property p, RenderingContext ctx, Boolean fromMeta) {
        String name = p.name;
        String camelName = Utils.makeCamelName(name);
        String dtoType = getPropType(p, ctx, DTO_TYPE_META);
        String dtoTypeName = getSimpleName(dtoType);
        String propTypeName = getSimpleName(p.type);
        if (isEnum(p, ctx)) {
            printLine(offset2 + "to.set" + camelName + "(" + (fromMeta ? dtoTypeName : propTypeName)
                    + ".valueOf(from.get" + camelName + "().name()));");
            return;
        }
        if (dtoType.startsWith(DTO_PACKAGE)) {
            String camel = Utils.makeCamelName(getPropSimpleName(p, ctx, DTO_EXCHANGER_TYPE_META));
            printLine(offset2 + "exchange" + (fromMeta && p.collectionType != null ? "All" : "")
                    + "(registry::get" + camel + ", to::set" + camelName + ", from.get" + camelName + "());");
            return;
        }
        if (areDates(dtoTypeName, propTypeName)) {
            printLine(offset2 + "exchange(to::set" + camelName + ", from.get" + camelName + "());");
            return;
        }
        printLine(offset2 + "exchange(to::set" + camelName + ", from::get" + camelName + ");");
    }

    private boolean areDates(String dtoTypeName, String propTypeName) {
        return (dtoTypeName.equals("Calendar") && propTypeName.equals("Long"))
                || (propTypeName.equals("Calendar") && dtoTypeName.equals("Long"));
    }

}
