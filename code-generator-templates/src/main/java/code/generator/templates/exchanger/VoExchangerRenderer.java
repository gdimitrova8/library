package code.generator.templates.exchanger;

import code.generator.plugin.mojo.renderer.context.Meta;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Property;
import code.generator.templates.JavaTemplateRenderer;
import static code.generator.templates.rest.VoRenderer.VO_TYPE_META;
import code.generator.templates.utils.Utils;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class VoExchangerRenderer extends JavaTemplateRenderer {

    public final static String MAIN_VO_EXCHANGER_TYPE = "com.library.bl.vo.exchanger.VoEntityExchanger";

    public final static String OTHER_MAIN_VO_EXCHANGER_TYPE = "com.library.bl.vo.exchanger.AbstractVoEntityExchanger";

    public final static String VO_EXCHANGER_TYPE_META = "vo.exchanger";

    public final static String VO_EXCHANGER_PARENT_TYPE_META = "vo.exchanger.parent";

    public final static String REST_PACKAGE = "com.library.rest.api";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add(VO_TYPE_META);
            add(VO_EXCHANGER_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!mapping.meta.getOrDefault("generate.vo.exchanger", true)) {
            return;
        }
        String voExchangerType = mapping.meta.get(VO_EXCHANGER_TYPE_META);
        info("\t\tGenerating VO exchanger: " + voExchangerType);
        info("");

        Collection<Property> props = mapping.props.values();
        String exchangerTypeName = getSimpleName(voExchangerType);
        String parentType = mapping.meta.getOrDefault(VO_EXCHANGER_PARENT_TYPE_META, MAIN_VO_EXCHANGER_TYPE);
        String parentTypeName = getSimpleName(parentType);
        String voType = mapping.meta.get(VO_TYPE_META);
        String voTypeName = getSimpleName(voType);
        String entityType = mapping.entityType;
        String entityTypeName = getSimpleName(entityType);
        boolean isAbstract = mapping.meta.getOrDefault("is.abstract", false);

        printLine("package " + getPackage(voExchangerType) + ";\n");
        printLine("import " + voType + ";");
        printLine("import " + entityType + ";");
        printImports(props, ctx, s -> !s.startsWith("java."));

        printInfo(this.getClass());

        printDeclaration(isAbstract, exchangerTypeName, parentTypeName, voTypeName, entityTypeName);
        printLine();

        Boolean isMainParent = parentType.equals(MAIN_VO_EXCHANGER_TYPE) || parentType.equals(OTHER_MAIN_VO_EXCHANGER_TYPE);
        if (!isAbstract) {
            printLine(offset + "public final static " + exchangerTypeName + " INSTANCE = new " + exchangerTypeName + "();");
            printLine();
            printLine(offset + "public " + exchangerTypeName + "() {");
            printLine(offset2 + "// Singleton");
            printLine(offset + "}");
            printLine();
            printExchangeMethod(entityTypeName, voTypeName, ctx, props, false, isMainParent);
            printLine();
            printExchangeMethod(voTypeName, entityTypeName, ctx, props, true, isMainParent);
            printLine();
        } else {
            printExchange("E", "V", ctx, props, false, isMainParent);
            printLine();
            printExchange("V", "E", ctx, props, true, isMainParent);
            printLine();
        }
        printLine("}");

        saveFile(destination, voExchangerType);
    }

    private void printImports(Collection<Property> props, RenderingContext ctx, Predicate<String> filter) {
        Set<String> types = new HashSet<>();
        for (Property p : props) {
            EntityMapping m = ctx.mappings.get(p.type);
            if (m == null) {
                continue;
            }
            Meta meta = m.meta;
            if (meta.getOrDefault("is.enum", false)) {
                types.add(p.type);
                types.add(meta.get(VO_TYPE_META));
                continue;
            }
            types.add(meta.get(VO_EXCHANGER_TYPE_META));
        }

        Set<String> imports = types.stream().filter(filter)
                .map(t -> "import " + t).collect(Collectors.toSet());
        if (!imports.isEmpty()) {
            print(String.join(";\n", imports).concat(";\n"));
        }
    }

    private void printDeclaration(Boolean isAbstract, String exchangerTypeName, String parent, String otherType, String domainType) {
        if (isAbstract) {
            printLine("public abstract class "
                    + exchangerTypeName + "<V extends " + otherType + ",E extends " + domainType
                    + "> extends " + parent + "<V,E> {");
            return;
        }
        printLine("public class " + exchangerTypeName + " extends " + parent + "<" + otherType + ", " + domainType + "> {");
    }

    private void printExchange(String toType, String fromType, RenderingContext ctx,
            Collection<Property> props, Boolean fromMeta, Boolean isMainParent) {
        printLine(offset + "protected void exchange(" + toType + " to, " + fromType + " from) {");
        printExchanges(ctx, props, fromMeta, isMainParent);
        printLine(offset + "}");

    }

    private void printExchangeMethod(String toType, String fromType, RenderingContext ctx,
            Collection<Property> props, Boolean fromMeta, Boolean isMainParent) {
        printLine(offset + "@Override");
        printLine(offset + "protected " + toType + " exchangeFrom(" + fromType + " from) {");
        printLine(offset2 + toType + " to = new " + toType + "();");
        printExchanges(ctx, props, fromMeta, isMainParent);
        printLine(offset2 + "return to;");
        printLine(offset + "}");

    }

    private void printExchanges(RenderingContext ctx, Collection<Property> props, Boolean fromMeta, Boolean isMainParent) {
        if (!isMainParent) {
            printLine(offset2 + "super.exchange(to, from);");
        }
        props.forEach((p) -> {
            printPropertyExchange(p, ctx, fromMeta);
        });
    }

    private void printPropertyExchange(Property p, RenderingContext ctx, Boolean fromMeta) {
        String name = p.name;
        String camelName = Utils.makeCamelName(name);
        String voType = getPropType(p, ctx, VO_TYPE_META);
        String voTypeName = getSimpleName(voType);
        String propTypeName = getSimpleName(p.type);
        if (isEnum(p, ctx)) {
            printLine(offset2 + "to.set" + camelName + "(" + (fromMeta ? voTypeName : propTypeName)
                    + ".valueOf(from.get" + camelName + "().name()));");
            return;
        }
        if (voType.startsWith(REST_PACKAGE)) {
            String camel = Utils.makeCamelName(getPropSimpleName(p, ctx, VO_EXCHANGER_TYPE_META));
            printLine(offset2 + "exchange" + (fromMeta && p.collectionType != null ? "All" : "")
                    + "(registry::get" + camel + ", to::set" + camelName + ", from.get" + camelName + "());");
            return;
        }
        if (areDates(voTypeName, propTypeName)) {
            printLine(offset2 + "exchange(to::set" + camelName + ", from.get" + camelName + "());");
            return;
        }
        printLine(offset2 + "exchange(to::set" + camelName + ", from::get" + camelName + ");");
    }

    private boolean areDates(String voTypeName, String propTypeName) {
        return (voTypeName.equals("Calendar") && propTypeName.equals("Long"))
                || (propTypeName.equals("Calendar") && voTypeName.equals("Long"));
    }

}
