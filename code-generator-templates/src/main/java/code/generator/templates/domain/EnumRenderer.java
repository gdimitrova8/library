package code.generator.templates.domain;

import code.generator.templates.JavaTemplateRenderer;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Property;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class EnumRenderer extends JavaTemplateRenderer {

    @Override
    public void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        info("\t\tGenerating Enum : " + mapping.entityType);
        info("");
        Collection<Property> props = mapping.props.values();
        printLine("package " + getPackage(mapping.entityType) + ";\n");

        printInfo(this.getClass());
        printDeclaration(mapping.meta, getSimpleName(mapping.entityType));
        printLine();
        printLine(offset + String.join(", ", props.stream().map(p -> p.name).collect(Collectors.toSet())));
        printLine();
        printLine("}");

        saveFile(destination, mapping.entityType);
    }

    @Override
    protected Set<String> getRequiredMeta() {
        return new HashSet() {
            {
                add("is.enum");
            }
        };
    }
}
