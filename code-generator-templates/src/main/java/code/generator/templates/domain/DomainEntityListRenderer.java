package code.generator.templates.domain;

import code.generator.templates.JavaTemplateRenderer;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.templates.ResourceTemplateRenderer;
import static code.generator.templates.domain.DomainEntityRenderer.DOMAIN_PACKAGE_PREFIX;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author gdimitrova
 */
public class DomainEntityListRenderer extends JavaTemplateRenderer {

    public final static String DOMAIN_LIST_PACKAGE = DOMAIN_PACKAGE_PREFIX + ".list";

    public final static String TEMPLATE_RESOURCE = "EntityListTemplate.txt";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add("is.class");
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!(mapping.meta.getOrDefault("is.class", false) && !mapping.meta.getOrDefault("is.abstract", false))) {
            return;
        }
        String entityType = mapping.entityType;
        String entityName = getSimpleName(mapping.entityType);
        String listEntityName = entityName.concat("sList");
        String listEntityType = makeType(entityName);
        info("\t\tGenerating Domain List entity : " + listEntityType);
        info("");

        Map<String, String> map = new HashMap<>();
        map.put("%LIST_PKG%", DOMAIN_LIST_PACKAGE);
        map.put("%PARENT_LIST_PKG%", DOMAIN_LIST_PACKAGE + ".EntityList");
        map.put("%ENTITY_PKG%", entityType);
        map.put("%RENDERER%", this.getClass().getName() + ".java");
        map.put("%LIST_TYPE_NAME%", listEntityName);
        map.put("%PARENT_LIST%", "EntityList");
        map.put("%ENTITY_NAME%", entityName);
      
        ResourceTemplateRenderer.INSTANCE.render(
                TEMPLATE_RESOURCE,
                map,
                destination,
                listEntityType,
                "java",
                writer
        );
    }

    public static String makeType(String entityName) {
        return DOMAIN_LIST_PACKAGE + "." + makeName(entityName);
    }

    public static String makeName(String entityName) {
        return entityName + "sList";
    }

}
