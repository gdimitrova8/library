package code.generator.templates.domain;

import code.generator.templates.JavaTemplateRenderer;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Method;
import code.generator.plugin.mojo.renderer.context.Property;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

/**
 *
 * @author gdimitrova
 */
public class DomainEntityRenderer extends JavaTemplateRenderer {

    public final static String DOMAIN_PACKAGE_PREFIX = "com.library.domain";

    public final static String MSG_NAME_META = "msg.name";

    public final static String MSG_KEY_PREFIX_META = "msg.key.suffix";

    public final static Set<String> META_MSG_KEYS = new HashSet<>() {
        {
            add(MSG_NAME_META);
            add(MSG_KEY_PREFIX_META);
        }
    };

    public final static Predicate<Property> HAS_MSG_META = p -> p.meta.keySet().containsAll(META_MSG_KEYS);

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add("is.class");
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    public void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!mapping.meta.getOrDefault("generate.domain", true)) {
            return;
        }
        info("\t\tGenerating Domain entity : " + mapping.entityType);
        info("");

        Collection<Property> props = mapping.props.values();
        List<EntityMapping> parents = getParents(ctx, mapping);
        EntityMapping[] filteredParents = filterParents(parents);
        List<Property> msgProps = findMessageProps(mapping);
        String entityPackage = getPackage(mapping.entityType);
        Method equalsMethod = findMethod(mapping, "equals");
        Method hashCode = findMethod(mapping, "hashCode");

        printLine("package " + entityPackage + ";\n");

        if (hashCode != null) {
            printLine("import java.util.Objects;");
        }
        if (!msgProps.isEmpty()) {
            printLine("import " + DOMAIN_PACKAGE_PREFIX + ".message.Message;");
            printLine("import " + DOMAIN_PACKAGE_PREFIX + ".validation.ValidationUtils;");
            printLine("import java.util.ArrayList;");
            printLine("import java.util.List;");
        }
        printImports(mapping, ctx);
        printInfo(this.getClass());
        printDeclaration(mapping.meta, getSimpleName(mapping.entityType), "parent", "implements");
        printLine();
        if (!msgProps.isEmpty()) {
            printMessageProps(msgProps, mapping.entityType);
        }
        printProps(props);
        if (mapping.meta.getOrDefault("has.constructors", true)) {
            printDefaultConstructor(mapping);
            printConstructor(mapping, ctx, "simple.name", filteredParents);
        }
        printAccessors(props, ctx);
        if (hashCode != null) {
            printHashCode(hashCode, props);
        }
        if (equalsMethod != null) {
            printEquals(mapping, props);
        }
        if (!msgProps.isEmpty()) {
            printValidateMethod(msgProps, parents);
        }
        printLine("}");

        saveFile(destination, mapping.entityType);
    }

    private void printMessageProps(List<Property> props, String entityType) {
        props.forEach(p -> {
            printLine(offset + "public final static Message " + p.meta.get("msg.name") + " = new Message(");
            printLine(offset + "\t \"" + entityType + "." + p.meta.get("msg.key.suffix") + "\"");
            printLine(offset + ");\n");
        });
    }

    private void printValidateMethod(List<Property> props, List<EntityMapping> parents) {
        printLine(offset + "@Override");
        printLine(offset + "public List<Message> validate() {");
        if (parents == null || parents.isEmpty() || !hasValidateMethod(parents.get(0))) {
            printLine(offset2 + "List<Message> errors = new ArrayList<>();");
        } else {
            printLine(offset2 + "List<Message> errors = super.validate();");
        }
        props.forEach((p) -> {
            printLine(offset2 + "ValidationUtils.validate(" + p.name + ", " + p.meta.get("msg.name") + ", errors);");
        });
        printLine(offset2 + "return errors;");
        printLine(offset + "}\n");
    }

    private boolean hasValidateMethod(EntityMapping parent) {
        return findProperty(parent, HAS_MSG_META) != null;
    }

    private List<Property> findMessageProps(EntityMapping mapping) {
        return filter(mapping, HAS_MSG_META);
    }
}
