package code.generator.templates.android;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.templates.AbstractTemplateRenderer;
import static code.generator.templates.rest.RootResourceRenderer.makePath;
import code.generator.templates.rest.VoRenderer;
import static code.generator.templates.rest.VoRenderer.VO_TYPE_META;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class ViewModelRenderer extends AbstractTemplateRenderer {

    public final static String VIEW_MODEL_PACKAGE = "com.library.ui.view_model";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add("is.class");
            add(VO_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (!m.meta.getOrDefault("generate.vo", true)
                    || !m.meta.getOrDefault("is.class", false)
                    || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        String entityName = getSimpleName(mapping.entityType);
        String viewModelType = VIEW_MODEL_PACKAGE + "." + entityName + "ViewModel";
        String voType = mapping.meta.get(VO_TYPE_META);
        String voName = getSimpleName(voType);
        String filterType = VoRenderer.makeFilterType(entityName);
        String filterName = getSimpleName(filterType);
        String listType = VoRenderer.makeListType(entityName);
        String listName = getSimpleName(listType);

        info("\t\tGenerating ViewModel class: " + viewModelType);

        printLine("package " + VIEW_MODEL_PACKAGE + ";");
        printLine();
        printLine("import android.app.Application;");
        printLine();
        printLine("import androidx.annotation.NonNull;");
        printLine();
        //    printLine("import " + filterType + "; ");
        printLine("import " + voType + "; ");
        printLine("import " + listType + "; ");
        printLine();
        printInfo(this.getClass());
        printLine("public class " + entityName + "ViewModel extends AbstractViewModel<"
                //          + filterName + ", "
                + voName + ", "
                + listName + "> {");
        printLine();
        printLine(offset + "public " + entityName + "ViewModel(@NonNull Application application) {");

        printLine(offset2 + "super("
                + voName + ".class, "
                + listName + ".class, \""
                + makePath(entityName) + "\", application);");
        printLine(offset + "}");
        printLine();
        printLine("}");

        saveFile(destination, "java." + viewModelType, "txt");
    }

}
