package code.generator.templates.android;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.templates.AbstractTemplateRenderer;
import code.generator.templates.ResourceTemplateRenderer;
import static code.generator.templates.rest.VoRenderer.FILTER_VO_PACKAGE;
import static code.generator.templates.rest.VoRenderer.VO_TYPE_META;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class SearchEntityViewRenderer extends AbstractTemplateRenderer {

    public final static String TEMPLATE_RESOURCE = "SearchEntityViewTemplate.txt";

    public final static String ENTITY_TYPE = "java.com.library.ui.activity.SearchEntityView";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add(VO_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (!m.meta.getOrDefault("generate.vo", true) 
                    || !m.meta.getOrDefault("is.class", false) 
                    || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        // nothing
    }

    @Override
    public void render(RenderingContext ctx, String destination) throws Exception {
        info("\t\tGenerating SearchEntityView implementation : " + ENTITY_TYPE);
        List<EntityMapping> filteredMappings = filter(ctx.mappings.values());

        StringBuilder sb = new StringBuilder();
        StringBuilder imports = new StringBuilder();
        for (EntityMapping m : filteredMappings) {
            String voType = m.meta.get(VO_TYPE_META);
            String entityName = getSimpleName(m);

            imports.append("import " + FILTER_VO_PACKAGE + "." + entityName + "SearchFieldsVo;\n");

            sb.append(offset2 + "if (name.equals(" + voType + ".class.getName())) {\n");
            sb.append(offset3 + "init(new FilterVo(0, 30, new SearchCfgVo()), "
                    + entityName + "SearchFieldsVo.values(), " + entityName + "ListView.class);\n");
            sb.append(offset3 + "return;\n");
            sb.append(offset2 + "}\n");

        }
        Map<String, String> map = new HashMap<>();
        map.put("%IMPORTS%", imports.toString());
        map.put("%INIT_CONDITIONS%", sb.toString());
        ResourceTemplateRenderer.INSTANCE.render(
                TEMPLATE_RESOURCE,
                map,
                destination,
                ENTITY_TYPE,
                "txt",
                writer
        );

    }

}
