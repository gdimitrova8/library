package code.generator.templates.android;

import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.templates.AbstractTemplateRenderer;
import code.generator.templates.ResourceTemplateRenderer;
import static code.generator.templates.rest.VoRenderer.VO_TYPE_META;
import code.generator.templates.utils.Utils;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class ListViewXMLRenderer extends AbstractTemplateRenderer {

    public final static String TEMPLATE_RESOURCE = "ListViewXMLTemplate.txt";

    public final static String PACKAGE = "res.layout";
    public final static String LIST_ITEM_META = "list.item.name";
    public final static String DEFAULT_LIST_ITEM_NAME = "named_item";

    private final static Set<String> REQUIRED_META = new HashSet() {
        {
            add(VO_TYPE_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    protected List<EntityMapping> filter(Collection<EntityMapping> mappings) {
        return mappings.stream().filter(m -> {
            if (!m.meta.getOrDefault("generate.vo", true)
                    || !m.meta.getOrDefault("is.class", false)
                    || m.meta.getOrDefault("is.abstract", false)) {
                return false;
            }
            return containsRequiredMeta(m);
        }).collect(Collectors.toList());
    }

    @Override
    protected void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        String entityName = getSimpleName(mapping);
        String list_name =  Utils.makeLettersByRegex(entityName,"([A-Z])", "_$1").toLowerCase();
        String list_view_name = list_name.concat("_list_view").toLowerCase();

        String listViewType = PACKAGE + "." + list_view_name;

        info("\t\tGenerating ListView implementation : " + listViewType);

        Map<String, String> map = new HashMap<>();
        map.put("WorkForm", entityName);
        map.put("work_form", list_name);
        map.put(DEFAULT_LIST_ITEM_NAME, mapping.meta.getOrDefault(LIST_ITEM_META, DEFAULT_LIST_ITEM_NAME));

        ResourceTemplateRenderer.INSTANCE.render(
                TEMPLATE_RESOURCE,
                map,
                destination,
                listViewType,
                "xml",
                writer
        );
    }

}
