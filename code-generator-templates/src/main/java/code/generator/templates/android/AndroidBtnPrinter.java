package code.generator.templates.android;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 *
 * @author gdimitrova
 */
public class AndroidBtnPrinter {

    public static String OFFSET = "    ";

    public static String ID = "id";

    public static String LAYOUT_WIDTH = "layout_width";

    public static String LAYOUT_HEIGHT = "layout_height";

    public static String WIDTH = "width";

    public static String HEIGHT = "height";

    public static String BACKGROUND_TINT = "backgroundTint";

    public static String TEXT = "text";

    public static String TEXT_COLOR = "textColor";

    public final static Map<String, String> DEFAULT = new LinkedHashMap<>() {
        {
            put(ID, "@+id/some_btn");
            put(LAYOUT_WIDTH, "match_parent");
            put(LAYOUT_HEIGHT, "wrap_content");
            put(WIDTH, "5dp");
            put(HEIGHT, "10dp");
            put(BACKGROUND_TINT, "#104073");
            put(TEXT, "@string/some_btn_name");
            put(TEXT_COLOR, "#FFFFFF");
        }
    };

    public static void print(Map<String, String> attrMap, Consumer<String> printLine) {
        print(attrMap, "\t", printLine);
    }

    public static void print(Map<String, String> attrMap, String offset, Consumer<String> printLine) {
        printLine.accept(offset + "<Button");
        attrMap.entrySet().forEach((entry) -> {
            printLine.accept(offset + OFFSET + "android:" + entry.getKey() + "=\"" + entry.getValue() + "\"");
        });
        printLine.accept(offset + OFFSET + "/>");
    }
}
