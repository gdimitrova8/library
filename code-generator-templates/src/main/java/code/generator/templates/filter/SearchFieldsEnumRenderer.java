package code.generator.templates.filter;

import code.generator.templates.JavaTemplateRenderer;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import static code.generator.templates.filter.FilterFieldsResolver.FILTER_FIELDS_META;
import static code.generator.templates.utils.Utils.makeEnumValues;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author gdimitrova
 */
public class SearchFieldsEnumRenderer extends JavaTemplateRenderer {

    public final static String FILTER_PKG = "com.library.domain.filter";

    public final static Set<String> REQUIRED_META = new HashSet() {
        {
            add("is.class");
            add(FILTER_FIELDS_META);
        }
    };

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    public void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!(mapping.meta.getOrDefault("is.class", false) && !mapping.meta.getOrDefault("is.abstract", false))) {
            return;
        }
        String entityName = getSimpleName(mapping.entityType);
        String entityType = FILTER_PKG + "." + entityName + "SearchFields";
        info("\t\tGenerating search fields enum : " + entityType);
        info("");

        List<String> fields = FilterFieldsResolver.INSTANCE.resolve(mapping);

        printLine("package " + FILTER_PKG + ";\n");
        printInfo(this.getClass());
        printLine("public enum " + entityName + "SearchFields {");
        printLine();
        printLine(offset + "NONE, " + String.join(", ", makeEnumValues(fields)));
        printLine();
        printLine("}");

        saveFile(destination, entityType);
    }

}
