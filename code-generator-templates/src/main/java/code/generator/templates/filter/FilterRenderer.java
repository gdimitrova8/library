package code.generator.templates.filter;

import code.generator.templates.JavaTemplateRenderer;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import static code.generator.templates.filter.SearchFieldsEnumRenderer.REQUIRED_META;
import java.util.Set;

/**
 *
 * @author gdimitrova
 */
public class FilterRenderer extends JavaTemplateRenderer {

    public final static String FILTER_PKG = "com.library.domain.filter";

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    public void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!(mapping.meta.getOrDefault("is.class", false) && !mapping.meta.getOrDefault("is.abstract", false))) {
            return;
        }
        String entityName = getSimpleName(mapping.entityType);

        String entityType = makeType(entityName);
        info("\t\tGenerating filter entity : " + entityType);
        info("");

        String searchCfgName = "SearchConfig";//SearchConfigRenderer.makeName(entityName);

        printLine("package " + FILTER_PKG + ";");
        printLine();
        printLine("import " + FILTER_PKG + "." + searchCfgName + ";\n");
        printLine("import " + FILTER_PKG + ".Filter;\n");
        printInfo(this.getClass());
        printLine("public class " + entityName + "sFilter extends Filter {");
        printLine();
        printDefaultConstructor(entityName + "sFilter");
        printLine(offset + "public " + entityName + "sFilter(Integer startIndex, Integer maxResults, "
                + searchCfgName + " cfg) {");
        printLine(offset2 + "super(startIndex, maxResults, cfg);");
        printLine(offset + "}");
        printLine();
        printLine("}");

        saveFile(destination, entityType);
    }

    public static String makeType(String entityName) {
        return FILTER_PKG + "." + makeName(entityName);
    }

    public static String makeName(String entityName) {
        return entityName + "sFilter";
    }
}
