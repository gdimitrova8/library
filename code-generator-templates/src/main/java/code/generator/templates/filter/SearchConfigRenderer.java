package code.generator.templates.filter;

import code.generator.templates.JavaTemplateRenderer;
import code.generator.plugin.mojo.renderer.context.RenderingContext;
import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Method;
import code.generator.templates.ResourceTemplateRenderer;
import static code.generator.templates.filter.SearchFieldsEnumRenderer.FILTER_PKG;
import static code.generator.templates.filter.SearchFieldsEnumRenderer.REQUIRED_META;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author gdimitrova
 */
public class SearchConfigRenderer extends JavaTemplateRenderer {

    public final static String TEMPLATE_RESOURCE = "SearchConfigTemplate.txt";

    @Override
    protected Set<String> getRequiredMeta() {
        return REQUIRED_META;
    }

    @Override
    public void render(RenderingContext ctx, EntityMapping mapping, String destination) throws Exception {
        if (!(mapping.meta.getOrDefault("is.class", false) && !mapping.meta.getOrDefault("is.abstract", false))) {
            return;
        }
        String entityName = getSimpleName(mapping.entityType);

        String entityType = FILTER_PKG + "." + entityName + "SearchConfig";
        info("\t\tGenerating SearchConfig entity : " + entityType);
        info("");

        Method m = findMethod(mapping, "hashCode");

        Map<String, String> map = new HashMap<>();
        map.put("%PACKAGE%", FILTER_PKG);
        map.put("%ENTITY_S_CFG%", entityName + "SearchConfig");
        map.put("%ENTITY_S_FIELDS%", entityName + "SearchFields");
        map.put("%CLASS_OR_INTERFACE%", "implements SearchCfg");
        map.put("%IMPORTS%", "");
        map.put("%ANNOTATION%", "");
        map.put("%HASH%", m.meta.get("hash").toString());
        map.put("%HASH_NUM%", m.meta.get("hash.number").toString());

        renderSearchCfg(map, destination, entityType, writer);
    }

    public static void renderSearchCfg(Map<String, String> map, String destination, String entityType, StringWriter writer) throws Exception {

        ResourceTemplateRenderer.INSTANCE.render(
                TEMPLATE_RESOURCE,
                map,
                destination,
                entityType,
                "java",
                writer
        );
    }
    public static void printSearchCfg(Map<String, String> map, StringWriter writer) throws Exception {
        ResourceTemplateRenderer.INSTANCE.print(
                TEMPLATE_RESOURCE,
                map,
                writer
        );
    }

    public static String makeType(String entityName) {
        return FILTER_PKG + "." + makeName(entityName);
    }

    public static String makeName(String entityName) {
        return entityName + "SearchConfig";
    }
}
