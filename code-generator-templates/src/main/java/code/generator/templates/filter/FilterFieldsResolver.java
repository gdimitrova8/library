package code.generator.templates.filter;

import code.generator.plugin.mojo.renderer.context.EntityMapping;
import code.generator.plugin.mojo.renderer.context.Property;
import edu.emory.mathcs.backport.java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 */
public class FilterFieldsResolver {

    public final static String FILTER_FIELDS_META = "filter.fields";

    public final static String FILTER_FIELDS_EXL_META = "filter.fields.exclude";

    public static FilterFieldsResolver INSTANCE = new FilterFieldsResolver();

    private FilterFieldsResolver() {
    }

    public List<String> resolve(EntityMapping mapping) {
        Collection<Property> props = mapping.props.values();
        String fieldsValue = mapping.meta.get(FILTER_FIELDS_META);
        List<String> fields = new ArrayList<>();
        if (!fieldsValue.equals("*")) {
            fields.addAll(Arrays.asList(fieldsValue.split(",")));
            return fields;
        }
        fields = props.stream().map(p -> p.name).collect(Collectors.toList());
        String fieldsExclude = mapping.meta.get(FILTER_FIELDS_EXL_META);
        if (fieldsExclude != null) {
            fields.removeAll(Arrays.asList(fieldsExclude.split(",")));
        }
        return fields;
    }

}
