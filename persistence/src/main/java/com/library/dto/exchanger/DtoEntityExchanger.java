package com.library.dto.exchanger;

import com.library.domain.Entity;
import com.library.domain.exchanger.AbstractEntityExchanger;
import com.library.dto.AbstractDto;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 *
 * @author gdimitrova
 * @param <Dto>
 * @param <E>
 */
public abstract class DtoEntityExchanger<Dto extends AbstractDto, E extends Entity>
        extends AbstractEntityExchanger<Dto, E> {

    protected DtoExchangerRegistry registry = new DtoExchangerRegistry();

    @Override
    public E exchangeId(Dto from, E to) {
        to.setId(from.getId());
        return to;
    }

    @Override
    public Dto exchangeId(E from, Dto to) {
        to.setId(from.getId());
        return to;
    }

    protected <V extends AbstractDto, F extends Entity, X extends DtoEntityExchanger<V, F>>
            void exchange(Supplier<X> valueGetter, Consumer<V> valueSetter, F from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchange(from));
    }

    protected <V extends AbstractDto, F extends Entity, X extends DtoEntityExchanger<V, F>>
            void exchange(Supplier<X> valueGetter, Consumer<F> valueSetter, V from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchange(from));
    }

    protected <V extends AbstractDto, F extends Entity, X extends DtoEntityExchanger<V, F>>
            void exchangeAll(Supplier<X> valueGetter, Consumer<Set<F>> valueSetter, Set<V> from) {
        X exchanger = valueGetter.get();
        List<F> exchanged = exchanger.exchangeAll(from.stream().collect(Collectors.toList()));
        valueSetter.accept(exchanged.stream().collect(Collectors.toSet()));
    }
  

    protected <V extends AbstractDto, F extends Entity, X extends DtoEntityExchanger<V, F>>
            void exchangeAll(Supplier<X> valueGetter, Consumer<Set<V>> valueSetter, List<F> from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchangeAllFrom(from).stream().collect(Collectors.toSet()));
    }

    protected <V extends AbstractDto, F extends Entity, X extends DtoEntityExchanger<V, F>>
            void exchange(Supplier<X> valueGetter, Consumer<List<F>> valueSetter, Set<V> from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchangeAll(from.stream().collect(Collectors.toList())));
    }

}
