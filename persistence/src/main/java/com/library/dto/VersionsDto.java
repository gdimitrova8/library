package com.library.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author gdimitrova
 */
@Entity(name = "versions")
@Table(name = "versions")
public class VersionsDto extends AbstractDto {

    public final static String APP_VERSION = "appVersion";

    public final static String APP_VERSION_COLUMN = "app_version";

    public final static String DB_VERSION = "dbVersion";

    public final static String DB_VERSION_COLUMN = "db_version";

    public final static String COMMENT = "comment";

    public final static String COMMENT_COLUMN = "comment";

    @Column(name = APP_VERSION_COLUMN, unique = true, nullable = false)
    private String appVersion;

    @Column(name = DB_VERSION_COLUMN, unique = true, nullable = false)
    private String dbVersion;

    @Column(name = COMMENT_COLUMN, unique = true, nullable = false)
    private String comment;

    public VersionsDto() {
    }

    public VersionsDto(String appVersion, String dbVersion, String comment) {
        this.appVersion = appVersion;
        this.dbVersion = dbVersion;
        this.comment = comment;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDbVersion() {
        return dbVersion;
    }

    public void setDbVersion(String dbVersion) {
        this.dbVersion = dbVersion;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
