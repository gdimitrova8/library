package com.library.dto;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author gdimitrova
 */
@Entity(name = "books")
@Table(name = "books")
public class BookDto extends AbstractDto {

    public static final String TITLE = "title";

    public static final String SIGNATURE = "signature";

    public static final String STATE = "state";

    public static final String GENRE = "genre";

    public static final String CHARACTERISTIC = "characteristic";

    public static final String STATUS = "status";

    public static final String FORM = "form_id";

    public static final String PUBLISHER = "publisher_id";

    public static final String STOCK_SIGNATURE = "stock_signature_id";

    public static final String FORMAT_SIGNATURE = "format_signature_id";

    public static final String SERIE = "serie_id";

    public static final String PUBLISH_YEAR = "publish_year";

    public static final String INVENTORY_NUMBER = "inventory_number";

    public static final String ISBN = "isbn";

    @Column(name = TITLE, nullable = false)
    private String title;

    @Column(name = SIGNATURE, nullable = false)
    private String signature;

    @Enumerated(EnumType.STRING)
    @Column(name = STATE, nullable = false)
    private BookStatesDto state;

    @Enumerated(EnumType.STRING)
    @Column(name = STATUS, nullable = false)
    private BookStatusDto status;

    @ManyToOne()
    @JoinColumn(name = PUBLISHER, nullable = false)
    private PublisherDto publisher;

    @Column(name = PUBLISH_YEAR, nullable = false)
    private Integer publishYear;

    @ManyToOne()
    @JoinColumn(name = FORM, nullable = false)
    private WorkFormDto form;

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "authors_books",
            joinColumns = {
                @JoinColumn(name = "book_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "author_id")}
    )
    Set<AuthorDto> authors = new HashSet<>();

    @ManyToOne()
    @JoinColumn(name = STOCK_SIGNATURE)
    private StockSignatureDto stockSignature;

    @ManyToOne()
    @JoinColumn(name = FORMAT_SIGNATURE)
    private FormatSignatureDto formatSignature;

    @ManyToOne
    @JoinColumn(name = SERIE)
    private BookSerieDto serie;

    @Column(name = INVENTORY_NUMBER, nullable = false)
    private String inventoryNumber;

    @Column(name = ISBN, nullable = false)
    private String isbn;

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "book_genres",
            joinColumns = {
                @JoinColumn(name = "book_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "genre_id")}
    )
    private Set<GenreDto> genres = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "book_characteristics",
            joinColumns = {
                @JoinColumn(name = "book_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "characteristic_id")}
    )
    private Set<CharacteristicDto> characteristics = new HashSet<>();

    public BookDto() {
        //Hibernate
    }

    public BookDto(String title, String signature, BookStatesDto state,
            BookStatusDto status, PublisherDto publisher, Integer publishYear,
            WorkFormDto form, Set<AuthorDto> authors, StockSignatureDto stockSignature,
            FormatSignatureDto formatSignature, BookSerieDto serie,
            String inventoryNumber, String isbn) {
        this.title = title;
        this.signature = signature;
        this.state = state;
        this.status = status;
        this.publisher = publisher;
        this.publishYear = publishYear;
        this.form = form;
        this.authors = authors;
        this.stockSignature = stockSignature;
        this.formatSignature = formatSignature;
        this.serie = serie;
        this.inventoryNumber = inventoryNumber;
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public BookStatesDto getState() {
        return state;
    }

    public void setState(BookStatesDto state) {
        this.state = state;
    }

    public BookStatusDto getStatus() {
        return status;
    }

    public void setStatus(BookStatusDto status) {
        this.status = status;
    }

    public PublisherDto getPublisher() {
        return publisher;
    }

    public void setPublisher(PublisherDto publisher) {
        this.publisher = publisher;
    }

    public Integer getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(Integer publishYear) {
        this.publishYear = publishYear;
    }

    public WorkFormDto getForm() {
        return form;
    }

    public void setForm(WorkFormDto form) {
        this.form = form;
    }

    public Set<AuthorDto> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<AuthorDto> authors) {
        this.authors.clear();
        this.authors.addAll(authors);
    }

    public BookSerieDto getSerie() {
        return serie;
    }

    public void setSerie(BookSerieDto serie) {
        this.serie = serie;
    }

    public String getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(String inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public StockSignatureDto getStockSignature() {
        return stockSignature;
    }

    public void setStockSignature(StockSignatureDto stockSignature) {
        this.stockSignature = stockSignature;
    }

    public FormatSignatureDto getFormatSignature() {
        return formatSignature;
    }

    public void setFormatSignature(FormatSignatureDto formatSignature) {
        this.formatSignature = formatSignature;
    }

    public Set<GenreDto> getGenres() {
        return genres;
    }

    public void setGenres(Set<GenreDto> genres) {
        this.genres.clear();
        this.genres.addAll(genres);
    }

    public Set<CharacteristicDto> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(Set<CharacteristicDto> characteristics) {
        this.characteristics.clear();
        this.characteristics.addAll(characteristics);
    }

}
