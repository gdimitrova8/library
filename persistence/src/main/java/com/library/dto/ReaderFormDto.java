package com.library.dto;

import javax.persistence.Column;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * Generated file. Template : code.generator.templates.dto.DtoRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
@Entity(name = "reader_forms")
@Table(name = "reader_forms")
public class ReaderFormDto extends AbstractDto {

    public final static String SEND_DATE = "sendDate";

    public final static String SEND_DATE_COLUMN = "send_date";

    public final static String USER = "user";

    public final static String USER_COLUMN = "user_id";

    public final static String ACCEPTED_DATE = "acceptedDate";

    public final static String ACCEPTED_DATE_COLUMN = "accepted_date";

    public final static String FORM_STATUS = "formStatus";

    public final static String FORM_STATUS_COLUMN = "form_status";

    @Column(name = SEND_DATE_COLUMN, unique = false, nullable = false)
    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar sendDate;

    @OneToOne
    @JoinColumn(name = USER_COLUMN, unique = true, nullable = false)
    private UserDto user;

    @Column(name = ACCEPTED_DATE_COLUMN, unique = false, nullable = false)
    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar acceptedDate;
    
    
    @Enumerated(EnumType.STRING)
    @Column(name = FORM_STATUS_COLUMN, nullable = false)
    private FormStatusDto formStatus;

    public ReaderFormDto() {
        // Hibernate
    }

    public ReaderFormDto(Calendar sendDate, UserDto user, Calendar acceptedDate, FormStatusDto formStatus) {
        this.sendDate = sendDate;
        this.user = user;
        this.acceptedDate = acceptedDate;
        this.formStatus = formStatus;
    }

    public void setSendDate(Calendar sendDate) {
        this.sendDate = sendDate;
    }

    public Calendar getSendDate() {
        return this.sendDate;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public UserDto getUser() {
        return this.user;
    }

    public void setAcceptedDate(Calendar acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public Calendar getAcceptedDate() {
        return this.acceptedDate;
    }

    public FormStatusDto getFormStatus() {
        return formStatus;
    }

    public void setFormStatus(FormStatusDto formStatus) {
        this.formStatus = formStatus;
    }

}
