/*
 * EuroRisk Systems (c) Ltd. All rights reserved.
 */
package com.library.dao;

import java.util.Map;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author gdimitrova
 */
public class EntityManagerFactoryFactory {

    public final static EntityManagerFactoryFactory INSTANCE = new EntityManagerFactoryFactory();

    private EntityManagerFactoryFactory() {
    }

    public EntityManagerFactory createFactory(String persistenceUnitName, Map<String, String> properties) {
        return Persistence.createEntityManagerFactory(persistenceUnitName, resolveProperties(properties));
    }

    private Map<String, String> resolveProperties(Map<String, String> properties) {
        DbConfigImpl dbConfig = new DbConfigImpl();
        dbConfig.setProperties(properties);
        return dbConfig.getProperties();
    }

}
