package com.library.dao;

import com.library.domain.Entity;
import com.library.domain.exchanger.EntityExchanger;
import com.library.domain.list.EntityList;
import com.library.dto.AbstractDto;
import java.util.List;
import java.util.function.Function;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author gdimitrova
 * @param <D>
 * @param <E>
 * @param <L>
 * @param <X>
 */
public abstract class AbstractCrudDao<D extends AbstractDto, E extends Entity, L extends EntityList<E>, X extends EntityExchanger<D, E>>
        extends AbstractDao<D, E, L, X> implements CrudDao<E> {

    public AbstractCrudDao(EntityManager em, Function< List<E>, L> listFactory, Class<E> entityClass, Class<D> dtoClassName, X exchanger) {
        super(em, listFactory, entityClass, dtoClassName, exchanger);
    }

    @Override
    public E save(E entity) {
        return save(exchange(entity));
    }

    @Override
    public List<E> saveAll(List<E> list) {
        return saveOrUpdateAll(exchangeAll(list));
    }

    @Override
    public int update(E e) {
        D dto = exchange(e);
        return super.update(dto, loadProperties(dto));
    }

    @Override
    public void delete(String id) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaDelete<D> delete = cb.createCriteriaDelete(dtoClassName);
        Root<D> root = delete.from(dtoClassName);
        delete.where(cb.equal(root.get("id").as(String.class), id));
        em.createQuery(delete).executeUpdate();
    }

    @Override
    public void deleteAll(List<E> list) {
        list.forEach((e) -> {
            delete(e.getId());
//            em.remove(e);
        });
    }

    @Override
    public E loadById(String id) {
        return getResult("id", id);
    }

    @Override
    public List<E> loadAll() {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<D> query = cb.createQuery(dtoClassName);
        query.select(query.from(dtoClassName));
        return exchangeResults(em.createQuery(query).getResultList());
    }

    protected void persistEntity(D entity) {
        em.persist(entity);
    }

    protected List<E> saveOrUpdateAll(List<D> entities) {
        entities.forEach((e) -> {
            saveOrUpdate(e);
        });
        return exchange(entities);
    }

    protected void mergeEntity(D entity) {
        em.merge(entity);
    }

    protected String saveOrUpdate(D entity) {
        if (entity.getId() == null) {
            persistEntity(entity);
            return entity.getId();
        }
        if (entity.getId() != null) {//|| entity.getId() > 0) {
            mergeEntity(entity);
            return entity.getId();
        }
        persistEntity(entity);
        return entity.getId();
    }

    public E save(D entity) {
        saveOrUpdate(entity);
        return exchange(entity);
    }
}
