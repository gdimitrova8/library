/*
 * Project library
 */
package com.library.dao.user;

import com.library.dao.AbstractSearchDao;
import com.library.dao.UserDao;
import com.library.domain.filter.Filter;
import com.library.domain.filter.UserSearchFields;
import com.library.domain.list.UsersList;
import com.library.domain.user.Roles;
import com.library.domain.user.User;
import com.library.dto.UserDto;
import com.library.dto.exchanger.UserDtoExchanger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;

/**
 *
 * @author gdimitrova
 */
public class UserDaoImpl extends AbstractSearchDao<
      UserDto, User, UsersList, UserDtoExchanger>
        implements UserDao {

    public UserDaoImpl(EntityManager em) {
        super(em, UsersList::new, User.class, UserDto.class, UserDtoExchanger.INSTANCE);
    }

    @Override
    public User load(String username) {
        Map<String, Object> props = new HashMap<>();
        props.put(UserDto.USER_NAME, username);
        List<User> users = getResults(props);
        return users.isEmpty() ? null : users.get(0);
    }

    @Override
    public User load(String username, String pass) {
        Map<String, Object> props = new HashMap<>();
        props.put(UserDto.USER_NAME, username);
        props.put(UserDto.PASSWORD, pass);
        List<User> users = getResults(props);
        return users.isEmpty() ? null : users.get(0);
    }

//    @Override
//    public List<User> searchReaders() {
//        Map<String, Object> props = new HashMap<>();
//        props.put(UserDto.ROLE, Roles.READER);
//        List<User> users = getResults(props);
//        return users.isEmpty() ? null : users;
//    }
    @Override
    public List<User> searchOperators(Filter filter) {
        return getResults(UserDto.ROLE, Roles.OPERATOR, UserDto.CREATED_DATE, filter, true);
    }

    @Override
    public List<User> searchReaders(Filter filter) {
        return getResults(UserDto.ROLE, Roles.READER, UserDto.CREATED_DATE, filter, true);
    }

    @Override
    protected Map<String, Object> loadProperties(UserDto newOne) {
        return new HashMap<>();
    }

    Map<String, String> mappedPropNameByEnumValue = new HashMap<>() {
        {
            put(UserSearchFields.CREATED_DATE.name(), UserDto.CREATED_DATE);
            put(UserSearchFields.USER_NAME.name(), UserDto.USER_NAME);
        }
    };

    @Override
    public String resolvePropertyName(String enumValue) {
        if (enumValue == null || enumValue.equals("NONE")) {
            return null;
        }

        return null;
    }

}
