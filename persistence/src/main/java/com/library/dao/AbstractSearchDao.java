package com.library.dao;

import com.library.domain.Entity;
import com.library.domain.exchanger.EntityExchanger;
import com.library.domain.filter.Filter;
import com.library.domain.filter.SearchConfig;
import com.library.domain.list.EntityList;
import com.library.dto.AbstractDto;
import java.util.List;
import java.util.function.Function;
import javax.persistence.EntityManager;

/**
 *
 * @author gdimitrova
 * @param <D>
 * @param <E>
 * @param <L>
 * @param <X>
 */
public abstract class AbstractSearchDao<
        D extends AbstractDto, E extends Entity, L extends EntityList<E>, X extends EntityExchanger<D, E>>
        extends AbstractCrudDao<D, E, L, X> implements SearchDao<E, L> {

    public AbstractSearchDao(
            EntityManager em,
            Function< List<E>, L> listFactory,
            Class<E> entityClass,
            Class<D> dtoClassName,
            X exchanger) {
        super(em, listFactory, entityClass, dtoClassName, exchanger);
    }

    @Override
    public L search(Filter filter) {
        return makeList(searchAll(filter, true));
    }

    @Override
    public int searchCount(Filter filter) {
        return searchAll(filter, false).size();
    }

    private List<E> searchAll(Filter filter, boolean withLimit) {

        SearchConfig cfg = filter.getCfg();
        String orderBy = resolvePropertyName(cfg.getOrderBy());
        String searchBy = resolvePropertyName(cfg.getSearchBy());
        String text = cfg.getText();

        return getResults(searchBy, text, orderBy, filter, withLimit);
    }

    abstract public String resolvePropertyName(String enumValue);

}
