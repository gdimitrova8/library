package com.library.dao;

import com.library.domain.Entity;
import com.library.domain.exchanger.EntityExchanger;
import com.library.domain.filter.Filter;
import com.library.domain.list.EntityList;
import com.library.dto.AbstractDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.Table;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author gdimitrova
 * @param <D>
 * @param <E>
 * @param <L>
 * @param <X>
 */
public abstract class AbstractDao<D extends AbstractDto, E extends Entity, L extends EntityList<E>, X extends EntityExchanger<D, E>> {

    protected final String table;

    protected final EntityManager em;

    protected final Class<E> entityClass;

    protected final Class<D> dtoClassName;

    private Function< List<E>, L> listFactory;

    protected final X exchanger;

    public AbstractDao(EntityManager em, Function< List<E>, L> listFactory, Class<E> entityClass, Class<D> dtoClassName, X exchanger) {
        this.table = resolveTableName(dtoClassName);
        this.em = em;
        this.entityClass = entityClass;
        this.dtoClassName = dtoClassName;
        this.exchanger = exchanger;
        this.listFactory = listFactory;
    }

    public CriteriaBuilder getCriteriaBuilder() {
        return em.getCriteriaBuilder();
    }

    protected E getResult(String property, Object value) {
        List<E> results = getResults(property, value);
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    protected List<E> getResultsLikeExpr(String property, String value) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<D> query = cb.createQuery(dtoClassName);
        Root<D> root = query.from(dtoClassName);
        query.select(root).where(cb.like(root.get(property), "%" + value + "%"));
        return exchangeResults(em.createQuery(query).getResultList());
    }

    protected List<E> getResultsLikeExpr(String property, String value, String propId, Long id) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<D> query = cb.createQuery(dtoClassName);
        Root<D> root = query.from(dtoClassName);
        query.select(root).where(cb.and(cb.like(root.get(property), "%" + value + "%"), cb.not(cb.equal(root.get(propId), id))));
        return exchangeResults(em.createQuery(query).getResultList());
    }

    protected List<E> getResults(String property, Object value) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<D> query = cb.createQuery(dtoClassName);
        Expression<E> prop = query.from(dtoClassName).get(property);
        query.where(cb.equal(prop, value));
        return exchangeResults(em.createQuery(query).getResultList());
    }

    protected List<E> getResults(String property, Object value,
            String orderProperty, Filter filter, boolean withLimit) {
        return getResults(property, value, orderProperty, filter.getStartIndex(), filter.getMaxResults(), withLimit);
    }

    protected List<E> getResults(String property, Object value,
            String orderProperty, int startIndex, int maxResults, boolean withLimit) {
        if (startIndex < 0 || maxResults < 1) {
            return new ArrayList<>();
        }
        List<E> results;
        if (value instanceof String) {
            results = getResults(property, (String) value, orderProperty);
        } else {
            results = getResults(property, value, orderProperty);
        }
        int count = results.size();
        if (count <= maxResults) {
            return results;
        }
        if (!withLimit) {
            return results;
        }
        if (count > startIndex + maxResults) {
            return results.subList(startIndex, startIndex + maxResults);
        }
        return results.subList(startIndex, count);
    }

    protected List<E> getResults(String property, Object value, String orderProperty) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<D> query = cb.createQuery(dtoClassName);
        Root<D> root = query.from(dtoClassName);
        Expression<D> prop = root.get(property);
        query.select(root);
        query.where(cb.equal(prop, value));
        if (orderProperty != null) {
            query.orderBy(cb.desc(root.get(orderProperty)));
        }
        return exchangeResults(em.createQuery(query).getResultList());
    }

    protected List<E> getResults(String property, String value, String orderProperty) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<D> query = cb.createQuery(dtoClassName);
        Root<D> root = query.from(dtoClassName);
        query.select(root);
        if (!(value.isBlank() || value.isEmpty())) {
            query.where(cb.like(root.get(property), "%" + value + "%"));
        }
        if (orderProperty != null) {
            query.orderBy(cb.desc(root.get(orderProperty)));
        }
        return exchangeResults(em.createQuery(query).getResultList());
    }

    protected List<E> getResults(Map<String, Object> properties) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<D> query = cb.createQuery(dtoClassName);
        Root<D> entity = query.from(dtoClassName);
        List<Predicate> list = new ArrayList<>();
        properties.entrySet().forEach((entry) -> {
            list.add(cb.equal(entity.get(entry.getKey()), entry.getValue()));
        });
        query.where(cb.and(list.toArray(new Predicate[]{})));
        return exchangeResults(em.createQuery(query).getResultList());
    }

    protected List<E> getResults(String property, Double value, ComparisonSign sign) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<D> query = cb.createQuery(dtoClassName);
        Expression<Double> prop = query.from(dtoClassName).get(property);
        query.where(makeExpr(cb, prop, value, sign));
        return exchangeResults(em.createQuery(query).getResultList());
    }

    protected List<E> getResults(String property, Long value) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<D> query = cb.createQuery(dtoClassName);
        Expression<Long> prop = query.from(dtoClassName).get(property);
        query.where(cb.le(prop, value));
        return exchangeResults(em.createQuery(query).getResultList());
    }

    protected List<E> getResults(String property, Long from, Long to) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<D> query = cb.createQuery(dtoClassName);
        Expression<Long> prop = query.from(dtoClassName).get(property);
        query.where(cb.between(prop, from, to));
        return exchangeResults(em.createQuery(query).getResultList());
    }

    protected int update(D oldOne, Map<String, Object> properties) {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaUpdate<D> updateCriteria = cb.createCriteriaUpdate(dtoClassName);
        Root<D> root = updateCriteria.from(dtoClassName);
        properties.entrySet().forEach((entry) -> {
            updateCriteria.set(entry.getKey(), entry.getValue());
        });
        updateCriteria.where(cb.equal(root.get("id"), oldOne.getId()));
        return em.createQuery(updateCriteria).executeUpdate();
    }

    protected List<E> exchangeResults(List<D> resultList) {
        if (resultList.isEmpty()) {
            return new ArrayList<>();
        }
        return resultList.stream().map(r -> exchange(r)).collect(Collectors.toList());
    }

    private Expression<Boolean> makeExpr(CriteriaBuilder cb, Expression<Double> prop, Double val, ComparisonSign sign) {
        switch (sign) {
            case EQUAL:
                return cb.equal(prop, val);
            case LESS_THAN:
                return cb.lessThan(prop, val);
            default:
                throw new AssertionError(sign.name());
        }
    }

    private String resolveTableName(Class<D> dtoClass) {
        Table t = dtoClass.getAnnotation(Table.class);
        return t == null ? dtoClass.getSimpleName() : t.name();
    }

    protected List<E> exchange(List<D> v) {
        return v.stream().map(vo -> exchange(vo)).collect(Collectors.toList());
    }

    protected List<D> exchangeAll(List<E> entities) {
        return entities.stream().map(e -> exchange(e)).collect(Collectors.toList());
    }

    protected E exchange(D v) {
        return exchanger.exchange(v);
    }

    protected D exchange(E e) {
        return exchanger.exchange(e);
    }

    protected L makeList(List<E> list) {
        return listFactory.apply(list);
    }

    protected abstract Map<String, Object> loadProperties(D newOne);
}
