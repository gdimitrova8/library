package com.library.dao;

/**
 *
 * @author gdimitrova
 */
public class DbConfigImpl implements DbConfig {

    private String driver;

    private String url;

    private String username;

    private String password;

    private String dialect;

    private Boolean showSql;

    public DbConfigImpl() {
    }

    public DbConfigImpl(String driver, String url, String username, String password, String dialect, Boolean showSql) {
        this.driver = driver;
        this.url = url;
        this.username = username;
        this.password = password;
        this.dialect = dialect;
        this.showSql = showSql;
    }

    @Override
    public String getDriver() {
        return driver;
    }

    @Override
    public String getURL() {
        return url;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getDialect() {
        return dialect;
    }

    @Override
    public Boolean showSql() {
        return showSql;
    }

    @Override
    public void setDialect(String dialect) {
        this.dialect = dialect;
    }

    @Override
    public void setDriver(String driver) {
        this.driver = driver;
    }

    @Override
    public void setURL(String url) {
        this.url = url;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void showSql(Boolean showSql) {
        this.showSql = showSql;
    }

}
