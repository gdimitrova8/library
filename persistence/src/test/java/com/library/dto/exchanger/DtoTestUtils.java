package com.library.dto.exchanger;

import com.library.domain.Entity;
import com.library.domain.NamedEntity;
import com.library.domain.book.Author;
import com.library.domain.book.Book;
import com.library.domain.book.BookRental;
import com.library.domain.book.signature.Signature;
import com.library.domain.user.User;
import com.library.dto.AbstractDto;
import com.library.dto.AuthorDto;
import com.library.dto.BookDto;
import com.library.dto.BookRentalDto;
import com.library.dto.CharacteristicDto;
import com.library.dto.GenreDto;
import com.library.dto.NamedDto;
import com.library.dto.SignatureDto;
import com.library.dto.UserDto;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.function.Function;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author gdimitrova
 */
public class DtoTestUtils {

    protected void assertDto(BookRental e, BookRentalDto t) {
        assertDto(e.getBook(), t.getBook());
        assertDto(e.getUser(), t.getUser());
        assertDates(e.getReceivableDate(), t.getReceivableDate());
        assertDates(e.getReturnDate(), t.getReturnDate());
        assertDates(e.getReturnDeadLine(), t.getReturnDeadLine());
    }

    public static void assertDto(Book e, BookDto dto) {
        assertEntities(e, dto);
        if (e == null || dto == null) {
            return;
        }
        assertIds(e, dto);
        assertEquals(e.getTitle(), dto.getTitle(), "\nNot equal titles.\n");
        assertEquals(e.getSignature(), dto.getSignature(), "\nNot equal signatures.\n");
        assertEquals(e.getInventoryNumber(), dto.getInventoryNumber(), "\nNot equal inventory numbers.\n");
        assertEquals(e.getIsbn(), dto.getIsbn(), "\nNot equal ISBN.\n");
        assertNamedEntities(e.getPublisher(), dto.getPublisher());
        assertEquals(e.getPublishYear(), dto.getPublishYear(), "\nNot equal publish years.\n");
        assertNamedEntities(e.getForm(), dto.getForm());
       // assertDto(e.getAuthor(), dto.getAuthor());
        assertNamedEntities(e.getSerie(), dto.getSerie());
        assertSignatures(e.getStockSignature(), dto.getStockSignature());
        assertSignatures(e.getFormatSignature(), dto.getFormatSignature());
        List<GenreDto> genres = new ArrayList<>();
        genres.addAll(dto.getGenres());
        assertLists(e.getGenres(), genres);
        List<CharacteristicDto> characteristics = new ArrayList<>();
        characteristics.addAll(dto.getCharacteristics());
        assertLists(e.getCharacteristics(), characteristics);
    }

    public static void assertDto(User e, UserDto dto) {
        assertEntities(e, dto);
        if (e == null || dto == null) {
            return;
        }
        assertIds(e, dto);
        assertEquals(e.getUserName(), dto.getUserName(), "\nNot equal username.\n");
        assertEquals(e.getPassword(), dto.getPassword(), "\nNot equal passwords.\n");
        assertEquals(e.getEmail(), dto.getEmail(), "\nNot equal email.\n");
        assertEquals(e.getFirstName(), dto.getFirstName(), "\nNot equal first name.\n");
        assertEquals(e.getSurname(), dto.getSurname(), "\nNot equal surname.\n");
        assertEquals(e.getLastName(), dto.getLastName(), "\nNot equal last name.\n");
        assertEquals(e.getPhoneNumber(), dto.getPhoneNumber(), "\nNot equal phone number.\n");
        assertEquals(e.getRole().name(), dto.getRole().name(), "\nNot equal user role.\n");
        assertDates(e.getCreatedDate(), dto.getCreatedDate(), "\nNot equal created date.\n");

    }

    public static void assertDates(Calendar c1, Calendar c2, String msg) {
        if (c1 != null && c2 != null) {
            assertEquals(c1.getTimeInMillis(), c2.getTimeInMillis(), msg);
            return;
        }
        assertTrue(c1 == c2, "\nNot equal dates.\n");
    }

    public static void assertDates(Calendar c1, Calendar c2) {
        assertDates(c1, c2, "\nNot equal dates.\n");
    }

    public static void assertDto(Author e, AuthorDto dto) {
        assertEntities(e, dto);
        if (e == null || dto == null) {
            return;
        }
        assertNamedEntities(e, dto);
        assertEquals(e.getBiography(), dto.getBiography(), "\nNot equal biographies.\n");
        assertEquals(e.getBirthPlace(), dto.getBirthPlace(), "\nNot equal birth places.\n");
        assertEquals(e.getBirthDate(), dto.getBirthDate(), "\nNot equal birth dates.\n");
    }

    public static <E extends Entity, Dto extends AbstractDto> void assertIds(E e, Dto dto) {
        assertEquals(e.getId(), dto.getId(), "\nNot equal id.\n");
    }

    public static <E extends Entity, Dto extends AbstractDto> void assertEntities(E e, Dto dto) {
        assertTrue((e == null && dto == null) || (e != null && dto != null),
                "\nThere is null entity.\n");
    }

    public static <E extends NamedEntity, Dto extends NamedDto> void assertNamedEntities(E e, Dto dto) {
        assertEntities(e, dto);
        if (e == null || dto == null) {
            return;
        }
        assertIds(e, dto);
        assertEquals(e.getName(), dto.getName(), "\nNot equal names.\n");
    }

    public static <E extends NamedEntity, Dto extends NamedDto> void assertLists(List<E> e, List<Dto> dto) {
        e.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
        dto.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
        for (int i = 0; i < e.size(); i++) {
            assertNamedEntities(e.get(i), dto.get(i));
        }
    }

    public static <E extends Signature, Dto extends SignatureDto> void assertSignatures(E e, Dto dto) {
        assertEntities(e, dto);
        if (e == null || dto == null) {
            return;
        }
        assertNamedEntities(e, dto);
        assertEquals(e.getAbbreviation(), dto.getAbbreviation(), "\nNot equal abbreviation.\n");
    }

    public static <T> List<T> fillList(Integer count, Function<Integer, T> creator) {
        List<T> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            list.add(creator.apply(i));
        }
        return list;
    }
}
