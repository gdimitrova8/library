package com.library.dao;

import com.library.dao.book.GenreDaoImpl;
import com.library.domain.book.Genre;
import com.library.domain.list.GenresList;
import com.library.dto.GenreDto;
import com.library.dto.exchanger.GenreDtoExchanger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gdimitrova
 */
public class GenreDaoTestCase extends AbstractCrudDaoTestCase<GenreDto, Genre, GenreDtoExchanger,GenresList, GenreDaoImpl> {

    @Override
    protected Genre createEntity() {
        return createEntity("Journal");
    }

    @Override
    protected List<Genre> createEntities() {
        return createGenres();
    }

    @Override
    protected GenreDaoImpl getDao(DaoRegistry registry) {
        return (GenreDaoImpl) registry.getGenreDao();
    }

    public static Genre createEntity(String name) {
        return new Genre(name);
    }

    public static List<Genre> createGenres() {
        List<Genre> genres = new ArrayList<>();
        genres.add(createEntity("Prose"));
        genres.add(createEntity("Memoir"));
        return genres;
    }

    @Override
    protected void prepareDbData(DaoRegistry registry) {
    }

    @Override
    protected boolean isRequiredDbDataPreparation() {
        return false;
    }
}
