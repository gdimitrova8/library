package com.library.dao;

import java.util.Map;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author gdimitrova
 */
public class TestDbConfig {

    public static Map<String, String> HSQLDB_PROPERTIES = createDbConfig(
            "org.hsqldb.jdbcDriver",
            //"jdbc:hsqldb:mem:~/databases/test_library/db",
            "jdbc:hsqldb:file:~/databases/test_library/db",
            "admin", "",
            "org.hibernate.dialect.HSQLDialect", false)
            .getProperties();

    public static Map<String, String> H2DB_PROPERTIES = createDbConfig(
            "org.h2.Driver",
            "jdbc:h2:mem:test_library;DB_CLOSE_DELAY=-1",
            "sa", "sa",
            "org.hibernate.dialect.H2Dialect", false)
            .getProperties();

    public static Map<String, String> MYSQL_PROPERTIES = createDbConfig(
            "com.mysql.cj.jdbc.Driver",
            "jdbc:mysql://localhost/library",
            "sa", "sa",
            "org.hibernate.dialect.MySQL8Dialect", false)
            .getProperties();

    private static DbConfig createDbConfig(String driver, String url, String username, String password,
            String dialect, Boolean showSql) {
        return new DbConfigImpl(driver, url, username, password, dialect, showSql);
    }

    public static EntityManagerFactory createFactory() {
        //return Persistence.createEntityManagerFactory("test.library", TestDbConfig.HSQLDB_PROPERTIES);
        return Persistence.createEntityManagerFactory("test.library", TestDbConfig.H2DB_PROPERTIES);
        // return Persistence.createEntityManagerFactory("test.library",TestDbConfig.MYSQL_PROPERTIES);
    }
}
