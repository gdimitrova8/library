/*
 * EuroRisk Systems (c) Ltd. All rights reserved.
 */
package com.library.dao;

import com.library.domain.Entity;
import com.library.domain.exchanger.EntityExchanger;
import com.library.domain.list.EntityList;
import com.library.dto.AbstractDto;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author gdimitrova
 * @param <D>
 * @param <E>
 * @param <X>
 * @param <L>
 * @param <Dao>
 */
public abstract class AbstractCrudDaoTestCase<
        D extends AbstractDto,
        E extends Entity, 
        X extends EntityExchanger<D, E>,
        L extends EntityList<E>, 
        Dao extends AbstractCrudDao<D, E, L, X>
        > {

    private final TestDbEnvironment dbEnvironment = new TestDbEnvironment();

    @AfterEach
    protected void tearDown() throws Exception {
        dbEnvironment.destroy();
    }

    @BeforeEach
    protected void setUp() throws Exception {
        dbEnvironment.setUp();
        if (!isRequiredDbDataPreparation()) {
            return;
        }
        try (DaoRegistry daoRegistry = dbEnvironment.makeDaoRegistry()) {
            daoRegistry.beginTransaction();
            prepareDbData(daoRegistry);
            daoRegistry.commitTransaction();
        }
    }

    @Test
    public void testSave() throws Exception {
        try (DaoRegistry daoRegistry = dbEnvironment.makeDaoRegistry()) {
            daoRegistry.beginTransaction();
            getDao(daoRegistry).save(createEntity());
            daoRegistry.commitTransaction();
            Assertions.assertTrue(true);
        }
    }

    @Test
    public void testSearch() throws Exception {
        try (DaoRegistry daoRegistry = dbEnvironment.makeDaoRegistry()) {
            daoRegistry.beginTransaction();
            getDao(daoRegistry).save(createEntity());
            daoRegistry.commitTransaction();
            Assertions.assertTrue(true);
        }
    }

    @Test
    public void testSaveAll() throws Exception {
        try (DaoRegistry daoRegistry = dbEnvironment.makeDaoRegistry()) {
            daoRegistry.beginTransaction();
            getDao(daoRegistry).saveAll(createEntities());
            daoRegistry.commitTransaction();
            Assertions.assertTrue(true);
        }
    }

    @Test
    public void testLoadById() throws Exception {
        try (DaoRegistry daoRegistry = dbEnvironment.makeDaoRegistry()) {
            daoRegistry.beginTransaction();
            Dao dao = getDao(daoRegistry);
            E expected = dao.save(createEntity());
            E actual = dao.loadById(expected.getId());
            daoRegistry.commitTransaction();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testDelete() throws Exception {
        try (DaoRegistry daoRegistry = dbEnvironment.makeDaoRegistry()) {
            daoRegistry.beginTransaction();
            Dao dao = getDao(daoRegistry);
            E expected = dao.save(createEntity());
            dao.delete(expected.getId());
            E actual = dao.loadById(expected.getId());
            daoRegistry.commitTransaction();
            assertNull(actual);
        }
    }

    @Test
    public void testLoadByIdNotFound() throws Exception {
        try (DaoRegistry daoRegistry = dbEnvironment.makeDaoRegistry()) {
            daoRegistry.beginTransaction();
            E actual = getDao(daoRegistry).loadById("100000L");
            daoRegistry.commitTransaction();
            assertNull(actual);
        }
    }

    abstract protected Dao getDao(DaoRegistry registry);

    abstract protected E createEntity();

    abstract protected List<E> createEntities();

    abstract protected boolean isRequiredDbDataPreparation();

    abstract protected void prepareDbData(DaoRegistry registry);
}
