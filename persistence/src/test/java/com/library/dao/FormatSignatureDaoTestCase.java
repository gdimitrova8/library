/*
 * Project library
 */
package com.library.dao;

import com.library.dao.book.signature.FormatSignatureDaoImpl;
import com.library.domain.book.signature.FormatSignature;
import com.library.domain.list.FormatSignaturesList;
import com.library.dto.FormatSignatureDto;
import com.library.dto.exchanger.FormatSignatureDtoExchanger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gdimitrova
 */
public class FormatSignatureDaoTestCase extends AbstractCrudDaoTestCase<FormatSignatureDto, FormatSignature,
        FormatSignatureDtoExchanger,FormatSignaturesList, FormatSignatureDaoImpl> {

    @Override
    protected FormatSignature createEntity() {
        return createDefault();
    }

    @Override
    protected List<FormatSignature> createEntities() {
        return createFormatSignatures();
    }

    @Override
    protected FormatSignatureDaoImpl getDao(DaoRegistry registry) {
        return (FormatSignatureDaoImpl) registry.getFormatSignatureDao();
    }

    private static FormatSignature createEntity(String abbreviation, String name) {
        return new FormatSignature(abbreviation, name);
    }

    public static FormatSignature createDefault() {
        return createEntity("D", "Default format");
    }

    public static List<FormatSignature> createFormatSignatures() {
        List<FormatSignature> workForms = new ArrayList<>();
        workForms.add(createEntity("A1", "A1 format"));
        workForms.add(createEntity("A2", "A2 format"));
        return workForms;
    }

    @Override
    protected void prepareDbData(DaoRegistry registry) {
    }

    @Override
    protected boolean isRequiredDbDataPreparation() {
        return false;
    }
}
