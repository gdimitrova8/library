/*
 * Project library
 */
package com.library.dao;

import com.library.dao.book.signature.StockSignatureDaoImpl;
import com.library.domain.book.signature.StockSignature;
import com.library.domain.list.StockSignaturesList;
import com.library.dto.StockSignatureDto;
import com.library.dto.exchanger.StockSignatureDtoExchanger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gdimitrova
 */
public class StockSignatureDaoTestCase extends AbstractCrudDaoTestCase<StockSignatureDto, StockSignature, StockSignatureDtoExchanger, StockSignaturesList, StockSignatureDaoImpl> {

    @Override
    protected StockSignature createEntity() {
        return createDefault();
    }

    @Override
    protected List<StockSignature> createEntities() {
        return createStockSignatures();
    }

    @Override
    protected StockSignatureDaoImpl getDao(DaoRegistry registry) {
        return (StockSignatureDaoImpl) registry.getStockSignatureDao();
    }

    private static StockSignature createEntity(String abbreviation, String name) {
        return new StockSignature(abbreviation, name);
    }

    public static StockSignature createDefault() {
        return createEntity("D", "Default format");
    }

    public static List<StockSignature> createStockSignatures() {
        List<StockSignature> workForms = new ArrayList<>();
        workForms.add(createEntity("A1", "A1 format"));
        workForms.add(createEntity("A2", "A2 format"));
        return workForms;
    }

    @Override
    protected void prepareDbData(DaoRegistry registry) {
    }

    @Override
    protected boolean isRequiredDbDataPreparation() {
        return false;
    }
}
