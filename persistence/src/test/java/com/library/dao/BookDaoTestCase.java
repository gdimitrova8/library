/*
 * Project library
 */
package com.library.dao;

import static com.library.dao.CharacteristicDaoTestCase.createEntity;
import com.library.dao.book.BookDaoImpl;
import com.library.domain.book.WorkForm;
import com.library.domain.book.Author;
import com.library.domain.book.Book;
import com.library.domain.book.BookSerie;
import com.library.domain.book.BookStates;
import com.library.domain.book.BookStatus;
import com.library.domain.book.Characteristic;
import com.library.domain.book.Genre;
import com.library.domain.book.Publisher;
import com.library.domain.book.signature.FormatSignature;
import com.library.domain.book.signature.StockSignature;
import com.library.domain.list.BooksList;
import com.library.dto.BookDto;
import com.library.dto.exchanger.BookDtoExchanger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author gdimitrova
 */
public class BookDaoTestCase extends AbstractCrudDaoTestCase<BookDto, Book, BookDtoExchanger, BooksList, BookDaoImpl> {

    private static Author author;

    private static List<Author> authors;

    private static Publisher publisher;

    private static List<Publisher> publishers;

    private static WorkForm workForm;

    private static List<WorkForm> workForms;

    private static Genre genre;

    private static List<Genre> genres;

    private static Characteristic characteristic;

    private static List<Characteristic> characteristics;

    private static StockSignature stockSignature;

    private static FormatSignature formatSignature;

    @Override
    protected BookDaoImpl getDao(DaoRegistry registry) {
        return (BookDaoImpl) registry.getBookDao();
    }

    @Override
    protected Book createEntity() {
        return createDefault();
    }

    @Override
    protected List<Book> createEntities() {
        return createBooks();
    }

    private static Book createEntity(
            String title,
            String signature,
            BookStates state,
            BookStatus status,
            Publisher publisher,
            Integer publishYear,
            Genre genre, Characteristic characteristic,
            WorkForm form, Author author,
            BookSerie serie, String inventoryNumber, String ISBN,
            StockSignature stockSignature, FormatSignature formatSignature) {
        Book b = new Book(title, signature, state, status, publisher, publishYear, form,
                Arrays.asList(author), serie, inventoryNumber, ISBN, stockSignature, formatSignature);
        b.setGenres(Arrays.asList(genre));
        b.setCharacteristics(Arrays.asList(characteristic));
        return b;
    }

    public static Book createDefault() {
        return createEntity("The big hunt", "2020", BookStates.NEW, BookStatus.AVAILABLE, publisher, 2010,
                genre, characteristic,
                workForm, author, null, "2020001", "561-561-55-63", null, null);
    }

    public static List<Book> createBooks() {
        List<Book> books = new ArrayList<>();
        books.add(createEntity(
                "The big goal", "202", BookStates.NEW, BookStatus.AVAILABLE, publishers.get(0), 2010,
                genres.get(0), characteristics.get(0),
                workForms.get(0), authors.get(0), null, "2020101", "561-561-45-63",
                stockSignature, formatSignature)
        );
        books.add(createEntity(
                "The big purpose", "2011", BookStates.NEW, BookStatus.AVAILABLE, publishers.get(1), 2010,
                genres.get(1), characteristics.get(1),
                workForms.get(1), authors.get(1), null, "2020101", "561-561-45-63", null, null)
        );
        return books;
    }

    public static void prepareDB(DaoRegistry registry) {
        AuthorDao authorDao = registry.getAuthorDao();
        PublisherDao publisherDao = registry.getPublisherDao();
        WorkFormDao workFormDao = registry.getWorkFormDao();
        GenreDao genreDao = registry.getGenreDao();
        CharacteristicDao characteristicDao = registry.getCharacteristicDao();
        author = authorDao.save(AuthorDaoTestCase.createDefault());
        authors = authorDao.saveAll(AuthorDaoTestCase.createAuthors());
        publisher = publisherDao.save(PublisherDaoTestCase.createDefault());
        publishers = publisherDao.saveAll(PublisherDaoTestCase.createPublishers());
        workForm = workFormDao.save(WorkFormDaoTestCase.createDefault());
        workForms = workFormDao.saveAll(WorkFormDaoTestCase.createWorkForms());
        genre = genreDao.save(GenreDaoTestCase.createEntity("Poem"));
        genres = genreDao.saveAll(GenreDaoTestCase.createGenres());
        characteristic = characteristicDao.save(CharacteristicDaoTestCase.createEntity("Humor"));
        characteristics = characteristicDao.saveAll(CharacteristicDaoTestCase.createCharacteristics());
        stockSignature = registry.getStockSignatureDao().save(new StockSignature("A1", "A1 stock"));
        formatSignature = registry.getFormatSignatureDao().save(new FormatSignature("A1", "A1 format"));

    }

    @Override
    protected void prepareDbData(DaoRegistry registry) {
        prepareDB(registry);
    }

    @Override
    protected boolean isRequiredDbDataPreparation() {
        return true;
    }
}
