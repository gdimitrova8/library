package com.library.server;

import com.library.bl.rest.impl.RootResourceImpl;
import com.library.bl.rest.impl.security.SecurityFilter;
import com.library.dao.DaoRegistryFactory;
import com.library.dao.DaoRegistryFactoryImpl;
import com.library.dao.EntityManagerFactoryFactory;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

/**
 *
 * @author gdimitrova
 */
@ApplicationPath("/")
public class RestApplication extends Application {

    private final Set<Object> beans = new LinkedHashSet<>();

    private final DaoRegistryFactory factory;

    public RestApplication(String persistenceUnitName, Map<String, String> properties) {
        this(EntityManagerFactoryFactory.INSTANCE.createFactory(persistenceUnitName, properties), properties);
    }

    private RestApplication(EntityManagerFactory emf, Map<String, String> properties) {
        factory = new DaoRegistryFactoryImpl(emf);
        beans.add(new JacksonJsonProvider());
        beans.add(new RootResourceImpl(factory));
        beans.add(new SecurityFilter(
                factory,
                properties.get(ConfigPropertiesLoader.ADMIN_USERNAME),
                properties.get(ConfigPropertiesLoader.ADMIN_PASSWORD))
        );
    }

    @Override
    public Set<Object> getSingletons() {
        return beans;
    }

}
