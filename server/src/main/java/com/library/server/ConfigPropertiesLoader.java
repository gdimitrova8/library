package com.library.server;

import com.library.dao.DbConfig;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author gdimitrova
 */
public class ConfigPropertiesLoader {

    public final static String ADMIN_USERNAME = "admin.username";

    public final static String ADMIN_PASSWORD = "admin.password";

    public final static String DB_URL = "db.url";

    public final static String DB_DIALECT = "db.dialect";

    public final static String DB_DRIVER = "db.driver";

    public final static String DB_SHOW_SQL = "db.show.sql";

    public final static String DB_USERNAME = "db.username";

    public final static String DB_PASSWORD = "db.username";

    public static Map<String, String> loadConfigProperties() throws FileNotFoundException, IOException {
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String configFile = rootPath + "config.properties";
        Properties configProps = new Properties();
        configProps.load(new FileInputStream(configFile));
        return loadProperties(configProps);
    }

    private static Map<String, String> loadProperties(Properties props) {
        Map<String, String> properties = new HashMap<>();
        properties.put(ADMIN_USERNAME, get(props, ADMIN_USERNAME));
        properties.put(ADMIN_PASSWORD, get(props, ADMIN_PASSWORD));
        properties.put(DbConfig.URL_KEY, get(props, DB_URL));
        properties.put(DbConfig.DIALECT_KEY, get(props, DB_DIALECT));
        properties.put(DbConfig.DRIVER_KEY, get(props, DB_DRIVER));
        properties.put(DbConfig.USER_KEY, get(props, DB_USERNAME));
        properties.put(DbConfig.PASSWORD_KEY, get(props, DB_PASSWORD));
        properties.put(DbConfig.SHOW_SQL_KEY, Boolean.valueOf(get(props, DB_SHOW_SQL)).toString());
        return properties;
    }

    private static String get(Properties props, String key) {
        String value = props.getProperty(key);
        if (value == null) {
            throw new InvalidParameterException("Missing value for property key ".concat(key));
        }
        return value;
    }
}
