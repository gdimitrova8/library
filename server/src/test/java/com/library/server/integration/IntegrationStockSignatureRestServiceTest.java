package com.library.server.integration;

import com.library.rest.api.service.StockSignatureRestService;
import com.library.rest.api.vo.book.signature.StockSignatureVo;
import com.library.rest.api.vo.list.StockSignaturesListVo;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author gdimitrova
 */
public class IntegrationStockSignatureRestServiceTest
        extends IntegrationAbstractCrudRestServiceTest<StockSignatureVo, StockSignaturesListVo, StockSignatureRestService> {

    public IntegrationStockSignatureRestServiceTest() {
        super(StockSignatureVo.class, StockSignaturesListVo.class);
    }

    @Override
    protected StockSignatureVo createVo() {
        return createDefault();
    }

    @Override
    protected StockSignatureRestService getRestService() {
        return proxy.getStockSignaturesRestService();
    }

    @Override
    protected StockSignaturesListVo createListVo() {
        return createStockSignatures();
    }

    @Override
    protected void assertVos(StockSignatureVo expected, StockSignatureVo actual, boolean isSaveAction) {
        if (!isSaveAction) {
            assertEquals(expected, actual);
            return;
        }
        assertEquals(expected.getAbbreviation(), actual.getAbbreviation());
        assertEquals(expected.getName(), actual.getName());
    }

    @Override
    protected void prepareData() {
    }

    private static StockSignatureVo createVo(String abbreviation, String name) {
        return new StockSignatureVo(abbreviation, name);
    }

    public static StockSignatureVo createDefault() {
        return createVo("D", "Default stock");
    }

    public static StockSignaturesListVo createStockSignatures() {
        List<StockSignatureVo> ssList = new ArrayList<>();
        ssList.add(createVo("A1", "A1 stock"));
        ssList.add(createVo("A2", "A2 stock"));
        return new StockSignaturesListVo(ssList);
    }

}
