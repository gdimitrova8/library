package com.library.server.integration;

import com.library.server.ConfigPropertiesLoader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Map;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

/**
 *
 * @author gdimitrova
 */
public class ClientTestRequestFilter implements ClientRequestFilter {

    private final String adminUsername;

    private final String adminPassword;

    public ClientTestRequestFilter(Map<String, String> properties) {
        adminUsername = properties.get(ConfigPropertiesLoader.ADMIN_USERNAME);
        adminPassword = properties.get(ConfigPropertiesLoader.ADMIN_PASSWORD);
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
       // "Library-Token-Based-Auth-Scheme " + Arrays.toString(Base64.encode(authorization.getBytes(), Base64.DEFAULT)));
        requestContext.getHeaders().add("Authorization", "Library-Token-Based-Auth-Scheme " 
                + Arrays.toString(Base64.getEncoder().encode((adminUsername + ":" + adminPassword).getBytes())));
    }

}
