package com.library.server.integration;

import com.library.rest.api.service.BookRentalRestService;
import com.library.rest.api.service.BookRestService;
import com.library.rest.api.service.UserRestService;
import com.library.rest.api.vo.book.BookRentalVo;
import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.list.BooksListVo;
import com.library.rest.api.vo.list.BookRentalsListVo;
import com.library.rest.api.vo.list.UsersListVo;
import com.library.rest.api.vo.user.UserVo;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import javax.ws.rs.core.Response;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author gdimitrova
 */
public class IntegrationBookRentalRestServiceTest
        extends IntegrationAbstractCrudRestServiceTest<BookRentalVo, BookRentalsListVo, BookRentalRestService> {

    private static BookVo book;

    private static BooksListVo books;

    private static UserVo user;

    private static UsersListVo users;

    public IntegrationBookRentalRestServiceTest() {
        super(BookRentalVo.class, BookRentalsListVo.class);
    }

    @Override
    protected BookRentalVo createVo() {
        return createDefault();
    }

    @Override
    protected BookRentalRestService getRestService() {
        return proxy.getBookRentalsRestService();
    }

    @Override
    protected BookRentalsListVo createListVo() {
        return createBookRentals();
    }

    @Override
    protected void assertVos(BookRentalVo expected, BookRentalVo actual, boolean isSaveAction) {
        if (!isSaveAction) {
            assertEquals(expected, actual);
            return;
        }
        IntegrationBookRestServiceTest.assertBooks(expected.getBook(), actual.getBook(), isSaveAction);
        IntegrationUserRestServiceTest.assertUsers(expected.getUser(), actual.getUser(), isSaveAction);
    }

    @Override
    protected void prepareData() {
        IntegrationBookRestServiceTest.prepareDB(proxy);
        BookRestService booksRestService = proxy.getBooksRestService();
        UserRestService usersRestService = proxy.getUsersRestService();
        Response rsp = booksRestService.save(IntegrationBookRestServiceTest.createDefault(), null);
        book = rsp.readEntity(BookVo.class);
        rsp.close();
        rsp = booksRestService.saveAll(IntegrationBookRestServiceTest.createBooks());
        rsp.close();
        rsp = booksRestService.loadAll();
        books = rsp.readEntity(BooksListVo.class);
        rsp.close();
        rsp = usersRestService.save(IntegrationUserRestServiceTest.createDefault(), null);
        user = rsp.readEntity(UserVo.class);
        rsp.close();
        rsp = usersRestService.saveAll(IntegrationUserRestServiceTest.createUsers());
        rsp.close();
        rsp = usersRestService.loadAll();
        users = rsp.readEntity(UsersListVo.class);
        rsp.close();
    }

    public static BookRentalVo createDefault() {
        return createVo(
                book, user,
                new GregorianCalendar(2020, 4, 25).getTimeInMillis(),
                new GregorianCalendar(2020, 5, 20).getTimeInMillis(), null
        );
    }

    private static BookRentalVo createVo(BookVo book, UserVo user, Long receivableDate, Long returnDeadLine, Long returnDate) {
        return new BookRentalVo(
                returnDate,
                book,
                receivableDate,
                user,
                returnDeadLine
        );
    }

    public static BookRentalsListVo createBookRentals() {
        List<BookRentalVo> list = new ArrayList<>();
        list.add(createVo(
                books.getEntities().get(0),
                users.getEntities().get(0),
                new GregorianCalendar(2020, 5, 2).getTimeInMillis(),
                new GregorianCalendar(2020, 5, 20).getTimeInMillis(),
                new GregorianCalendar(2020, 5, 10).getTimeInMillis()
        ));
        list.add(createVo(
                books.getEntities().get(1),
                users.getEntities().get(1),
                new GregorianCalendar(2020, 5, 2).getTimeInMillis(),
                new GregorianCalendar(2020, 5, 20).getTimeInMillis(), null
        ));
        return new BookRentalsListVo(list);
    }

}
