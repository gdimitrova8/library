package com.library.server.integration;

import com.library.rest.api.service.FormatSignatureRestService;
import com.library.rest.api.vo.book.signature.FormatSignatureVo;
import com.library.rest.api.vo.list.FormatSignaturesListVo;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author gdimitrova
 */
public class IntegrationFormatSignatureRestServiceTest
        extends IntegrationAbstractCrudRestServiceTest<FormatSignatureVo, FormatSignaturesListVo, FormatSignatureRestService> {

    public IntegrationFormatSignatureRestServiceTest() {
        super(FormatSignatureVo.class, FormatSignaturesListVo.class);
    }

    @Override
    protected FormatSignatureVo createVo() {
        return createDefault();
    }

    @Override
    protected FormatSignatureRestService getRestService() {
        return proxy.getFormatSignaturesRestService();
    }

    @Override
    protected FormatSignaturesListVo createListVo() {
        return createFormatSignatures();
    }

    @Override
    protected void assertVos(FormatSignatureVo expected, FormatSignatureVo actual, boolean isSaveAction) {
        if (!isSaveAction) {
            assertEquals(expected, actual);
            return;
        }
        assertEquals(expected.getAbbreviation(), actual.getAbbreviation());
        assertEquals(expected.getName(), actual.getName());
    }

    @Override
    protected void prepareData() {
    }

    private static FormatSignatureVo createVo(String abbreviation, String name) {
        return new FormatSignatureVo(abbreviation, name);
    }

    public static FormatSignatureVo createDefault() {
        return createVo("D", "Default format");
    }

    public static FormatSignaturesListVo createFormatSignatures() {
        List<FormatSignatureVo> list = new ArrayList<>();
        list.add(createVo("A1", "A1 format"));
        list.add(createVo("A2", "A2 format"));
        return new FormatSignaturesListVo(list);
    }

}
