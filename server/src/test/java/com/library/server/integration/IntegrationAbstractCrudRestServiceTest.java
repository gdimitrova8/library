package com.library.server.integration;

import com.library.dao.EntityManagerFactoryFactory;
import com.library.rest.api.CrudRestService;
import com.library.rest.api.RootResource;
import com.library.rest.api.vo.AbstractVo;
import com.library.rest.api.vo.EntityListVo;
import com.library.server.ConfigPropertiesLoader;
import com.library.server.ServerApplication;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.internal.ResteasyClientBuilderImpl;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author gdimitrova
 * @param <Vo>
 * @param <ListVo>
 * @param <RestService>
 */
public abstract class IntegrationAbstractCrudRestServiceTest<
        Vo extends AbstractVo, ListVo extends EntityListVo<Vo>, RestService extends CrudRestService<Vo, ListVo>> {

    private final static Logger LOGGER = Logger.getLogger(IntegrationAbstractCrudRestServiceTest.class.getName());

    protected final static String SERVER_URI = "http://localhost:8000/";

    private static EntityManagerFactory factory;

    private final static String PERSISTENCE_UNIT_NAME = "server.test.library";

    private static EntityManagerFactory createFactory(Map<String, String> props) throws IOException {
        if (factory == null) {
            factory = EntityManagerFactoryFactory.INSTANCE.createFactory(PERSISTENCE_UNIT_NAME, props);
        }
        return factory;
    }

    private static Map<String, String> loadConfigProperties() throws IOException {
        return ConfigPropertiesLoader.loadConfigProperties();
    }

    private static ResteasyClient createClient() {
        ResteasyClientBuilderImpl builder = new ResteasyClientBuilderImpl();
        builder.getProviderFactory().register(JacksonJsonProvider.class);
        return builder.build();
    }

    private static RootResource createProxy(Map<String, String> props) {
        client.register(new ClientTestRequestFilter(props));
        ResteasyWebTarget target = client.target(SERVER_URI);
        return target.proxyBuilder(RootResource.class)
                .defaultConsumes(MediaType.APPLICATION_JSON)
                .defaultProduces(MediaType.APPLICATION_JSON)
                .build();
    }

    private static ResteasyClient client;

    protected static RootResource proxy;

    private static ServerApplication SERVER_APP;

    private final Class<Vo> voClass;

    private final Class<ListVo> listVoClass;

    public IntegrationAbstractCrudRestServiceTest(Class<Vo> voClass, Class<ListVo> listVoClass) {
        this.voClass = voClass;
        this.listVoClass = listVoClass;
    }

    @BeforeAll
    public static void beforeAll() {
        try {
            Map<String, String> props = loadConfigProperties();
            SERVER_APP = new ServerApplication(PERSISTENCE_UNIT_NAME, props);
            SERVER_APP.startServer();
            client = createClient();
            proxy = createProxy(props);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }

    }

    @AfterAll
    public static void afterAll() {
        try {
            SERVER_APP.stopServer();
            SERVER_APP.close();
            client.close();
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
    }

    @BeforeEach
    public void beforeEach() {
        prepareData();
    }

    @AfterEach
    public void afterEach() {
        new TestDbEnvironment(proxy).removeData();
    }

    abstract protected RestService getRestService();

    abstract protected Vo createVo();

    abstract protected void assertVos(Vo expected, Vo actual, boolean isSaveAction);

    abstract protected ListVo createListVo();

    abstract protected void prepareData();

    @Test
    public void testSave() {
        Vo vo = createVo();
        RestService restService = getRestService();
        Response rsp = restService.save(vo,null);
        assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
        assertVos(vo, (Vo) rsp.readEntity(voClass), true);
        rsp.close();
    }

    @Test
    public void testLoadById() {
        Vo vo = createVo();
        RestService restService = getRestService();
        Response saveRsp = restService.save(vo,null);
        Vo saved = (Vo) saveRsp.readEntity(voClass);
        saveRsp.close();
        Response rsp = restService.loadById(saved.getId());
        assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
        assertVos(saved, (Vo) rsp.readEntity(voClass), false);
        rsp.close();
    }

    @Test
    public void testLoadNotValidId() {
        RestService restService = getRestService();
        Response rsp = restService.loadById("123L");
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), rsp.getStatus());
        rsp.close();
    }

    @Test
    public void testDelete() {
        Vo vo = createVo();
        RestService restService = getRestService();
        Response saveRsp = restService.save(vo,null);
        Vo saved = (Vo) saveRsp.readEntity(voClass);
        saveRsp.close();
        restService.delete(saved.getId());
        Response rsp = restService.loadById(saved.getId());
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), rsp.getStatus());
        rsp.close();
    }

    @Test
    public void testSaveAll() {
        ListVo createListVo = createListVo();
        RestService restService = getRestService();
        Response rsp = restService.saveAll(createListVo);
        assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
        rsp.close();
    }

    @Test
    public void testLoadAll() {
        ListVo createListVo = createListVo();
        RestService restService = getRestService();
        Response rsp = restService.saveAll(createListVo);
        assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
        rsp.close();
        rsp = restService.loadAll();
        assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
        ListVo actual = rsp.readEntity(listVoClass);
        rsp.close();
        assertLists(createListVo, actual);
    }

    @Test
    public void testDeleteAll() {
        ListVo createListVo = createListVo();
        RestService restService = getRestService();
        Response rsp = restService.saveAll(createListVo);
        assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
        rsp.close();
        rsp = restService.loadAll();
        assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
        ListVo loaded = rsp.readEntity(listVoClass);
        rsp.close();
        rsp = restService.deleteAll(loaded);
        assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
        rsp.close();
        rsp = restService.loadAll();
        assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
        ListVo actual = rsp.readEntity(listVoClass);
        rsp.close();
        assertEquals(0, actual.getEntities().size());
    }

    private void assertLists(ListVo exList, ListVo acList) {
        List<Vo> expected = exList.getEntities();
        List<Vo> actual = acList.getEntities();

        for (int i = 0; i < expected.size(); i++) {
            assertVos(expected.get(i), actual.get(i), true);
        }
    }

}
