package com.library.server.integration;

import com.library.rest.api.RootResource;
import com.library.rest.api.service.AuthorRestService;
import com.library.rest.api.service.BookRestService;
import com.library.rest.api.service.CharacteristicRestService;
import com.library.rest.api.service.GenreRestService;
import com.library.rest.api.service.PublisherRestService;
import com.library.rest.api.service.WorkFormRestService;
import com.library.rest.api.vo.book.AuthorVo;
import com.library.rest.api.vo.book.BookSerieVo;
import com.library.rest.api.vo.book.BookStatesVo;
import com.library.rest.api.vo.book.BookStatusVo;
import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.book.CharacteristicVo;
import com.library.rest.api.vo.book.GenreVo;
import com.library.rest.api.vo.book.PublisherVo;
import com.library.rest.api.vo.book.WorkFormVo;
import com.library.rest.api.vo.list.AuthorsListVo;
import com.library.rest.api.vo.list.BooksListVo;
import com.library.rest.api.vo.list.CharacteristicsListVo;
import com.library.rest.api.vo.list.GenresListVo;
import com.library.rest.api.vo.list.PublishersListVo;
import com.library.rest.api.vo.list.WorkFormsListVo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.core.Response;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author gdimitrova
 */
public class IntegrationBookRestServiceTest
        extends IntegrationAbstractCrudRestServiceTest<BookVo, BooksListVo, BookRestService> {

    private static AuthorVo author;

    private static AuthorsListVo authors;

    private static PublisherVo publisher;

    private static PublishersListVo publishers;

    private static WorkFormVo workForm;

    private static WorkFormsListVo workForms;

    private static GenreVo genre;

    private static GenresListVo genres;

    private static CharacteristicVo characteristic;

    private static CharacteristicsListVo characteristics;

    public IntegrationBookRestServiceTest() {
        super(BookVo.class, BooksListVo.class);
    }

    @Override
    protected BookVo createVo() {
        return createDefault();
    }

    @Override
    protected BookRestService getRestService() {
        return proxy.getBooksRestService();
    }

    @Override
    protected BooksListVo createListVo() {
        return createBooks();
    }

    @Override
    protected void assertVos(BookVo expected, BookVo actual, boolean isSaveAction) {
        assertBooks(expected, actual, isSaveAction);
    }

    @Override
    protected void prepareData() {
        prepareDB(proxy);
    }

    public static void assertBooks(BookVo expected, BookVo actual, boolean isSaveAction) {
        if (!isSaveAction) {
            assertEquals(expected, actual);
            return;
        }
        assertEquals(expected.getTitle(), actual.getTitle());
        assertEquals(expected.getSignature(), actual.getSignature());
        assertEquals(expected.getState(), actual.getState());
        assertEquals(expected.getStatus(), actual.getStatus());
        assertEquals(expected.getPublishYear(), actual.getPublishYear());
        assertEquals(expected.getPublisher(), actual.getPublisher());
        assertEquals(expected.getForm(), actual.getForm());
        assertEquals(expected.getAuthors(), actual.getAuthors());
        assertEquals(expected.getSerie(), actual.getSerie());
        assertEquals(expected.getInventoryNumber(), actual.getInventoryNumber());
        assertEquals(expected.getIsbn(), actual.getIsbn());
        assertEquals(expected.getGenres(), actual.getGenres());
        assertEquals(expected.getCharacteristics(), actual.getCharacteristics());
    }

    public static void prepareDB(RootResource proxy) {
        AuthorRestService authorsRestService = proxy.getAuthorsRestService();
        PublisherRestService publishersRestService = proxy.getPublishersRestService();
        WorkFormRestService workformsRestService = proxy.getWorkFormsRestService();
        GenreRestService genreRestService = proxy.getGenresRestService();
        CharacteristicRestService characteristicRestService = proxy.getCharacteristicsRestService();

        Response rsp = authorsRestService.save(IntegrationAuthorRestServiceTest.createDefault(), null);
        author = rsp.readEntity(AuthorVo.class);
        rsp.close();
        rsp = authorsRestService.saveAll(IntegrationAuthorRestServiceTest.createAuthors());
        rsp.close();
        rsp = authorsRestService.loadAll();
        authors = rsp.readEntity(AuthorsListVo.class);
        rsp.close();

        rsp = publishersRestService.save(IntegrationPublisherRestServiceTest.createDefault(), null);
        publisher = rsp.readEntity(PublisherVo.class);
        rsp.close();
        rsp = publishersRestService.saveAll(IntegrationPublisherRestServiceTest.createPublishers());
        rsp.close();
        rsp = publishersRestService.loadAll();
        publishers = rsp.readEntity(PublishersListVo.class);
        rsp.close();

        rsp = workformsRestService.save(IntegrationWorkFormRestServiceTest.createDefault(), null);
        workForm = rsp.readEntity(WorkFormVo.class);
        rsp.close();
        rsp = workformsRestService.saveAll(IntegrationWorkFormRestServiceTest.createWorkForms());
        rsp.close();
        rsp = workformsRestService.loadAll();
        workForms = rsp.readEntity(WorkFormsListVo.class);
        rsp.close();

        rsp = genreRestService.save(IntegrationGenreRestServiceTest.createDefault(), null);
        genre = rsp.readEntity(GenreVo.class);
        rsp.close();
        rsp = genreRestService.saveAll(IntegrationGenreRestServiceTest.createGenres());
        rsp.close();
        rsp = genreRestService.loadAll();
        genres = rsp.readEntity(GenresListVo.class);
        rsp.close();

        rsp = characteristicRestService.save(IntegrationCharacteristicRestServiceTest.createDefault(), null);
        characteristic = rsp.readEntity(CharacteristicVo.class);
        rsp.close();
        rsp = characteristicRestService.saveAll(IntegrationCharacteristicRestServiceTest.createCharacteristics());
        rsp.close();
        rsp = characteristicRestService.loadAll();
        characteristics = rsp.readEntity(CharacteristicsListVo.class);
        rsp.close();
    }

    private static BookVo createVo(String title, String signature, BookStatesVo state, BookStatusVo status,
            PublisherVo publisher, Integer publishYear, WorkFormVo form, AuthorVo author,
            BookSerieVo serie, String inventoryNumber, String ISBN, CharacteristicVo characteristic, GenreVo genre) {
        return new BookVo(
                Arrays.asList(characteristic),
                signature,
                ISBN,
                title,
                null,
                inventoryNumber,
                publishYear,
                form,
                Arrays.asList(genre),
                serie,
                null,
                publisher,
                state,
                status,
                Arrays.asList(author));
    }

    public static BookVo createDefault() {
        return createVo("The big hunt", "2020", BookStatesVo.NEW, BookStatusVo.AVAILABLE, publisher,
                2010, workForm, author, null, "2020001", "561-561-55-63",
                characteristic, genre);
    }

    public static BooksListVo createBooks() {
        List<BookVo> books = new ArrayList<>();
        books.add(createVo(
                "The big goal", "202", BookStatesVo.NEW, BookStatusVo.AVAILABLE,
                publishers.getEntities().get(0), 2010,
                workForms.getEntities().get(0), authors.getEntities().get(0),
                null, "2020101", "561-561-45-63",
                characteristics.getEntities().get(0), genres.getEntities().get(0))
        );
        books.add(createVo(
                "The big purpose", "2011", BookStatesVo.NEW, BookStatusVo.AVAILABLE,
                publishers.getEntities().get(1), 2010,
                workForms.getEntities().get(1), authors.getEntities().get(1),
                null, "2020101", "561-561-45-63",
                characteristics.getEntities().get(1), genres.getEntities().get(1))
        );
        return new BooksListVo(books);
    }

}
