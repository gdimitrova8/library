package com.library.server.stress;

import com.library.rest.api.RootResource;
import com.library.rest.api.service.UserRestService;
import com.library.rest.api.vo.list.UsersListVo;
import com.library.rest.api.vo.user.UserVo;
import com.library.server.integration.IntegrationUserRestServiceTest;
import javax.ws.rs.core.Response;
import static junit.framework.TestCase.assertEquals;
import org.junit.Test;

/**
 *
 * @author gdimitrova
 */
public class StressUserRestServiceTest
        extends StressAbstractCrudRestServiceTest<UserVo, UsersListVo, UserRestService> {
    
    public StressUserRestServiceTest() {
        super(UserVo.class, UsersListVo.class);
    }
    
    @Override
    protected UserVo createVo() {
        return IntegrationUserRestServiceTest.createDefault();
    }
    
    @Override
    protected UserRestService getRestService(RootResource proxy) {
        return proxy.getUsersRestService();
    }
    
    @Override
    protected UsersListVo createListVo() {
        return IntegrationUserRestServiceTest.createUsers();
    }
    
    @Override
    protected void assertVos(UserVo expected, UserVo actual, boolean isSaveAction) {
        assertUsers(expected, actual, isSaveAction);
    }
    
    @Override
    protected void prepareData(RootResource proxy) {
    }
    
    public static void assertUsers(UserVo expected, UserVo actual, boolean isSaveAction) {
        if (!isSaveAction) {
            assertEquals(expected, actual);
            return;
        }
        assertEquals(expected.getCreatedDate(), actual.getCreatedDate());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getSurname(), actual.getSurname());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getUserName(), actual.getUserName());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getPhoneNumber(), actual.getPhoneNumber());
        assertEquals(expected.getRole(), actual.getRole());
        
    }
    
    
    @Test
    @Override
    public void testSave() {
        for (int i = 0; i < MAX_COUNT; i++) {
            UserVo vo = createVo();
            UserRestService usersRestService = PROXIES.get(i).getUsersRestService();
            Response rsp = usersRestService.save(vo,null);
            assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
            rsp.close();
            rsp = usersRestService.load(vo.getUserName());
            assertEquals(Response.Status.OK.getStatusCode(), rsp.getStatus());
            rsp.close();
        }
    }
    
}
