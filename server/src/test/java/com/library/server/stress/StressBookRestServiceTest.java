package com.library.server.stress;

import com.library.rest.api.RootResource;
import com.library.rest.api.service.AuthorRestService;
import com.library.rest.api.service.BookRestService;
import com.library.rest.api.service.FormatSignatureRestService;
import com.library.rest.api.service.PublisherRestService;
import com.library.rest.api.service.StockSignatureRestService;
import com.library.rest.api.service.WorkFormRestService;
import com.library.rest.api.vo.book.AuthorVo;
import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.book.PublisherVo;
import com.library.rest.api.vo.book.WorkFormVo;
import com.library.rest.api.vo.book.signature.FormatSignatureVo;
import com.library.rest.api.vo.book.signature.StockSignatureVo;
import com.library.rest.api.vo.list.AuthorsListVo;
import com.library.rest.api.vo.list.BooksListVo;
import com.library.rest.api.vo.list.FormatSignaturesListVo;
import com.library.rest.api.vo.list.PublishersListVo;
import com.library.rest.api.vo.list.StockSignaturesListVo;
import com.library.rest.api.vo.list.WorkFormsListVo;
import com.library.server.integration.IntegrationAuthorRestServiceTest;
import com.library.server.integration.IntegrationBookRestServiceTest;
import static com.library.server.integration.IntegrationBookRestServiceTest.assertBooks;
import com.library.server.integration.IntegrationFormatSignatureRestServiceTest;
import com.library.server.integration.IntegrationPublisherRestServiceTest;
import com.library.server.integration.IntegrationStockSignatureRestServiceTest;
import com.library.server.integration.IntegrationWorkFormRestServiceTest;
import com.library.server.stress.StressAbstractCrudRestServiceTest;
import javax.ws.rs.core.Response;

/**
 *
 * @author gdimitrova
 */
public class StressBookRestServiceTest
        extends StressAbstractCrudRestServiceTest<BookVo, BooksListVo, BookRestService> {

    private static AuthorVo author;

    private static AuthorsListVo authors;

    private static PublisherVo publisher;

    private static PublishersListVo publishers;

    private static WorkFormVo workForm;

    private static WorkFormsListVo workForms;

    private static FormatSignatureVo formatSignature;

    private static FormatSignaturesListVo formatSignatures;

    private static StockSignatureVo stockSignature;

    private static StockSignaturesListVo stockSignatures;

    public StressBookRestServiceTest() {
        super(BookVo.class, BooksListVo.class);
    }

    @Override
    protected BookVo createVo() {
        return IntegrationBookRestServiceTest.createDefault();
    }

    @Override
    protected BookRestService getRestService(RootResource proxy) {
        return proxy.getBooksRestService();
    }

    @Override
    protected BooksListVo createListVo() {
        return IntegrationBookRestServiceTest.createBooks();
    }

    @Override
    protected void assertVos(BookVo expected, BookVo actual, boolean isSaveAction) {
        assertBooks(expected, actual, isSaveAction);
    }

    @Override
    protected void prepareData(RootResource proxy) {
        prepareDB(proxy);
    }


    public static void prepareDB(RootResource proxy) {
        AuthorRestService authorsRestService = proxy.getAuthorsRestService();
        PublisherRestService publishersRestService = proxy.getPublishersRestService();
        WorkFormRestService workformsRestService = proxy.getWorkFormsRestService();
        StockSignatureRestService stockSignaturesRestService = proxy.getStockSignaturesRestService();
        FormatSignatureRestService formatSignaturesRestService = proxy.getFormatSignaturesRestService();

        Response rsp = authorsRestService.save(IntegrationAuthorRestServiceTest.createDefault(),null);
        author = rsp.readEntity(AuthorVo.class);
        rsp.close();
        rsp = authorsRestService.saveAll(IntegrationAuthorRestServiceTest.createAuthors());
        rsp.close();
        rsp = authorsRestService.loadAll();
        authors = rsp.readEntity(AuthorsListVo.class);
        rsp.close();

        rsp = publishersRestService.save(IntegrationPublisherRestServiceTest.createDefault(),null);
        publisher = rsp.readEntity(PublisherVo.class);
        rsp.close();
        rsp = publishersRestService.saveAll(IntegrationPublisherRestServiceTest.createPublishers());
        rsp.close();
        rsp = publishersRestService.loadAll();
        publishers = rsp.readEntity(PublishersListVo.class);
        rsp.close();

        rsp = workformsRestService.save(IntegrationWorkFormRestServiceTest.createDefault(),null);
        workForm = rsp.readEntity(WorkFormVo.class);
        rsp.close();
        rsp = workformsRestService.saveAll(IntegrationWorkFormRestServiceTest.createWorkForms());
        rsp.close();
        rsp = workformsRestService.loadAll();
        workForms = rsp.readEntity(WorkFormsListVo.class);
        rsp.close();

        rsp = stockSignaturesRestService.save(IntegrationStockSignatureRestServiceTest.createDefault(),null);
        stockSignature = rsp.readEntity(StockSignatureVo.class);
        rsp.close();
        rsp = stockSignaturesRestService.saveAll(IntegrationStockSignatureRestServiceTest.createStockSignatures());
        rsp.close();
        rsp = stockSignaturesRestService.loadAll();
        stockSignatures = rsp.readEntity(StockSignaturesListVo.class);
        rsp.close();

        rsp = formatSignaturesRestService.save(IntegrationFormatSignatureRestServiceTest.createDefault(),null);
        formatSignature = rsp.readEntity(FormatSignatureVo.class);
        rsp.close();
        rsp = formatSignaturesRestService.saveAll(IntegrationFormatSignatureRestServiceTest.createFormatSignatures());
        rsp.close();
        rsp = formatSignaturesRestService.loadAll();
        formatSignatures = rsp.readEntity(FormatSignaturesListVo.class);
        rsp.close();
    }


}
