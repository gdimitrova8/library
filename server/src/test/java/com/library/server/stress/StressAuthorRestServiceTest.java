package com.library.server.stress;

import com.library.rest.api.RootResource;
import com.library.rest.api.service.AuthorRestService;
import com.library.rest.api.vo.book.AuthorVo;
import com.library.rest.api.vo.list.AuthorsListVo;
import com.library.server.integration.IntegrationAuthorRestServiceTest;
import com.library.server.stress.StressAbstractCrudRestServiceTest;
import static junit.framework.TestCase.assertEquals;

/**
 *
 * @author gdimitrova
 */
public class StressAuthorRestServiceTest
        extends StressAbstractCrudRestServiceTest<AuthorVo, AuthorsListVo, AuthorRestService> {

    public StressAuthorRestServiceTest() {
        super(AuthorVo.class, AuthorsListVo.class);
    }

    @Override
    protected AuthorVo createVo() {
        return IntegrationAuthorRestServiceTest.createDefault();
    }

    @Override
    protected AuthorRestService getRestService(RootResource proxy) {
        return proxy.getAuthorsRestService();
    }

    @Override
    protected AuthorsListVo createListVo() {
        return IntegrationAuthorRestServiceTest.createAuthors();
    }

    @Override
    protected void assertVos(AuthorVo expected, AuthorVo actual, boolean isSaveAction) {
        if (!isSaveAction) {
            assertEquals(expected, actual);
            return;
        }
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getBiography(), actual.getBiography());
        assertEquals(expected.getBirthPlace(), actual.getBirthPlace());
        assertEquals(expected.getBirthDate(), actual.getBirthDate());
    }

    @Override
    protected void prepareData(RootResource proxy) {
    }

}
