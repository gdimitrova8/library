package com.library.server.stress;

import com.library.rest.api.RootResource;
import com.library.rest.api.service.BookRentalRestService;
import com.library.rest.api.service.BookRestService;
import com.library.rest.api.service.UserRestService;
import com.library.rest.api.vo.book.BookRentalVo;
import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.list.BookRentalsListVo;
import com.library.rest.api.vo.list.BooksListVo;
import com.library.rest.api.vo.list.UsersListVo;
import com.library.rest.api.vo.user.UserVo;
import com.library.server.integration.IntegrationBookRentalRestServiceTest;
import com.library.server.integration.IntegrationBookRestServiceTest;
import com.library.server.integration.IntegrationUserRestServiceTest;
import javax.ws.rs.core.Response;
import static junit.framework.TestCase.assertEquals;

/**
 *
 * @author gdimitrova
 */
public class StressBookRentalRestServiceTest
        extends StressAbstractCrudRestServiceTest<BookRentalVo, BookRentalsListVo, BookRentalRestService> {

    private static BookVo book;

    private static BooksListVo books;

    private static UserVo user;

    private static UsersListVo users;

    public StressBookRentalRestServiceTest() {
        super(BookRentalVo.class, BookRentalsListVo.class);
    }

    @Override
    protected BookRentalVo createVo() {
        return IntegrationBookRentalRestServiceTest.createDefault();
    }

    @Override
    protected BookRentalRestService getRestService(RootResource proxy) {
        return proxy.getBookRentalsRestService();
    }

    @Override
    protected BookRentalsListVo createListVo() {
        return IntegrationBookRentalRestServiceTest.createBookRentals();
    }

    @Override
    protected void assertVos(BookRentalVo expected, BookRentalVo actual, boolean isSaveAction) {
        if (!isSaveAction) {
            assertEquals(expected, actual);
            return;
        }
        IntegrationBookRestServiceTest.assertBooks(expected.getBook(), actual.getBook(), isSaveAction);
        IntegrationUserRestServiceTest.assertUsers(expected.getUser(), actual.getUser(), isSaveAction);
    }

    @Override
    protected void prepareData(RootResource proxy) {
        IntegrationBookRestServiceTest.prepareDB(proxy);
        BookRestService booksRestService = proxy.getBooksRestService();
        UserRestService usersRestService = proxy.getUsersRestService();
        Response rsp = booksRestService.save(IntegrationBookRestServiceTest.createDefault(),null);
        book = rsp.readEntity(BookVo.class);
        rsp.close();
        rsp = booksRestService.saveAll(IntegrationBookRestServiceTest.createBooks());
        rsp.close();
        rsp = booksRestService.loadAll();
        books = rsp.readEntity(BooksListVo.class);
        rsp.close();
        rsp = usersRestService.save(IntegrationUserRestServiceTest.createDefault(),null);
        user = rsp.readEntity(UserVo.class);
        rsp.close();
        rsp = usersRestService.saveAll(IntegrationUserRestServiceTest.createUsers());
        rsp.close();
        rsp = usersRestService.loadAll();
        users = rsp.readEntity(UsersListVo.class);
        rsp.close();
    }

    
}
