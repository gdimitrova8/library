package com.library.server.stress;

import com.library.rest.api.RootResource;
import com.library.rest.api.service.WorkFormRestService;
import com.library.rest.api.vo.book.WorkFormVo;
import com.library.rest.api.vo.list.WorkFormsListVo;
import com.library.server.integration.IntegrationWorkFormRestServiceTest;
import com.library.server.stress.StressAbstractCrudRestServiceTest;
import static junit.framework.TestCase.assertEquals;

/**
 *
 * @author gdimitrova
 */
public class StressWorkFormRestServiceTest
        extends StressAbstractCrudRestServiceTest<WorkFormVo, WorkFormsListVo, WorkFormRestService> {

    public StressWorkFormRestServiceTest() {
        super(WorkFormVo.class, WorkFormsListVo.class);
    }

    @Override
    protected WorkFormVo createVo() {
        return IntegrationWorkFormRestServiceTest.createDefault();
    }

    @Override
    protected WorkFormRestService getRestService(RootResource proxy) {
        return proxy.getWorkFormsRestService();
    }

    @Override
    protected WorkFormsListVo createListVo() {
        return IntegrationWorkFormRestServiceTest.createWorkForms();
    }

    @Override
    protected void assertVos(WorkFormVo expected, WorkFormVo actual, boolean isSaveAction) {
        if (!isSaveAction) {
            assertEquals(expected, actual);
            return;
        }
        assertEquals(expected.getName(), actual.getName());
    }

    @Override
    protected void prepareData(RootResource proxy) {
    }

}
