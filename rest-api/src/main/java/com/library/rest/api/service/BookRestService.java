package com.library.rest.api.service;

import com.library.rest.api.CrudRestService;
import com.library.rest.api.SearchRestService;
import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.list.BooksListVo;

/**
 *
 * @author gdimitrova
 */
public interface BookRestService extends SearchRestService<BookVo, BooksListVo>{
    
}
