package com.library.rest.api.vo.filter;

import java.io.Serializable;

/**
 *
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class SearchCfgVo implements Serializable {

    private String text;

    private String orderBy;

    private String searchBy;

    public SearchCfgVo() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getSearchBy() {
        return searchBy;
    }

    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }

    

}
