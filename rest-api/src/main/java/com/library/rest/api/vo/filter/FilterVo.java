package com.library.rest.api.vo.filter;

import java.util.Objects;
import java.io.Serializable;

/**
 *
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class FilterVo implements Serializable {

    private Integer startIndex;

    private Integer maxResults;

    private SearchCfgVo cfg;

    public FilterVo() {
    }

    public FilterVo(Integer startIndex, Integer maxResults, SearchCfgVo cfg) {
        this.startIndex = startIndex;
        this.maxResults = maxResults;
        this.cfg = cfg;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    public Integer getStartIndex() {
        return this.startIndex;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    public Integer getMaxResults() {
        return this.maxResults;
    }

    public SearchCfgVo getCfg() {
        return cfg;
    }

    public void setCfg(SearchCfgVo cfg) {
        this.cfg = cfg;
    }

    @Override
    public int hashCode() {
        int hash = 6;
        hash = 54 * hash + Objects.hashCode(this.startIndex);
        hash = 54 * hash + Objects.hashCode(this.maxResults);
        hash = 54 * hash + Objects.hashCode(this.cfg);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FilterVo other = (FilterVo) obj;
        if (!Objects.equals(this.startIndex, other.startIndex)) {
            return false;
        }
        if (!Objects.equals(this.maxResults, other.maxResults)) {
            return false;
        }
        return Objects.equals(this.cfg, other.cfg);
    }

}