package com.library.rest.api;

import com.library.rest.api.vo.AbstractVo;
import com.library.rest.api.vo.EntityListVo;
import com.library.rest.api.vo.filter.FilterVo;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author gdimitrova
 * @param <Vo>
 * @param <ListVo>
 */
@Consumes("application/json")
@Produces("application/json")
public interface SearchRestService< Vo extends AbstractVo, ListVo extends EntityListVo<Vo>>
        extends CrudRestService<Vo, ListVo> {

    @POST
    @Path("/search")
    public Response search(FilterVo filter, @Context SecurityContext sc);

    @POST
    @Path("/searchCount")
    public Response searchCount(FilterVo filter, @Context SecurityContext sc);

}
