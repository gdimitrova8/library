package com.library.rest.api.service;

import com.library.rest.api.CrudRestService;
import com.library.rest.api.SearchRestService;
import com.library.rest.api.vo.filter.FilterVo;
import com.library.rest.api.vo.list.UsersListVo;
import com.library.rest.api.vo.user.UserVo;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author gdimitrova
 */
public interface UserRestService extends SearchRestService<UserVo, UsersListVo> {

    @GET
    @Path("/load/{username}")
    public Response load(@PathParam("username") String username);

    @POST
    @Path("/searchOperators")
    public Response searchOperators(FilterVo filter);
    
    @POST
    @Path("/searchReaders")
    public Response searchReaders(FilterVo filter);
}
