package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.book.signature.FormatSignatureVo; 
import com.library.rest.api.vo.list.FormatSignaturesListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class FormatSignatureViewModel extends AbstractViewModel<FormatSignatureVo, FormatSignaturesListVo> {

   public FormatSignatureViewModel(@NonNull Application application) {
      super(FormatSignatureVo.class, FormatSignaturesListVo.class, "/format/signature", application);
   }

}
