package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.user.ReaderFormVo; 
import com.library.rest.api.vo.list.ReaderFormsListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class ReaderFormViewModel extends AbstractViewModel<ReaderFormVo, ReaderFormsListVo> {

   public ReaderFormViewModel(@NonNull Application application) {
      super(ReaderFormVo.class, ReaderFormsListVo.class, "/reader/form", application);
   }

}
