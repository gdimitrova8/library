package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.book.BookVo; 
import com.library.rest.api.vo.list.BooksListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class BookViewModel extends AbstractViewModel<BookVo, BooksListVo> {

   public BookViewModel(@NonNull Application application) {
      super(BookVo.class, BooksListVo.class, "/book", application);
   }

}
