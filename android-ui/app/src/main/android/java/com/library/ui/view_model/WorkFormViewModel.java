package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.book.WorkFormVo; 
import com.library.rest.api.vo.list.WorkFormsListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class WorkFormViewModel extends AbstractViewModel<WorkFormVo, WorkFormsListVo> {

   public WorkFormViewModel(@NonNull Application application) {
      super(WorkFormVo.class, WorkFormsListVo.class, "/work/form", application);
   }

}
