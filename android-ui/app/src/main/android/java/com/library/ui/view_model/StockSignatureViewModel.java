package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.book.signature.StockSignatureVo; 
import com.library.rest.api.vo.list.StockSignaturesListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class StockSignatureViewModel extends AbstractViewModel<StockSignatureVo, StockSignaturesListVo> {

   public StockSignatureViewModel(@NonNull Application application) {
      super(StockSignatureVo.class, StockSignaturesListVo.class, "/stock/signature", application);
   }

}
