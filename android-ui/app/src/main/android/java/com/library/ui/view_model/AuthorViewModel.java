package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.book.AuthorVo; 
import com.library.rest.api.vo.list.AuthorsListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class AuthorViewModel extends AbstractViewModel<AuthorVo, AuthorsListVo> {

   public AuthorViewModel(@NonNull Application application) {
      super(AuthorVo.class, AuthorsListVo.class, "/author", application);
   }

}
