package com.library.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.library.rest.api.vo.book.WorkFormVo;
import com.library.rest.api.vo.filter.FilterVo;
import com.library.rest.api.vo.filter.SearchCfgVo;
import com.library.rest.api.vo.filter.GenreSearchFieldsVo;
import com.library.rest.api.vo.filter.UserSearchFieldsVo;
import com.library.rest.api.vo.filter.BookSerieSearchFieldsVo;
import com.library.rest.api.vo.filter.BookRentalSearchFieldsVo;
import com.library.rest.api.vo.filter.WorkFormSearchFieldsVo;
import com.library.rest.api.vo.filter.ReaderFormSearchFieldsVo;
import com.library.rest.api.vo.filter.StockSignatureSearchFieldsVo;
import com.library.rest.api.vo.filter.PublisherSearchFieldsVo;
import com.library.rest.api.vo.filter.AuthorSearchFieldsVo;
import com.library.rest.api.vo.filter.CharacteristicSearchFieldsVo;
import com.library.rest.api.vo.filter.BookSearchFieldsVo;
import com.library.rest.api.vo.filter.FormatSignatureSearchFieldsVo;

import com.library.ui.R;

public class SearchEntityView extends AppCompatActivity {

    public static final String ENTITY_NAME = "com.library.ui.activity.SearchEntityView.ENTITY_NAME";
    public static final String SEARCH_FILTER = "com.library.ui.activity.SearchEntityView.SEARCH_FILTER";
    ActivityResultLauncher<Intent> searchResult = create();
    private Intent mainIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_entity_view);

        mainIntent = getIntent();

        String name = mainIntent.getStringExtra(ENTITY_NAME);
        if(name == null){
            return;
        }
      if (name.equals(com.library.rest.api.vo.book.GenreVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), GenreSearchFieldsVo.values(), GenreListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.user.UserVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), UserSearchFieldsVo.values(), UserListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.book.BookSerieVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), BookSerieSearchFieldsVo.values(), BookSerieListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.book.BookRentalVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), BookRentalSearchFieldsVo.values(), BookRentalListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.book.WorkFormVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), WorkFormSearchFieldsVo.values(), WorkFormListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.user.ReaderFormVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), ReaderFormSearchFieldsVo.values(), ReaderFormListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.book.signature.StockSignatureVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), StockSignatureSearchFieldsVo.values(), StockSignatureListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.book.PublisherVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), PublisherSearchFieldsVo.values(), PublisherListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.book.AuthorVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), AuthorSearchFieldsVo.values(), AuthorListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.book.CharacteristicVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), CharacteristicSearchFieldsVo.values(), CharacteristicListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.book.BookVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), BookSearchFieldsVo.values(), BookListView.class);
         return;
      }
      if (name.equals(com.library.rest.api.vo.book.signature.FormatSignatureVo.class.getName())) {
         init(new FilterVo(0, 30, new SearchCfgVo()), FormatSignatureSearchFieldsVo.values(), FormatSignatureListView.class);
         return;
      }


    }

    private <T, F extends FilterVo> void init(F filter, T[] values, Class<?> listViewClass) {
        Spinner search_by_spinner =  findViewById(R.id.search_by_spinner);
        Spinner order_by_spinner =  findViewById(R.id.order_by_spinner);
        if (values.length > 2) {
            init(R.id.search_by_text, R.id.search_by_layout, search_by_spinner, values);
        }
        init(R.id.order_by_text, R.id.order_by_layout, order_by_spinner, values);
        Button search_btn =  findViewById(R.id.search_btn);
        EditText search_text = findViewById(R.id.search_text);
        search_btn.setOnClickListener(v -> {

            Intent intent = new Intent(this, listViewClass);
            intent.putExtras(mainIntent);
            SearchCfgVo cfg = filter.getCfg();
            cfg.setText(search_text.getText().toString());
            if (values.length > 2) {
                cfg.setSearchBy(search_by_spinner.getSelectedItem().toString());
            }
            cfg.setOrderBy(order_by_spinner.getSelectedItem().toString());
            intent.putExtra(SEARCH_FILTER, filter);
            searchResult.launch(intent);
        });
    }

    private <T> void init(int editTextId, int layoutId, Spinner spinner, T[] values) {
        EditText text =  findViewById(editTextId);
        text.setVisibility(View.VISIBLE);
        RelativeLayout layout =  findViewById(layoutId);
        layout.setVisibility(View.VISIBLE);
        spinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, values));
    }

    private ActivityResultLauncher<Intent> create() {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    int resultCode = result.getResultCode();
                    if (resultCode == RESULT_CANCELED) {
                        return;
                    }
                    if (resultCode == RESULT_OK) {
                        setResult(RESULT_OK, result.getData());
                        finish();
                    }
                });
    }
}