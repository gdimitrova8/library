package com.library.ui.reports;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.library.rest.api.vo.filter.AuthorSearchFieldsVo;
import com.library.rest.api.vo.filter.BookRentalSearchFieldsVo;
import com.library.rest.api.vo.filter.BookSearchFieldsVo;
import com.library.rest.api.vo.filter.BookSerieSearchFieldsVo;
import com.library.rest.api.vo.filter.CharacteristicSearchFieldsVo;
import com.library.rest.api.vo.filter.FilterVo;
import com.library.rest.api.vo.filter.GenreSearchFieldsVo;
import com.library.rest.api.vo.filter.PublisherSearchFieldsVo;
import com.library.rest.api.vo.filter.SearchCfgVo;
import com.library.rest.api.vo.filter.UserSearchFieldsVo;
import com.library.rest.api.vo.filter.WorkFormSearchFieldsVo;
import com.library.ui.R;
import com.library.ui.activity.AuthorListView;
import com.library.ui.activity.BookListView;
import com.library.ui.activity.BookRentalListView;
import com.library.ui.activity.BookSerieListView;
import com.library.ui.activity.CharacteristicListView;
import com.library.ui.activity.GenreListView;
import com.library.ui.activity.PublisherListView;
import com.library.ui.activity.UserListView;
import com.library.ui.activity.WorkFormListView;

public class ReportSearchEntityView extends AppCompatActivity {

    public static final String ENTITY_NAME ="com.library.ui.reports.ReportSearchEntityView.ENTITY_NAME";
    public static final String RATING ="com.library.ui.reports.ReportSearchEntityView.RATING";
    public static final String SEARCH_FILTER = "com.library.ui.reports.ReportSearchEntityView.SEARCH_FILTER";
    ActivityResultLauncher<Intent> searchResult = create();
    private Intent mainIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_entity_view);

        mainIntent = getIntent();

        String name = mainIntent.getStringExtra(ENTITY_NAME);
        if (name == null) {
            return;
        }
        int i = name.lastIndexOf(".");
        TextView search_title = findViewById(R.id.search_title);
        search_title.setText("Search " + name.substring(i + 1).replace("Vo", "s"));
        if (name.equals(com.library.rest.api.vo.user.UserVo.class.getName())) {
            init(new FilterVo(0, 30, new SearchCfgVo()), UserSearchFieldsVo.values(), UserListView.class);
            return;
        }
        if (name.equals(com.library.rest.api.vo.book.BookVo.class.getName())) {
            init(new FilterVo(0, 30, new SearchCfgVo()), BookSearchFieldsVo.values(), BooksReport.class);
        }

    }

    private <T, F extends FilterVo> void init(F filter, T[] values, Class<?> reportViewClass) {
        Spinner search_by_spinner = findViewById(R.id.search_by_spinner);
        Spinner order_by_spinner = findViewById(R.id.order_by_spinner);
        if (values.length > 2) {
            init(R.id.search_by_text, R.id.search_by_layout, search_by_spinner, values);
        }
        init(R.id.order_by_text, R.id.order_by_layout, order_by_spinner, values);
        Button search_btn = findViewById(R.id.search_btn);
        EditText search_text = findViewById(R.id.search_text);
        search_btn.setOnClickListener(v -> {

            Intent intent = new Intent(this, reportViewClass);
            intent.putExtras(mainIntent);
            SearchCfgVo cfg = filter.getCfg();
            cfg.setText(search_text.getText().toString());
            if (values.length > 2) {
                cfg.setSearchBy(search_by_spinner.getSelectedItem().toString());
            }
            cfg.setOrderBy(order_by_spinner.getSelectedItem().toString());
            intent.putExtra(SEARCH_FILTER, filter);
            searchResult.launch(intent);
        });
    }

    private <T> void init(int editTextId, int layoutId, Spinner spinner, T[] values) {
        EditText text = findViewById(editTextId);
        text.setVisibility(View.VISIBLE);
        RelativeLayout layout = findViewById(layoutId);
        layout.setVisibility(View.VISIBLE);
        spinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, values));
    }

    private ActivityResultLauncher<Intent> create() {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    int resultCode = result.getResultCode();
                    if (resultCode == RESULT_CANCELED) {
                        return;
                    }
                    if (resultCode == RESULT_OK) {
                        setResult(RESULT_OK, result.getData());
                        finish();
                    }
                });
    }
}