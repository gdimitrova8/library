package com.library.ui.request;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

public class RequestFactory {
    private static final String TAG = "RequestFactory";
    private static RequestFactory instance;
    private final Gson gson = new Gson();
    private final ObjectMapper mapper = new ObjectMapper();
    private Context context;
    private RequestQueue requestQueue;

    private RequestFactory(Context context) {
        this.context = context;
        this.requestQueue = getRequestQueue();
    }

    public static synchronized RequestFactory getInstance(Context context) {
        if (instance == null) {
            instance = new RequestFactory(context);
        }
        return instance;
    }

    public <T, Req> void save(String svcURL, Req request, Class<T> clazz, Response.Listener<T> listener) {
        makeRequest(Request.Method.POST, svcURL + "/save", request, clazz, listener);
    }

    public <T, Req> void update(String svcURL, Req request, Class<T> clazz, Response.Listener<T> listener) {
        makeRequest(Request.Method.POST, svcURL + "/update", request, clazz, listener);
    }

    public <T, Req> void searchCount(String svcURL, Req request, Response.Listener<Integer> listener) {
        makeRequest(Request.Method.POST, svcURL + "/searchCount", request, Integer.class, listener);
    }
    public <T, Req> void search(String svcURL, Req request, Class<T> clazz, Response.Listener<T> listener) {
        makeRequest(Request.Method.POST, svcURL + "/search", request, clazz, listener);
    }

    public <T, Req> void load(String svcURL, Req request, Class<T> clazz, Response.Listener<T> listener) {
        makeRequest(Request.Method.GET, svcURL + "/load/" + request, request, clazz, listener);
    }

    public <T, Req> void loadById(String svcURL, Req request, Class<T> clazz, Response.Listener<T> listener) {
        makeRequest(Request.Method.GET, svcURL + "/loadById", request, clazz, listener);
    }

    public <T, Req> void delete(String svcURL, Req request, Class<T> clazz, Response.Listener<T> listener) {
        makeRequest(Request.Method.DELETE, svcURL + "/delete", request, clazz, listener);
    }

    public <T, Req> void loadAll(String svcURL, Req request, Class<T> clazz, Response.Listener<T> listener) {
        makeRequest(Request.Method.GET, svcURL + "/loadAll",
                request,//request == null ? null : gson.toJson(request),
                clazz,
                // new TypeToken<T>() { }.getType(),
                listener);
    }

    private <T, Req> void makeRequest(int method, String url, Req request, Class<T> clazz, Response.Listener<T> listener) {
        String requestBody=null;
        try {
            requestBody = request == null ? null : mapper.writeValueAsString(request);
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }
            addToRequestQueue(new JsonRequest<T>(method, url, requestBody, clazz, null, context, listener, getErrorListener()));
    }

    private RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }

    private <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    private Response.ErrorListener getErrorListener() {
        return error -> {
            if (error.getCause() != null) {
                Log.i(TAG, "\n" + error.getCause().getMessage() + "\n");
            }
            if (error.getMessage() != null) {
                Log.i(TAG, "\n" + error.getMessage() + "\n");
            }
            if (error instanceof NetworkError) {
                Toast.makeText(context, "No network is available", Toast.LENGTH_LONG).show();
                return;
            }
            if (error.networkResponse != null && error.networkResponse.data != null) {
                try {
                    String data = new String(error.networkResponse.data, "UTF-8");
                    data = data.replace("[", "");
                    data = data.replace("]", "");
                    data = data.replace("\"", "");
                    String[] dataMsg = data.split(", ");
                    for (String s : dataMsg) {
                        Toast.makeText(context, s, Toast.LENGTH_LONG).show();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return;
            }
            Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
        };
    }

}
