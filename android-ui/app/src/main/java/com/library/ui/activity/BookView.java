package com.library.ui.activity;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.library.rest.api.vo.book.AuthorVo;
import com.library.rest.api.vo.book.BookSerieVo;
import com.library.rest.api.vo.book.BookStatesVo;
import com.library.rest.api.vo.book.BookStatusVo;
import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.book.CharacteristicVo;
import com.library.rest.api.vo.book.GenreVo;
import com.library.rest.api.vo.book.PublisherVo;
import com.library.rest.api.vo.book.WorkFormVo;
import com.library.ui.R;
import com.library.ui.Utils;
import com.library.ui.view_model.BookViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

public class BookView extends AppCompatActivity {
    public static final String EXTRA_ENTITY = "com.library.book.EXTRA_BOOK";
    public static final String EXTRA_MODE = "com.library.book.EXTRA_MODE";
    public static final String CREATE_MODE = "CREATE_MODE";
    public static final String DISGARDE_MODE = "ADD_MODE";
    public static final String EDIT_MODE = "EDIT_MODE";
    public static final String ARCHIVE_MODE = "ARCHIVE_MODE";
    public static final String DELETE_MODE = "DELETE_MODE";
    public static final String RENT_MODE = "RENT_MODE";
    public static final String TAG = "AddEditBook";
    private final List<AuthorVo> authors = new ArrayList<>();
    private final List<GenreVo> genres = new ArrayList<>();
    private final List<CharacteristicVo> characteristics = new ArrayList<>();
    private BookViewModel bookViewModel;
    private EditText inventoryNumber;
    private EditText title;
    private EditText yearText;
    private EditText isbn;
    //
    private EditText authorName;
    private ImageButton authorSearch;
    private AuthorVo author;
    ActivityResultLauncher<Intent> authorResult = create(data -> {
        author = (AuthorVo) data.getSerializableExtra(AuthorView.EXTRA_ENTITY);
        if (author == null) {
            Toast.makeText(this, "Not added author!", Toast.LENGTH_SHORT).show();
            return;
        }
        authorName.setText(author.getName());
        if (authors.isEmpty()) {
            authors.add(author);
        } else {
            authors.set(0, author);
        }
    }, "\nSuccessfully added author!\n");
    //
    private EditText publisherName;
    private ImageButton publisherSearch;
    private PublisherVo publisher;
    ActivityResultLauncher<Intent> publisherResult = create(data -> {
        publisher = (PublisherVo) data.getSerializableExtra(PublisherView.EXTRA_ENTITY);
        if (publisher == null) {
            Toast.makeText(this, "Not added publisher!", Toast.LENGTH_SHORT).show();
            return;
        }
        publisherName.setText(publisher.getName());
    }, "\nSuccessfully added publisher!\n");
    //
    private EditText genreName;
    private GenreVo genre;
    ActivityResultLauncher<Intent> genreResult = create(data -> {
        genre = (GenreVo) data.getSerializableExtra(GenreView.EXTRA_ENTITY);
        if (genre == null) {
            Toast.makeText(this, "Not added book genre!", Toast.LENGTH_SHORT).show();
            return;
        }
        genreName.setText(genre.getName());
        if (genres.isEmpty()) {
            genres.add(genre);
        } else {
            genres.set(0, genre);
        }
    }, "\nSuccessfully added genre!\n");
    private ImageButton genreSearch;
    //
    private EditText characteristicName;
    private CharacteristicVo characteristic;
    ActivityResultLauncher<Intent> characteristicResult = create(data -> {
        characteristic = (CharacteristicVo) data.getSerializableExtra(CharacteristicView.EXTRA_ENTITY);
        if (characteristic == null) {
            Toast.makeText(this, "Not added characteristic!", Toast.LENGTH_SHORT).show();
            return;
        }
        characteristicName.setText(characteristic.getName());
        if (characteristics.isEmpty()) {
            characteristics.add(characteristic);
        } else {
            characteristics.set(0, characteristic);
        }
    }, "\nSuccessfully added characteristic!\n");
    private ImageButton characteristicSearch;
    //
    private EditText bookSerieName;
    private ImageButton bookSerieSearch;
    private BookSerieVo bookSerie;
    ActivityResultLauncher<Intent> bookSerieResult = create(data -> {
        bookSerie = (BookSerieVo) data.getSerializableExtra(BookSerieView.EXTRA_ENTITY);
        if (bookSerie == null) {
            Toast.makeText(this, "Not added book serie!", Toast.LENGTH_SHORT).show();
            return;
        }
        bookSerieName.setText(bookSerie.getName());
    }, "\nSuccessfully added book serie!\n");
    //
    private EditText workFormName;
    private ImageButton workFormSearch;
    private WorkFormVo workForm;
    ActivityResultLauncher<Intent> workFormResult = create(data -> {
        workForm = (WorkFormVo) data.getSerializableExtra(WorkFormView.EXTRA_ENTITY);
        if (workForm == null) {
            Toast.makeText(this, "Not added work form!", Toast.LENGTH_SHORT).show();
            return;
        }
        workFormName.setText(workForm.getName());
    }, "\nSuccessfully added work form!\n");
    //
    private EditText stockSignatureName;
    private ImageButton stockSignatureSearch;
    //
    private EditText formatSignatureName;
    private ImageButton formatSignatureSearch;

    private DateFormat df = new SimpleDateFormat("yyyy-mm-dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_view);

        bookViewModel = new ViewModelProvider(this).get(BookViewModel.class);

        inventoryNumber = findViewById(R.id.inventory_number_text);
        title = findViewById(R.id.title_text);
        yearText = findViewById(R.id.year_text);
        isbn = findViewById(R.id.ISBN_text);

        Button addBookButton = findViewById(R.id.add_btn);
        Button archiveBookButton = findViewById(R.id.archive_btn);
        Button discardBookButton = findViewById(R.id.discard_btn);
        Button changeBookButton = findViewById(R.id.change_btn);
        Button rentBookButton = findViewById(R.id.rent_btn);

        authorName = findViewById(R.id.author_text);
        authorSearch = findViewById(R.id.author_search);
        //
        publisherName = findViewById(R.id.publisher_text);
        publisherSearch = findViewById(R.id.publisher_search);
        //
        genreName = findViewById(R.id.genre_text);
        genreSearch = findViewById(R.id.genre_search);
        //
        characteristicName = findViewById(R.id.characteristic_text);
        characteristicSearch = findViewById(R.id.characteristic_search);
        //
        workFormName = findViewById(R.id.work_form_text);
        workFormSearch = findViewById(R.id.work_form_search);
        //
        bookSerieName = findViewById(R.id.book_serie_text);
        bookSerieSearch = findViewById(R.id.book_serie_search);
        //
        stockSignatureName = findViewById(R.id.stock_signature_text);
        stockSignatureSearch = findViewById(R.id.stock_signature_search);
        //stockSignatureName.setVisibility(View.INVISIBLE);
        //stockSignatureSearch.setVisibility(View.INVISIBLE);
        //
        formatSignatureName = findViewById(R.id.format_signature_text);
        formatSignatureSearch = findViewById(R.id.format_signature_search);
        //formatSignatureName.setVisibility(View.INVISIBLE);
        //formatSignatureSearch.setVisibility(View.INVISIBLE);
        //
        authorSearch.setOnClickListener(v -> {
            onSearchClick(authorResult, AuthorVo.class, AuthorView.EXTRA_MODE, AuthorView.ADD_MODE);
        });
        workFormSearch.setOnClickListener(v -> {
            onSearchClick(workFormResult, WorkFormVo.class, WorkFormView.EXTRA_MODE, WorkFormView.ADD_MODE);
        });

        publisherSearch.setOnClickListener(v -> {
            onSearchClick(publisherResult, PublisherVo.class, PublisherView.EXTRA_MODE, PublisherView.ADD_MODE);
        });

        genreSearch.setOnClickListener(v -> {
            onSearchClick(genreResult, GenreVo.class, GenreView.EXTRA_MODE, GenreView.ADD_MODE);
        });

        characteristicSearch.setOnClickListener(v -> {
            onSearchClick(characteristicResult, CharacteristicVo.class, CharacteristicView.EXTRA_MODE, CharacteristicView.ADD_MODE);
        });

        bookSerieSearch.setOnClickListener(v -> {
            onSearchClick(bookSerieResult, BookSerieVo.class, BookSerieView.EXTRA_MODE, BookSerieView.ADD_MODE);
        });

        Intent intent = getIntent();
        String mode = intent.getStringExtra(EXTRA_MODE);
        BookVo bookVo = (BookVo) intent.getSerializableExtra(EXTRA_ENTITY);
        if (bookVo != null) {
            fillData(bookVo);
        }

        if (mode == null)
            throw new RuntimeException("Missing work mode!");
        switch (mode) {
            case CREATE_MODE:
                addBookButton.setVisibility(View.VISIBLE);
                addBookButton.setOnClickListener(v -> {
                    if (!areMissingData()) {
                        return;
                    }
                    bookViewModel.save(makeBookVo(BookStatesVo.NEW, BookStatusVo.AVAILABLE)).observe(this, savedBook -> {
                        setResult(RESULT_OK, new Intent());
                        finish();
                    });
                });
                break;
            case ARCHIVE_MODE:
                if (bookVo == null) {
                    throw new RuntimeException("Missing book for editing!");
                }
                hideSearches();
                makeUnchangeableEditTexts(bookVo);
                archiveBookButton.setVisibility(View.VISIBLE);
                archiveBookButton.setOnClickListener(v -> {
                    bookViewModel.update(makeBookVo(BookStatesVo.OLD, BookStatusVo.READING_ROOM)).observe(this, savedBook -> {
                        setResult(RESULT_OK, new Intent());
                        finish();
                    });
                });
                break;
            case EDIT_MODE:
                if (bookVo == null) {
                    throw new RuntimeException("Missing book for editing!");
                }
                if (!areMissingData()) {
                    return;
                }
                changeBookButton.setOnClickListener(v -> {
                    bookViewModel.update(makeBookVo(bookVo.getState(), bookVo.getStatus())).observe(this, savedBook -> {
                        setResult(RESULT_OK, new Intent());
                        finish();
                    });
                });
                break;
            case RENT_MODE:
                if (bookVo == null) {
                    throw new RuntimeException("Missing book for renting!");
                }
                hideSearches();
                makeUnchangeableEditTexts(bookVo);
                rentBookButton.setVisibility(View.VISIBLE);
                rentBookButton.setOnClickListener(v -> {
                    Intent rentIntent = new Intent();
                    rentIntent.putExtra(EXTRA_ENTITY, bookVo);
                    setResult(RESULT_OK, rentIntent);
                    finish();
                });
                break;
            case DELETE_MODE:
                if (bookVo == null) {
                    throw new RuntimeException("Missing book for discarding!");
                }
                hideSearches();
                discardBookButton.setVisibility(View.VISIBLE);
                discardBookButton.setOnClickListener(v -> {
                    bookViewModel.delete(bookVo).observe(this, isDeleted -> {
                        setResult(RESULT_OK, new Intent());
                        finish();
                    });
                });
                break;
            default:
                throw new RuntimeException("Missing or not valid mode!");
        }

    }

    private boolean areMissingData() {
        if (inventoryNumber.getText().toString().isEmpty()) {
            Toast.makeText(this, "Not added inventory number!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (title.getText().toString().isEmpty()) {
            Toast.makeText(this, "Not added title!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (isbn.getText().toString().isEmpty()) {
            Toast.makeText(this, "Not added ISBN!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (authorName.getText().toString().isEmpty()) {
            Toast.makeText(this, "Not added author!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (publisherName.getText().toString().isEmpty()) {
            Toast.makeText(this, "Not added publisher!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (yearText.getText().toString().isEmpty()) {
            Toast.makeText(this, "Not added publish year!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void onSearchClick(ActivityResultLauncher<Intent> resultLauncher, Class<?> voClass, String modeExtra, String modeValue) {
        Intent intent = new Intent(this, SearchEntityView.class);
        intent.putExtra(SearchEntityView.ENTITY_NAME, voClass.getName());
        intent.putExtra(modeExtra, modeValue);
        resultLauncher.launch(intent);
    }

    private ActivityResultLauncher<Intent> create(Consumer<Intent> onSuccess, String successMsg) {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    int resultCode = result.getResultCode();
                    if (resultCode == RESULT_CANCELED || result.getData() == null) {
                        return;
                    }
                    if (resultCode == RESULT_OK) {
                        onSuccess.accept(result.getData());
                        Log.i(TAG, successMsg);
                        return;
                    }
                    Toast.makeText(this, "No result", Toast.LENGTH_SHORT).show();
                });
    }

    private void fillData(BookVo book) {
        inventoryNumber.setText(book.getInventoryNumber());
        title.setText(book.getTitle());
        yearText.setText(book.getPublishYear().toString());
        isbn.setText(book.getIsbn());
        Utils.setText(workFormName, book.getForm());
        Utils.setText(publisherName, book.getPublisher());
        Utils.setText(bookSerieName, book.getSerie());
        List<AuthorVo> bAuthors = book.getAuthors();
        if (!bAuthors.isEmpty()) {
            Utils.setText(authorName, bAuthors.get(0));
        }
        List<GenreVo> bGenres = book.getGenres();
        if (!bGenres.isEmpty()) {
            Utils.setText(genreName, bGenres.get(0));
        }
        List<CharacteristicVo> bCharacteristics = book.getCharacteristics();
        if (!bCharacteristics.isEmpty()) {
            Utils.setText(characteristicName, bCharacteristics.get(0));
        }
    }

    private void hideSearches() {
        authorSearch.setVisibility(View.GONE);
        publisherSearch.setVisibility(View.GONE);
        genreSearch.setVisibility(View.GONE);
        bookSerieSearch.setVisibility(View.GONE);
        characteristicSearch.setVisibility(View.GONE);
        workFormSearch.setVisibility(View.GONE);
    }

    private void makeUnchangeableEditTexts(BookVo bookVo) {
        Utils.makeUnchangeable(inventoryNumber, bookVo.getInventoryNumber());
        Utils.makeUnchangeable(title, bookVo.getTitle());
        Utils.makeUnchangeable(isbn, bookVo.getIsbn());
        Utils.makeUnchangeable(yearText, bookVo.getPublishYear().toString());
    }

    private BookVo makeBookVo(BookStatesVo state, BookStatusVo status) {
        Date currentTime = Calendar.getInstance().getTime();
        // String signature = df.format(currentTime) + "-" + author.getId();
        String signature = currentTime.getTime() + "-" + author.getId();

        return new BookVo(
                characteristics,
                signature,
                isbn.getText().toString(),
                title.getText().toString(),
                null,// formatSignature,
                inventoryNumber.getText().toString(),
                Integer.parseInt(yearText.getText().toString()),
                workForm,
                genres,
                bookSerie,
                null,//stockSignature,
                publisher,
                state,
                status,
                authors);
    }
}