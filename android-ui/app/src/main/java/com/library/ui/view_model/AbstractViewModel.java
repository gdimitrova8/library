package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.library.rest.api.response.SuccessResponse;
import com.library.rest.api.vo.AbstractVo;
import com.library.rest.api.vo.EntityListVo;
import com.library.rest.api.vo.filter.FilterVo;
import com.library.ui.request.RequestFactory;

public abstract class AbstractViewModel< Entity extends AbstractVo, ListVo extends EntityListVo<Entity>> extends AndroidViewModel {
    public static final String APP_URL = "http://10.0.2.2:8000";
    private final Class<Entity> entityClass;
    private final Class<ListVo> entityListClass;
    private final String url;
    private MutableLiveData<ListVo> searchEntities;
    private MutableLiveData<Integer> searchEntitiesCount;
    private MutableLiveData<ListVo> allEntities;
    private MutableLiveData<Entity> saveEntity;
    private MutableLiveData<Entity> loadedEntity;
    private MutableLiveData<Boolean> deleteResult;
    private MutableLiveData<Boolean> updateResult;

    public AbstractViewModel( Class<Entity> entityClass, Class<ListVo> entityListClass, String url, @NonNull Application application) {
        super(application);
        this.entityClass = entityClass;
        this.entityListClass = entityListClass;
        this.url = APP_URL + url;
    }

    public MutableLiveData<Entity> loadById(Long id) {
        loadedEntity = new MutableLiveData<>();
        RequestFactory.getInstance(this.getApplication()).loadById(
                url,
                id,
                entityClass,
                response -> loadedEntity.setValue(response)
        );
        return loadedEntity;
    }

    public MutableLiveData<Entity> save(Entity entity) {
        saveEntity = new MutableLiveData<>();
        RequestFactory.getInstance(this.getApplication()).save(
                url,
                entity,
                entityClass,
                response -> saveEntity.setValue(response)
        );
        return saveEntity;
    }

    public MutableLiveData<Boolean> update(Entity entity) {
        updateResult = new MutableLiveData<>();
        RequestFactory.getInstance(this.getApplication()).update(
                url,
                entity,
                SuccessResponse.class,
                response -> updateResult.setValue(true)
        );
        return updateResult;
    }

    public MutableLiveData<ListVo> search(FilterVo entity) {
        searchEntities = new MutableLiveData<>();
        RequestFactory.getInstance(this.getApplication()).search(
                url,
                entity,
                entityListClass,
                response -> searchEntities.setValue(response)
        );
        return searchEntities;
    }
    public MutableLiveData<Integer> searchCount(FilterVo entity) {
        searchEntitiesCount = new MutableLiveData<>();
        RequestFactory.getInstance(this.getApplication()).searchCount(
                url,
                entity,
                response -> searchEntitiesCount.setValue(response)
        );
        return searchEntitiesCount;
    }

    public MutableLiveData<Boolean> delete(Entity entity) {
        deleteResult = new MutableLiveData<>();
        RequestFactory.getInstance(this.getApplication()).delete(
                url,
                entity.getId(),
                SuccessResponse.class,
                response -> deleteResult.setValue(true)
        );
        return deleteResult;
    }

    public MutableLiveData<ListVo> loadAll() {
        allEntities = new MutableLiveData<>();

        RequestFactory.getInstance(this.getApplication()).loadAll(
                url,
                null,
                entityListClass,
                response -> allEntities.setValue(response)
        );

        return allEntities;
    }

}
