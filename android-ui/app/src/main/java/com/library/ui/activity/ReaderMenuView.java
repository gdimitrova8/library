package com.library.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.library.ui.R;

public class ReaderMenuView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reader_menu);
    }
}