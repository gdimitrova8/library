package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.book.GenreVo; 
import com.library.rest.api.vo.list.GenresListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class GenreViewModel extends AbstractViewModel<GenreVo, GenresListVo> {

   public GenreViewModel(@NonNull Application application) {
      super(GenreVo.class, GenresListVo.class, "/genre", application);
   }

}
