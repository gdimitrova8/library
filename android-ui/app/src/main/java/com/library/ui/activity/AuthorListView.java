package com.library.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.library.rest.api.vo.filter.FilterVo;
import com.library.ui.R;
import com.library.ui.adapter.AuthorAdapter;
import com.library.ui.view_model.AuthorViewModel;

import static com.library.ui.activity.SearchEntityView.SEARCH_FILTER;

public class AuthorListView extends AppCompatActivity {

    private AuthorAdapter authorAdapter;
    private AuthorViewModel authorViewModel;
    private Intent mainIntent;
    private EditText page_text;
    private EditText pages_text;
    private Button prev_btn;
    private Button next_btn;
    private FilterVo filter;
    ActivityResultLauncher<Intent> addResult = create("Successfully added author!");
    ActivityResultLauncher<Intent> changedResult = create("Successfully changed author!");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.author_list_view);
        mainIntent = getIntent();
        filter = (FilterVo) mainIntent.getSerializableExtra(SEARCH_FILTER);

        page_text = findViewById(R.id.page_text);
        pages_text = findViewById(R.id.pages_text);
        prev_btn = findViewById(R.id.prev_btn);
        next_btn = findViewById(R.id.next_btn);

        FloatingActionButton buttonAddNote = findViewById(R.id.button_add_author);
        buttonAddNote.setOnClickListener(v -> {
            Intent intent = new Intent(this, AuthorView.class);
            intent.putExtra(AuthorView.EXTRA_MODE, AuthorView.CREATE_MODE);
            addResult.launch(intent);
        });

        RecyclerView recyclerView = findViewById(R.id.author_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        authorAdapter = new AuthorAdapter();
        recyclerView.setAdapter(authorAdapter);

        authorViewModel = new ViewModelProvider(this).get(AuthorViewModel.class);

        authorAdapter.setOnItemClickListener(author -> {
            Intent intent = new Intent(AuthorListView.this, AuthorView.class);

            intent.putExtras(mainIntent);
            intent.putExtra(AuthorView.EXTRA_ENTITY, author);
            changedResult.launch(intent);
        });

        prev_btn.setOnClickListener(e -> {
            int si = filter.getStartIndex();
            filter.setStartIndex(si - filter.getMaxResults());
            search();
        });
        next_btn.setOnClickListener(e -> {
            int si = filter.getStartIndex();
            filter.setStartIndex(si + filter.getMaxResults());
            search();
        });

        search();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout_now:
                logoutNow();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logoutNow() {
        Intent intent = new Intent(this, LoginView.class);
        startActivity(intent);
        setResult(Activity.RESULT_OK);
        finish();
    }

    private ActivityResultLauncher<Intent> create(String successMsg) {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    int resultCode = result.getResultCode();
                    if (resultCode == RESULT_CANCELED) {
                        return;
                    }
                    if (resultCode == RESULT_OK) {
                        setResult(RESULT_OK, result.getData());
                        finish();
                        Toast.makeText(this, successMsg, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Toast.makeText(this, "No request", Toast.LENGTH_SHORT).show();
                    search();
                });
    }

    private void search() {
        authorViewModel.searchCount(filter).observe(this,
                (count) -> {
                    if (count == null || count == 0) {
                        page_text.setText("0");
                        pages_text.setText("0");
                        makeUnchangeable(prev_btn);
                        makeUnchangeable(next_btn);
                        return;
                    }
                    search(count);
                }
        );
    }

    @SuppressLint("SetTextI18n")
    private void search(Integer count) {
        authorViewModel.search(filter).observe(this,
                authors -> {
                    authorAdapter.submitList(authors.getEntities());

                    int times = count / 30;
                    int pages = Math.round(times);
                    int divided = count % 30;
                    if (divided > 0) {
                        pages += 1;
                    }
                    int si = filter.getStartIndex();
                    int currentPage = si == 0 ? 1 : si / 30 + 1;
                    page_text.setText(Integer.toString(currentPage));
                    pages_text.setText(Integer.toString(pages));
                    if (currentPage == 1) {
                        makeUnchangeable(prev_btn);
                    } else {
                        makeChangeable(prev_btn);
                    }
                    if (currentPage == pages) {
                        makeUnchangeable(next_btn);
                    } else {
                        makeChangeable(next_btn);
                    }
                }
        );
    }

    private void makeUnchangeable(Button btn) {
        btn.setClickable(false);
        btn.setBackground(getDrawable(R.mipmap.ic_grey_right_arrow_icon));
    }

    private void makeChangeable(Button btn) {
        btn.setClickable(true);
        btn.setBackground(getDrawable(R.mipmap.ic_right_arrow_icon));
    }
}