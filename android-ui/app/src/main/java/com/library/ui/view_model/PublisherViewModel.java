package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.book.PublisherVo; 
import com.library.rest.api.vo.list.PublishersListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class PublisherViewModel extends AbstractViewModel<PublisherVo, PublishersListVo> {

   public PublisherViewModel(@NonNull Application application) {
      super(PublisherVo.class, PublishersListVo.class, "/publisher", application);
   }

}
