package com.library.ui.reports;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProvider;

import com.library.rest.api.vo.book.AuthorVo;
import com.library.rest.api.vo.book.BookSerieVo;
import com.library.rest.api.vo.book.BookStatesVo;
import com.library.rest.api.vo.book.BookStatusVo;
import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.book.CharacteristicVo;
import com.library.rest.api.vo.book.GenreVo;
import com.library.rest.api.vo.book.PublisherVo;
import com.library.rest.api.vo.book.WorkFormVo;
import com.library.rest.api.vo.filter.BookSearchFieldsVo;
import com.library.rest.api.vo.filter.FilterVo;
import com.library.rest.api.vo.filter.SearchCfgVo;
import com.library.ui.R;
import com.library.ui.view_model.BookViewModel;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

public class BooksReport extends AppCompatActivity {
    private BookViewModel bookViewModel;
    private EditText page_text;
    private EditText pages_text;
    private Button prev_btn;
    private Button next_btn;
    private Button export_csv;
    private FilterVo filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.books_report_view);
        SearchCfgVo cfg = new SearchCfgVo();
        cfg.setText("");
        cfg.setSearchBy(BookSearchFieldsVo.NONE.name());
        filter = new FilterVo(0, 30, cfg);

        page_text = findViewById(R.id.page_text);
        pages_text = findViewById(R.id.pages_text);
        prev_btn = findViewById(R.id.prev_btn);
        next_btn = findViewById(R.id.next_btn);
        export_csv = findViewById(R.id.export_csv);

        bookViewModel = new ViewModelProvider(this).get(BookViewModel.class);

        prev_btn.setOnClickListener(e -> {
            int si = filter.getStartIndex();
            filter.setStartIndex(si - filter.getMaxResults());
            search();
        });
        next_btn.setOnClickListener(e -> {
            int si = filter.getStartIndex();
            filter.setStartIndex(si + filter.getMaxResults());
            search();
        });

        search();
    }

    private void addCell(TableRow row, String text, @ColorInt int color, int typeface) {
        TextView cell = new TextView(this);
        cell.setText(text);
        cell.setTextColor(color);
        cell.setPadding(10, 10, 10, 10);
        cell.setBackground(getDrawable(R.drawable.border));
        cell.setTextSize(20);
        cell.setTypeface(null, typeface);
        row.addView(cell);
    }

    public void refreshTable(List<BookVo> data) {
        TableLayout table = findViewById(R.id.table_main);
        TableRow headerRow = new TableRow(this);
        headerRow.setBackground(getDrawable(R.drawable.border));
        addCell(headerRow, getString(R.string.inventory_number), Color.BLACK, Typeface.BOLD);
        addCell(headerRow, getString(R.string.title), Color.BLACK, Typeface.BOLD);
        addCell(headerRow, getString(R.string.author), Color.BLACK, Typeface.BOLD);
        addCell(headerRow, getString(R.string.signature), Color.BLACK, Typeface.BOLD);
        addCell(headerRow, getString(R.string.isbn), Color.BLACK, Typeface.BOLD);
        addCell(headerRow, getString(R.string.serie), Color.BLACK, Typeface.BOLD);
        addCell(headerRow, getString(R.string.publish_year), Color.BLACK, Typeface.BOLD);
        addCell(headerRow, getString(R.string.publisher), Color.BLACK, Typeface.BOLD);
        addCell(headerRow, getString(R.string.state), Color.BLACK, Typeface.BOLD);
        addCell(headerRow, getString(R.string.status), Color.BLACK, Typeface.BOLD);
        table.addView(headerRow);
        for (int i = 0; i < data.size(); i++) {
            TableRow row = new TableRow(this);
            row.setBackground(getDrawable(R.drawable.border));
            BookVo item = data.get(i);
            addCell(row, item.getInventoryNumber(), Color.BLACK, Typeface.ITALIC);
            addCell(row, item.getTitle(), Color.BLACK, Typeface.ITALIC);
            addCell(row, item.getAuthors().stream()
                            .map(n -> String.valueOf(n.getName()))
                            .collect(Collectors.joining(", ", "", ""))
                    , Color.BLACK, Typeface.ITALIC);
            addCell(row, item.getSignature(), Color.BLACK, Typeface.ITALIC);
            addCell(row, item.getIsbn(), Color.BLACK, Typeface.ITALIC);
            BookSerieVo serie = item.getSerie();
            addCell(row, serie != null ? serie.getName() : "", Color.BLACK, Typeface.ITALIC);
            addCell(row, item.getPublishYear().toString(), Color.BLACK, Typeface.ITALIC);
            PublisherVo publisher = item.getPublisher();
            addCell(row, publisher != null ? publisher.getName() : "", Color.BLACK, Typeface.ITALIC);
            BookStatesVo state = item.getState();
            addCell(row, state != null ? state.name() : "", Color.BLACK, Typeface.ITALIC);
            BookStatusVo status = item.getStatus();
            addCell(row, status != null ? status.name() : "", Color.BLACK, Typeface.ITALIC);
            table.addView(row);
        }
    }

    private void search() {
        bookViewModel.searchCount(filter).observe(this,
                (count) -> {
                    if (count == null || count == 0) {
                        page_text.setText("0");
                        pages_text.setText("0");
                        makeUnchangeable(prev_btn);
                        makeUnchangeable(next_btn);
                        return;
                    }
                    search(count);
                }
        );
    }

    private static List<BookVo> books = new ArrayList<BookVo>() {
        {
            add(new BookVo(
                    Arrays.asList(new CharacteristicVo("")),
                    "П 886.7-1/В15",
                    "978-619-759-614-4",
                    "Epic of the Forgotten ",
                    null,
                    "20201224-1200-1",
                    1893,
                    new WorkFormVo("novel"),
                    Arrays.asList(new GenreVo("Novel")),
                    null,
                    null,
                    new PublisherVo("Helikon"),
                    BookStatesVo.OLD,
                    BookStatusVo.READING_ROOM,
                    Arrays.asList(new AuthorVo("Ivan Vazov",null,null,null))
            ));
            add(new BookVo(
                    Arrays.asList(new CharacteristicVo("")),
                    "П 886.7-1/В15",
                    "978-619-759-614-4",
                    "Epic of the Forgotten ",
                    null,
                    "20201224-1200-2",
                    2019,
                    new WorkFormVo("novel"),
                    Arrays.asList(new GenreVo("Novel")),
                    null,
                    null,
                    new PublisherVo("Helikon"),
                    BookStatesVo.NEW,
                    BookStatusVo.AVAILABLE,
                    Arrays.asList(new AuthorVo("Ivan Vazov",null,null,null))
            ));
        }
    };
    @SuppressLint("SetTextI18n")
    private void search(Integer count) {
        bookViewModel.search(filter).observe(this,
                list -> {
                    // bookReportAdapter.submitList(list.getEntities());
                    // refreshTable(list.getEntities());
                    refreshTable(books);
                    export_csv.setVisibility(count == 0 ? View.INVISIBLE : View.VISIBLE);
                    int times = count / 30;
                    int pages = Math.round(times);
                    int divided = count % 30;
                    if (divided > 0) {
                        pages += 1;
                    }
                    int si = filter.getStartIndex();
                    int currentPage = si == 0 ? 1 : si / 30 + 1;
                    makeChangeable(page_text, currentPage > 1 && pages > 1);
                    page_text.setText(Integer.toString(currentPage));
                    pages_text.setText(Integer.toString(pages));
                    if (currentPage == 1) {
                        makeUnchangeable(prev_btn);
                    } else {
                        makeChangeable(prev_btn);
                    }
                    if (currentPage == pages) {
                        makeUnchangeable(next_btn);
                    } else {
                        makeChangeable(next_btn);
                    }
                }
        );
    }

    public void export(View view) {
        //generate data
        bookViewModel.searchCount(filter).observe(this,
                (count) -> {
                    FilterVo f = new FilterVo();
                    f.setStartIndex(0);
                    f.setMaxResults(count);
                    f.setCfg(filter.getCfg());
                    bookViewModel.search(f).observe(this,
                            list -> {
                                final StringBuilder data = writeData(list.getEntities());
                                try {
                                    //saving the file into device
                                    FileOutputStream out = openFileOutput("books.csv", Context.MODE_PRIVATE);
                                    out.write((data.toString()).getBytes());
                                    out.close();

                                    //exporting
                                    Context context = getApplicationContext();
                                    File fileLocation = new File(getFilesDir(), "books.csv");
                                    Uri path = FileProvider.getUriForFile(context, "com.library.export.csv.file.provider", fileLocation);
                                    Intent fileIntent = new Intent(Intent.ACTION_SEND);
                                    fileIntent.setType("text/csv");
                                    fileIntent.putExtra(Intent.EXTRA_SUBJECT, "Data");
                                    fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    fileIntent.putExtra(Intent.EXTRA_STREAM, path);
                                    startActivity(Intent.createChooser(fileIntent, "Send mail"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                    );
                }
        );


    }

    private StringBuilder writeData(List<BookVo> data) {
        StringBuilder sb = new StringBuilder();

        sb.append(getString(R.string.inventory_number)).append(",");
        sb.append(getString(R.string.title)).append(",");
        sb.append(getString(R.string.author)).append(",");
        sb.append(getString(R.string.signature)).append(",");
        sb.append(getString(R.string.isbn)).append(",");
        sb.append(getString(R.string.serie)).append(",");
        sb.append(getString(R.string.publish_year)).append(",");
        sb.append(getString(R.string.publisher)).append(",");
        sb.append(getString(R.string.state)).append(",");
        sb.append(getString(R.string.status)).append("\n");
        for (int i = 0; i < data.size(); i++) {
            BookVo item = data.get(i);
            sb.append(item.getInventoryNumber()).append(",");
            sb.append(item.getTitle()).append(",");
            sb.append(item.getAuthors().get(0).getName()).append(",");
            sb.append(item.getSignature()).append(",");
            sb.append(item.getIsbn()).append(",");
            BookSerieVo serie = item.getSerie();
            sb.append(serie != null ? serie.getName() : "").append(",");
            sb.append(item.getPublishYear().toString()).append(",");
            PublisherVo publisher = item.getPublisher();
            sb.append(publisher != null ? publisher.getName() : "").append(",");
            BookStatesVo state = item.getState();
            sb.append(state != null ? state.name() : "").append(",");
            BookStatusVo status = item.getStatus();
            sb.append(status != null ? status.name() : "" + "\n");
        }
        return sb;
    }

    private void makeChangeable(EditText txt, boolean editable) {
        txt.setTextIsSelectable(editable);
        txt.setEnabled(editable);
        txt.setFocusable(editable);
        txt.setClickable(editable);
    }

    private void makeUnchangeable(Button btn) {
        btn.setClickable(false);
        btn.setBackground(getDrawable(R.mipmap.ic_grey_right_arrow_icon));
        btn.setVisibility(View.INVISIBLE);
    }

    private void makeChangeable(Button btn) {
        btn.setClickable(true);
        btn.setBackground(getDrawable(R.mipmap.ic_right_arrow_icon));
        btn.setVisibility(View.VISIBLE);
    }
}