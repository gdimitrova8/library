package com.library.ui.reports;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.user.ReaderFormVo;
import com.library.rest.api.vo.user.UserVo;
import com.library.ui.R;

public class ReportsMenu extends AppCompatActivity {

    ActivityResultLauncher<Intent> resultLauncher = create("");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports_menu_view);


        Button books_btn = findViewById(R.id.books_btn);
        books_btn.setOnClickListener(v -> {
            Intent intent = new Intent(this, ReportSearchEntityView.class);
            intent.putExtra(ReportSearchEntityView.ENTITY_NAME, BookVo.class.getName());
            resultLauncher.launch(intent);
        });

        Button reader_forms_btn = findViewById(R.id.reader_forms_btn);
        reader_forms_btn.setOnClickListener(v -> {
            Intent intent = new Intent(this, ReportSearchEntityView.class);
            intent.putExtra(ReportSearchEntityView.ENTITY_NAME, ReaderFormVo.class.getName());
            resultLauncher.launch(intent);
        });

        Button readers_btn = findViewById(R.id.readers_btn);
        readers_btn.setOnClickListener(v -> {
            Intent intent = new Intent(this, ReportSearchEntityView.class);
            intent.putExtra(ReportSearchEntityView.ENTITY_NAME, UserVo.class.getName());
            resultLauncher.launch(intent);
        });
        Button readers_rating_btn = findViewById(R.id.readers_rating_btn);
        readers_rating_btn.setOnClickListener(v -> {
            Intent intent = new Intent(this, ReportSearchEntityView.class);
            intent.putExtra(ReportSearchEntityView.ENTITY_NAME, UserVo.class.getName());
            resultLauncher.launch(intent);
        });

    }

    private ActivityResultLauncher<Intent> create(String successMsg) {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    int resultCode = result.getResultCode();
                    if (resultCode == RESULT_CANCELED) {
                        return;
                    }
                    if (resultCode == RESULT_OK) {
                        setResult(RESULT_OK, result.getData());
                        finish();
                        return;
                    }
                    Toast.makeText(this, "No request", Toast.LENGTH_SHORT).show();
                });
    }
}