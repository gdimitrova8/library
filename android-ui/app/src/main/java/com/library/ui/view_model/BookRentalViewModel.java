package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.book.BookRentalVo; 
import com.library.rest.api.vo.list.BookRentalsListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class BookRentalViewModel extends AbstractViewModel<BookRentalVo, BookRentalsListVo> {

   public BookRentalViewModel(@NonNull Application application) {
      super(BookRentalVo.class, BookRentalsListVo.class, "/book/rental", application);
   }

}
