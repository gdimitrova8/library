package com.library.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.library.rest.api.vo.user.RolesVo;
import com.library.ui.R;

public class OperatorMenuView extends AppCompatActivity {

    public static final int ADD_USER_REQUEST = 1;
    public static final int ADD_RENT_BOOK_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.operator_menu);

        Button addReaderButton = findViewById(R.id.add_reader_btn);
        Button delReaderButton = findViewById(R.id.delete_reader_btn);
        Button rentBookButton = findViewById(R.id.rent_book_btn);
        Button returnBookButton = findViewById(R.id.return_a_book_btn);

        addReaderButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, UserView.class);
            intent.putExtra(UserView.EXTRA_ROLE, RolesVo.READER.name());
            intent.putExtra(UserView.EXTRA_MODE, UserView.ADD_READER_FORM_MODE);
            startActivityForResult(intent, ADD_USER_REQUEST);
        });
        rentBookButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, BookRentalView.class);
            intent.putExtra(BookRentalView.EXTRA_MODE, BookRentalView.CREATE_MODE);
            startActivityForResult(intent, ADD_RENT_BOOK_REQUEST);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_USER_REQUEST && resultCode == RESULT_OK) {
            Toast.makeText(this, "Successfully saved reader!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (requestCode == ADD_RENT_BOOK_REQUEST && resultCode == RESULT_OK) {
            Toast.makeText(this, "Successfully rent book!", Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this, "No event for the menu request", Toast.LENGTH_SHORT).show();
    }

}
