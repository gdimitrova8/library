package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.book.BookSerieVo; 
import com.library.rest.api.vo.list.BookSeriesListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class BookSerieViewModel extends AbstractViewModel<BookSerieVo, BookSeriesListVo> {

   public BookSerieViewModel(@NonNull Application application) {
      super(BookSerieVo.class, BookSeriesListVo.class, "/book/serie", application);
   }

}
