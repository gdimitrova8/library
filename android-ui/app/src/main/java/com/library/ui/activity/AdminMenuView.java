package com.library.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.user.RolesVo;
import com.library.ui.R;
import com.library.ui.reports.ReportsMenu;

public class AdminMenuView extends AppCompatActivity {

    ActivityResultLauncher<Intent> addUserResult = create("Successfully saved user!");
    ActivityResultLauncher<Intent> addBookResult = create("Successfully added book!");
    ActivityResultLauncher<Intent> disposedBookResult = create("Successfully disposed book!");
    ActivityResultLauncher<Intent> archiveBookResult = create("Successfully archived book!");
    ActivityResultLauncher<Intent> reportsResult = create("Successfully reports opening!");
    ActivityResultLauncher<Intent> logoutResult = create("Successfully logout!");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_menu);

        Button addAdminButton = findViewById(R.id.add_admin_btn);
        Button addOperatorButton = findViewById(R.id.add_op_btn);
        Button addBookButton = findViewById(R.id.add_new_book_btn);
        Button disposalButton = findViewById(R.id.disposal_of_damaged_books_btn);
        Button archiveBookButton = findViewById(R.id.archive_book_btn);
        Button reportsButton = findViewById(R.id.reports_btn);
        Button logoutButton = findViewById(R.id.logout_btn);

        addAdminButton.setOnClickListener(v -> {
            launchUserResult(RolesVo.ADMINISTRATOR);
        });
        addOperatorButton.setOnClickListener(v -> {
            launchUserResult(RolesVo.OPERATOR);
        });
        disposalButton.setOnClickListener(v -> {
            launchBookResult(disposedBookResult, BookView.DELETE_MODE);
        });
        addBookButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, BookView.class);
            intent.putExtra(BookView.EXTRA_MODE, BookView.CREATE_MODE);
            addBookResult.launch(intent);
        });
        archiveBookButton.setOnClickListener(v -> {
            launchBookResult(archiveBookResult, BookView.ARCHIVE_MODE);
        });
        reportsButton.setOnClickListener(v -> {
            reportsResult.launch(new Intent(this, ReportsMenu.class));
        });
        logoutButton.setOnClickListener(v -> {
            logoutResult.launch(new Intent(this, LoginView.class));
        });
    }

    private ActivityResultLauncher<Intent> create(String successMsg) {
        return registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    int resultCode = result.getResultCode();
                    if (resultCode == RESULT_CANCELED) {
                        return;
                    }
                    if (resultCode == RESULT_OK) {
                        Toast.makeText(this, successMsg, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Toast.makeText(this, "No event", Toast.LENGTH_SHORT).show();
                });
    }

    private void launchUserResult(RolesVo rolesVo) {
        Intent intent = new Intent(this, UserView.class);
        intent.putExtra(UserView.EXTRA_ROLE, rolesVo.name());
        addUserResult.launch(intent);
    }

    private void launchBookResult(ActivityResultLauncher<Intent> bookResultLauncher, String mode) {
        Intent intent = new Intent(this, SearchEntityView.class);
        intent.putExtra(SearchEntityView.ENTITY_NAME, BookVo.class.getName());
        intent.putExtra(BookView.EXTRA_MODE, mode);
        bookResultLauncher.launch(intent);
    }
}
