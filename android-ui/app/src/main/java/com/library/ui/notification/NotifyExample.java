package com.library.ui.notification;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.core.app.NotificationCompat;

import com.library.ui.R;

import static androidx.core.app.NotificationCompat.BADGE_ICON_LARGE;


public class NotifyExample extends Activity {
    public final static String NOT_CHANNEL_ID = "chanel-01-id";
    public final static String NOTIFICATION_CHANNEL_NAME = "chanel-01-name";
    Button b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_test_view);

        b1 = findViewById(R.id.button);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // addNotification("Sub text alabala","Notifications Example","This is a notification message");
                addNotification("Books for archiving", "Reminder", "Required 12 books for archiving.");
            }
        });
    }

    private void addNotification(String subText, String contentTitle, String contentText) {
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel notificationChannel = new NotificationChannel(NOT_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            manager.createNotificationChannel(notificationChannel);
        }
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_magic_book_square_icon);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this, NOT_CHANNEL_ID)
                        .setSubText(subText)
                        .setCategory("Category 1")
                        .setBadgeIconType(BADGE_ICON_LARGE)
                        .setLargeIcon(largeIcon)
                        .setSmallIcon(R.drawable.ic_open_book) //set icon for notification
                        .setContentTitle(contentTitle) //set title of notification
                        .setContentText(contentText)//this is notification message
                        .setAutoCancel(true) // makes auto cancel of notification
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT); //set priority of notification


        Intent notificationIntent = new Intent(this, NotificationView.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //notification message will get at NotificationView
        notificationIntent.putExtra("message", "This is a notification message");

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        // Add as notifications
        manager.notify(0, builder.build());
    }
}