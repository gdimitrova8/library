package com.library.ui.notification;

import android.os.Bundle;
import android.app.Activity;
import android.widget.TextView;

import com.library.ui.R;

public class NotificationView extends Activity{
    TextView text;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_view);
        text = findViewById(R.id.text);
        //getting the notification message
        String message=getIntent().getStringExtra("message");
        text.setText(message);
    }
}