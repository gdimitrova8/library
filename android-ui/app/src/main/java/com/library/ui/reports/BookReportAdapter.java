package com.library.ui.reports;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.library.rest.api.vo.book.BookSerieVo;
import com.library.rest.api.vo.book.BookStatesVo;
import com.library.rest.api.vo.book.BookStatusVo;
import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.book.PublisherVo;
import com.library.ui.R;

import org.jetbrains.annotations.NotNull;

public class BookReportAdapter extends ListAdapter<BookVo, BookReportAdapter.BookVoHolder> {
    private OnItemClickListener listener;

    public BookReportAdapter() {
        super(new DiffUtil.ItemCallback<BookVo>() {
            @Override
            public boolean areItemsTheSame(@NotNull BookVo oldItem, @NotNull BookVo newItem) {
                return oldItem.getId().equals(newItem.getId());
            }

            @Override
            public boolean areContentsTheSame(@NotNull BookVo oldItem, @NotNull BookVo newItem) {
                return oldItem.getId().equals(newItem.getId()) && oldItem.equals(newItem);
            }
        });
    }

    @NonNull
    @Override
    public BookVoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_report_item, parent, false);
        return new BookVoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BookVoHolder holder, int position) {
        BookVo current = getBookVoAt(position);
        holder.inventoryNumber.setText(current.getInventoryNumber());
        holder.title.setText(current.getTitle());
        holder.author.setText(current.getAuthors().get(0).getName());
        holder.signature.setText(current.getSignature());
        holder.isbn.setText(current.getIsbn());
        BookSerieVo serie = current.getSerie();
        holder.serie.setText(serie != null ? serie.getName() : "");
        holder.publishY.setText(current.getPublishYear().toString());
        PublisherVo publisher = current.getPublisher();
        holder.publisher.setText(publisher != null ? publisher.getName() : "");
        BookStatesVo state = current.getState();
        holder.state.setText(state != null ? state.name() : "");
        BookStatusVo status = current.getStatus();
        holder.status.setText(status != null ? status.name() : "");
    }


    public BookVo getBookVoAt(int position) {
        return getItem(position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(BookVo user);
    }

    class BookVoHolder extends RecyclerView.ViewHolder {
        private TextView inventoryNumber;
        private TextView title;
        private TextView author;
        private TextView signature;
        private TextView isbn;
        private TextView serie;
        private TextView publishY;
        private TextView publisher;
        private TextView status;
        private TextView state;


        public BookVoHolder(View itemView) {
            super(itemView);
            inventoryNumber = itemView.findViewById(R.id.inventoryNumber);
            title = itemView.findViewById(R.id.title);
            author = itemView.findViewById(R.id.author);
            signature = itemView.findViewById(R.id.signature);
            isbn = itemView.findViewById(R.id.isbn);
            serie = itemView.findViewById(R.id.serie);
            publishY = itemView.findViewById(R.id.publishY);
            publisher = itemView.findViewById(R.id.publisher);
            status = itemView.findViewById(R.id.status);
            state = itemView.findViewById(R.id.state);

            itemView.setOnClickListener(v -> {
                int position = getBindingAdapterPosition();
                if (listener != null && position != RecyclerView.NO_POSITION) {
                    listener.onItemClick(getBookVoAt(position));
                }
            });
        }
    }
}
