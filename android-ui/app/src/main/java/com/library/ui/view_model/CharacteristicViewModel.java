package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.book.CharacteristicVo; 
import com.library.rest.api.vo.list.CharacteristicsListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class CharacteristicViewModel extends AbstractViewModel<CharacteristicVo, CharacteristicsListVo> {

   public CharacteristicViewModel(@NonNull Application application) {
      super(CharacteristicVo.class, CharacteristicsListVo.class, "/characteristic", application);
   }

}
