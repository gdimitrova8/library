package com.library.ui.view_model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.library.rest.api.vo.user.UserVo; 
import com.library.rest.api.vo.list.UsersListVo; 

/**
 *
 * Generated file. Template : code.generator.templates.android.ViewModelRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class UserViewModel extends AbstractViewModel<UserVo, UsersListVo> {

   public UserViewModel(@NonNull Application application) {
      super(UserVo.class, UsersListVo.class, "/user", application);
   }

}
