package com.library.dao;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author gdimitrova
 */
public interface DbConfig {

    public final static String DRIVER_KEY = "javax.persistence.jdbc.driver";

    public final static String URL_KEY = "javax.persistence.jdbc.url";

    public final static String USER_KEY = "javax.persistence.jdbc.user";

    public final static String PASSWORD_KEY = "javax.persistence.jdbc.password";

    public final static String DIALECT_KEY = "hibernate.dialect";

    public final static String SHOW_SQL_KEY = "hibernate.show_sql";

    public String getDialect();

    public String getDriver();

    public String getURL();

    public String getUsername();

    public String getPassword();

    public Boolean showSql();

    public void setDialect(String dialect);

    public void setDriver(String driver);

    public void setURL(String url);

    public void setUsername(String username);

    public void setPassword(String password);

    public void showSql(Boolean showSql);

    public default void setProperties(Map<String, String> props) {
        setDriver(props.get(DRIVER_KEY));
        setURL(props.get(URL_KEY));
        setUsername(props.get(USER_KEY));
        setPassword(props.get(PASSWORD_KEY));
        setDialect(props.get(DIALECT_KEY));
        showSql(Boolean.valueOf(props.get(SHOW_SQL_KEY)));
    }

    public default Map<String, String> getProperties() {
        Map<String, String> props = new HashMap<>();
        props.put(DRIVER_KEY, getDriver());
        props.put(URL_KEY, getURL());
        props.put(USER_KEY, getUsername());
        props.put(PASSWORD_KEY, getPassword());
        props.put(DIALECT_KEY, getDialect());
        props.put(SHOW_SQL_KEY, showSql().toString());
        return props;
    }
}
