/*
 * EuroRisk Systems (c) Ltd. All rights reserved.
 */
package com.library.dao;

import com.library.domain.Entity;
import com.library.domain.filter.Filter;
import com.library.domain.list.EntityList;

/**
 *
 * @author gdimitrova
 * @param <List>
 * @param <E>
 */
public interface SearchDao< E extends Entity, List extends EntityList<E>> extends CrudDao<E> {

    public List search(Filter filter);
    
    public int searchCount(Filter filter);

}
