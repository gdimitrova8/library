package com.library.dao;

import com.library.domain.filter.Filter;
import com.library.domain.list.UsersList;
import com.library.domain.user.User;
import java.util.List;

/**
 *
 * @author gdimitrova
 */
public interface UserDao extends SearchDao<User, UsersList> {

    public User load(String username, String pass);

    public User load(String username);

    public List<User> searchOperators(Filter filter);

    public List<User> searchReaders(Filter filter);

}
