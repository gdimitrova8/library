package com.library.domain.exchanger;

import com.library.domain.Entity;

/**
 *
 * @author gdimitrova
 * @param <D>
 * @param <E>
 */
public interface EntityExchanger< D, E extends Entity> extends ValidatableEntityExchanger<D, E> {

    public E exchangeId(D from, E to);

    public D exchangeId(E from, D to);

}
