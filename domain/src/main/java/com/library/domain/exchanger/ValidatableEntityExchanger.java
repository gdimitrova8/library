package com.library.domain.exchanger;

import com.library.domain.validation.Validatable;
import java.util.List;

/**
 *
 * @author gdimitrova
 * @param <D>
 * @param <E>
 */
public interface ValidatableEntityExchanger< D, E extends Validatable> {

    public E exchange(D dto);

    public D exchange(E e);

    public List<D> exchangeAllFrom(List<E> list);

    public List<E> exchangeAll(List<D> list);
}
