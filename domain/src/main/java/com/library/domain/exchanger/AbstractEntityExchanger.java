package com.library.domain.exchanger;

import com.library.domain.Entity;

/**
 *
 * @author gdimitrova
 * @param <D>
 * @param <E>
 */
public abstract class AbstractEntityExchanger< D, E extends Entity> extends ValidatableExchanger<D, E> implements EntityExchanger<D, E>{

    @Override
    public E exchange(D from) {
        if (from == null) {
            return null;
        }
        return exchangeId(from, exchangeFrom(from));
    }

    @Override
    public D exchange(E from) {
        if (from == null) {
            return null;
        }
        return exchangeId(from, exchangeFrom(from));
    }

}
