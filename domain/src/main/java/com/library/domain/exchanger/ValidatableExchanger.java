package com.library.domain.exchanger;

import com.library.domain.Entity;
import com.library.domain.validation.Validatable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 *
 * @author gdimitrova
 * @param <D>
 * @param <E>
 */
public abstract class ValidatableExchanger< D, E extends Validatable> implements ValidatableEntityExchanger<D, E> {

    @Override
    public E exchange(D from) {
        if (from == null) {
            return null;
        }
        return exchangeFrom(from);
    }

    @Override
    public D exchange(E from) {
        if (from == null) {
            return null;
        }
        return exchangeFrom(from);
    }

    protected <T> void exchange(Consumer<T> valueSetter, Supplier<T> valueGetter) {
        valueSetter.accept(valueGetter.get());
    }

    protected <T> void exchange(Consumer<Long> valueSetter, Calendar c) {
        if (c != null) {
            valueSetter.accept(c.getTimeInMillis());
        }
    }

    protected <T> void exchange(Consumer<Calendar> valueSetter, Long ms) {
        if (ms == null) {
            return;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(ms);
        valueSetter.accept(instance);
    }

    abstract protected E exchangeFrom(D from);

    abstract protected D exchangeFrom(E e);

    @Override
    public List<D> exchangeAllFrom(List<E> list) {
        return this.exchangeAll(list, this::exchange);
    }

    @Override
    public List<E> exchangeAll(List<D> list) {
        return this.exchangeAll(list, this::exchange);
    }

    public <F, T> List<T> exchangeAll(List<F> from, Function<F, T> exchange) {
        if (from == null || from.isEmpty()) {
            return new ArrayList<>();
        }
        return from.stream().collect(
                ArrayList::new,
                ((to, f) -> {
                    to.add(exchange.apply(f));
                }),
                ((to1, to2) -> {
                    to1.addAll(to2);
                })
        );
    }

}
