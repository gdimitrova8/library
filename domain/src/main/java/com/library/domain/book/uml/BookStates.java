package com.library.domain.book.uml;

import com.library.domain.book.*;

/**
 *
 * Generated file. Template : code.generator.templates.domain.EnumRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public enum BookStates {

   NEW, OLD, DEFECTIVE

}
