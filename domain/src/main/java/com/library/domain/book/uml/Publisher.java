package com.library.domain.book.uml;

import com.library.domain.book.*;
import java.util.Objects;
import com.library.domain.NamedEntity;
/**
 *
 * Generated file. Template : code.generator.templates.domain.DomainEntityRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class Publisher extends NamedEntity {

   public Publisher(){
   }

   public Publisher(String name) {
      super(name);
   }

   @Override
   public int hashCode() {
      int hash = 9;
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final Publisher other = (Publisher) obj;
      return super.equals(other);
   }

}
