package com.library.domain.book.uml;

import com.library.domain.book.*;
import java.util.Objects;
import com.library.domain.NamedEntity;
/**
 *
 * Generated file. Template : code.generator.templates.domain.DomainEntityRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class Characteristic extends NamedEntity {

   public Characteristic(){
   }

   public Characteristic(String name) {
      super(name);
   }

   @Override
   public int hashCode() {
      int hash = 5;
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final Characteristic other = (Characteristic) obj;
      return super.equals(other);
   }

}
