package com.library.domain.book.uml;

import com.library.domain.message.*;

/**
 *
 * @author gdimitrova
 */
public class Message {

    private final String messageKey;

    public Message(String messageKey) {
        this.messageKey = messageKey;
    }

    public String getMessageKey() {
        return messageKey;
    }

}
