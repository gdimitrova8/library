package com.library.domain.book.uml;

import com.library.domain.book.*;
import java.util.Objects;
import com.library.domain.NamedEntity;
/**
 *
 * Generated file. Template : code.generator.templates.domain.DomainEntityRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class WorkForm extends NamedEntity {

   public WorkForm(){
   }

   public WorkForm(String name) {
      super(name);
   }

   @Override
   public int hashCode() {
      int hash = 99;
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final WorkForm other = (WorkForm) obj;
      return super.equals(other);
   }

}
