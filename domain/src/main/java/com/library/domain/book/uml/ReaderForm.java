package com.library.domain.book.uml;

import com.library.domain.user.*;
import java.util.Objects;
import com.library.domain.message.Message;
import com.library.domain.validation.ValidationUtils;
import java.util.ArrayList;
import java.util.List;
import com.library.domain.Entity;
import java.util.Calendar;
/**
 *
 * Generated file. Template : code.generator.templates.domain.DomainEntityRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class ReaderForm extends Entity {

   public final static Message MISSING_SEND_DATE = new Message(
   	 "com.library.domain.user.ReaderForm.missing.send.date"
   );

   public final static Message MISSING_STATUS = new Message(
   	 "com.library.domain.user.ReaderForm.missing.status"
   );

   public final static Message MISSING_USERNAME = new Message(
   	 "com.library.domain.user.ReaderForm.missing.user"
   );

   public final static Message MISSING_ACCEPTED_DATE = new Message(
   	 "com.library.domain.user.ReaderForm.missing.accepted.date"
   );

   private Calendar sendDate;

   private FormStatus formStatus;

   private User user;

   private Calendar acceptedDate;

   public ReaderForm(){
   }

   public ReaderForm(Calendar sendDate, FormStatus formStatus, User user, Calendar acceptedDate) {
      this.sendDate = sendDate;
      this.formStatus = formStatus;
      this.user = user;
      this.acceptedDate = acceptedDate;
   }

   public void setSendDate(Calendar sendDate) {
      this.sendDate = sendDate;
   }

   public Calendar getSendDate() {
      return this.sendDate;
   }

   public void setFormStatus(FormStatus formStatus) {
      this.formStatus = formStatus;
   }

   public FormStatus getFormStatus() {
      return this.formStatus;
   }

   public void setUser(User user) {
      this.user = user;
   }

   public User getUser() {
      return this.user;
   }

   public void setAcceptedDate(Calendar acceptedDate) {
      this.acceptedDate = acceptedDate;
   }

   public Calendar getAcceptedDate() {
      return this.acceptedDate;
   }

   @Override
   public int hashCode() {
      int hash = 55;
      hash = 25 * hash + Objects.hashCode(this.sendDate);
      hash = 25 * hash + Objects.hashCode(this.formStatus);
      hash = 25 * hash + Objects.hashCode(this.user);
      hash = 25 * hash + Objects.hashCode(this.acceptedDate);
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final ReaderForm other = (ReaderForm) obj;
      if (!Objects.equals(this.sendDate, other.sendDate)) {
         return false;
      }
      if (!Objects.equals(this.formStatus, other.formStatus)) {
         return false;
      }
      if (!Objects.equals(this.user, other.user)) {
         return false;
      }
      return Objects.equals(this.acceptedDate, other.acceptedDate);
   }

   @Override
   public List<Message> validate() {
      List<Message> errors = new ArrayList<>();
      ValidationUtils.validate(sendDate, MISSING_SEND_DATE, errors);
      ValidationUtils.validate(formStatus, MISSING_STATUS, errors);
      ValidationUtils.validate(user, MISSING_USERNAME, errors);
      ValidationUtils.validate(acceptedDate, MISSING_ACCEPTED_DATE, errors);
      return errors;
   }

}
