package com.library.domain.book.uml;

import com.library.domain.book.signature.*;
import java.util.Objects;
import com.library.domain.message.Message;
import com.library.domain.validation.ValidationUtils;
import java.util.ArrayList;
import java.util.List;
import com.library.domain.NamedEntity;
/**
 *
 * Generated file. Template : code.generator.templates.domain.DomainEntityRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public abstract class Signature extends NamedEntity {

   public final static Message MISSING_OR_EMPTY_ABBREVIATION = new Message(
   	 "com.library.domain.book.signature.Signature.missing.or.empty.abbreviation"
   );

   private String abbreviation;

   public Signature(){
   }

   public Signature(String name, String abbreviation) {
      super(name);
      this.abbreviation = abbreviation;
   }

   public void setAbbreviation(String abbreviation) {
      this.abbreviation = abbreviation;
   }

   public String getAbbreviation() {
      return this.abbreviation;
   }

   @Override
   public int hashCode() {
      int hash = 25;
      hash = 81 * hash + Objects.hashCode(this.abbreviation);
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final Signature other = (Signature) obj;
      return Objects.equals(this.abbreviation, other.abbreviation);
   }

   @Override
   public List<Message> validate() {
      List<Message> errors = super.validate();
      ValidationUtils.validate(abbreviation, MISSING_OR_EMPTY_ABBREVIATION, errors);
      return errors;
   }

}
