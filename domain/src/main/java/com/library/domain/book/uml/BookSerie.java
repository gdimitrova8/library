package com.library.domain.book.uml;

import com.library.domain.book.*;
import java.util.Objects;
import com.library.domain.NamedEntity;
/**
 *
 * Generated file. Template : code.generator.templates.domain.DomainEntityRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class BookSerie extends NamedEntity {

   public BookSerie(){
   }

   public BookSerie(String name) {
      super(name);
   }

   @Override
   public int hashCode() {
      int hash = 4;
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final BookSerie other = (BookSerie) obj;
      return super.equals(other);
   }

}
