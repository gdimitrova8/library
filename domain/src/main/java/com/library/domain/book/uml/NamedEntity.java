package com.library.domain.book.uml;

import com.library.domain.*;
import java.util.Objects;
import com.library.domain.message.Message;
import com.library.domain.validation.ValidationUtils;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * Generated file. Template : code.generator.templates.domain.DomainEntityRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public abstract class NamedEntity extends Entity {

   public final static Message MISSING_OR_EMPTY_NAME = new Message(
   	 "com.library.domain.NamedEntity.missing.or.empty.name"
   );

   private String name;

   public NamedEntity(){
   }

   public NamedEntity(String name) {
      this.name = name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getName() {
      return this.name;
   }

   @Override
   public int hashCode() {
      int hash = 2;
      hash = 11 * hash + Objects.hashCode(this.name);
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final NamedEntity other = (NamedEntity) obj;
      return Objects.equals(this.name, other.name);
   }

   @Override
   public List<Message> validate() {
      List<Message> errors = new ArrayList<>();
      ValidationUtils.validate(name, MISSING_OR_EMPTY_NAME, errors);
      return errors;
   }

}
