package com.library.domain.book.uml;

import com.library.domain.*;
import java.util.Objects;
import com.library.domain.validation.Validatable;
/**
 *
 * Generated file. Template : code.generator.templates.domain.DomainEntityRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public abstract class Entity implements Validatable {

   private String id;

   public void setId(String id) {
      this.id = id;
   }

   public String getId() {
      return this.id;
   }

   @Override
   public int hashCode() {
      int hash = 2;
      hash = 10 * hash + Objects.hashCode(this.id);
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final Entity other = (Entity) obj;
      return Objects.equals(this.id, other.id);
   }

}
