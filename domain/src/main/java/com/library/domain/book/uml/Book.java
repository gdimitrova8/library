package com.library.domain.book.uml;

import com.library.domain.book.*;
import com.library.domain.Entity;
import com.library.domain.book.signature.FormatSignature;
import com.library.domain.book.signature.StockSignature;
import com.library.domain.message.Message;
import com.library.domain.validation.ValidationUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author gdimitrova
 */
public class Book extends Entity {

    private String title;

    private String signature;

    private BookStates state;

    private BookStatus status;

    private Publisher publisher;

    private Integer publishYear;

    private WorkForm form;

    // private Author author;
    private List<Author> authors;

    private BookSerie serie;

    private String inventoryNumber;

    private String isbn;

    private StockSignature stockSignature;

    private FormatSignature formatSignature;

    private List<Genre> genres = new ArrayList<>();

    private List<Characteristic> characteristics = new ArrayList<>();

    public Book() {
    }

    public Book(String title, String signature, BookStates state,
            BookStatus status, Publisher publisher, Integer publishYear,
            WorkForm form, List<Author> authors, BookSerie serie, String inventoryNumber,
            String isbn, StockSignature stockSignature, FormatSignature formatSignature) {
        this.title = title;
        this.signature = signature;
        this.state = state;
        this.status = status;
        this.publisher = publisher;
        this.publishYear = publishYear;
        this.form = form;
        this.authors = authors;
        this.serie = serie;
        this.inventoryNumber = inventoryNumber;
        this.isbn = isbn;
        this.stockSignature = stockSignature;
        this.formatSignature = formatSignature;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public BookStates getState() {
        return state;
    }

    public void setState(BookStates state) {
        this.state = state;
    }

    public BookStatus getStatus() {
        return status;
    }

    public void setStatus(BookStatus status) {
        this.status = status;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Integer getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(Integer publishYear) {
        this.publishYear = publishYear;
    }

    public WorkForm getForm() {
        return form;
    }

    public void setForm(WorkForm form) {
        this.form = form;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public BookSerie getSerie() {
        return serie;
    }

    public void setSerie(BookSerie serie) {
        this.serie = serie;
    }

    public String getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(String inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public StockSignature getStockSignature() {
        return stockSignature;
    }

    public void setStockSignature(StockSignature stockSignature) {
        this.stockSignature = stockSignature;
    }

    public FormatSignature getFormatSignature() {
        return formatSignature;
    }

    public void setFormatSignature(FormatSignature formatSignature) {
        this.formatSignature = formatSignature;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres != null ? genres : new ArrayList<>();
    }

    public List<Characteristic> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(List<Characteristic> characteristics) {
        this.characteristics = characteristics != null ? characteristics : new ArrayList<>();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.title);
        hash = 17 * hash + Objects.hashCode(this.signature);
        hash = 17 * hash + Objects.hashCode(this.state);
        hash = 17 * hash + Objects.hashCode(this.status);
        hash = 17 * hash + Objects.hashCode(this.publisher);
        hash = 17 * hash + Objects.hashCode(this.publishYear);
        hash = 17 * hash + Objects.hashCode(this.form);
        hash = 17 * hash + Objects.hashCode(this.authors);
        hash = 17 * hash + Objects.hashCode(this.serie);
        hash = 17 * hash + Objects.hashCode(this.inventoryNumber);
        hash = 17 * hash + Objects.hashCode(this.isbn);
        hash = 17 * hash + Objects.hashCode(this.genres);
        hash = 17 * hash + Objects.hashCode(this.characteristics);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Book other = (Book) obj;
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.signature, other.signature)) {
            return false;
        }
        if (!Objects.equals(this.inventoryNumber, other.inventoryNumber)) {
            return false;
        }
        if (!Objects.equals(this.isbn, other.isbn)) {
            return false;
        }
        if (this.state != other.state) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.publisher, other.publisher)) {
            return false;
        }
        if (!Objects.equals(this.publishYear, other.publishYear)) {
            return false;
        }
        if (!Objects.equals(this.form, other.form)) {
            return false;
        }
        if (!Objects.equals(this.serie, other.serie)) {
            return false;
        }
        if (!this.authors.containsAll(other.authors)) {
            return false;
        }
        if (!this.genres.containsAll(other.genres)) {
            return false;
        }
        return this.characteristics.containsAll(other.characteristics);
    }

    private final static Message MISSING_OR_EMPTY_TITLE = new Message(
            "com.library.domain.book.Book.missing.or.empty.title"
    );

    private final static Message MISSING_OR_EMPTY_SIGNATURE = new Message(
            "com.library.domain.book.Book.missing.or.empty.signature"
    );

    private final static Message MISSING_OR_EMPTY_INVENTORY_NUMBER = new Message(
            "com.library.domain.book.Book.missing.or.empty.inventory.number"
    );

    private final static Message MISSING_OR_EMPTY_ISBN = new Message(
            "com.library.domain.book.Book.missing.or.empty.isbn"
    );

    private final static Message MISSING_PUBLISHER = new Message(
            "com.library.domain.book.Book.missing.publish"
    );

    private final static Message MISSING_PUBLISH_YEAR = new Message(
            "com.library.domain.book.Book.missing.publish.year"
    );

    private final static Message MISSING_FORM = new Message(
            "com.library.domain.book.Book.missing.work.form"
    );

    private final static Message MISSING_AUTHOR = new Message(
            "com.library.domain.book.Book.missing.author"
    );

    private final static Message MISSING_GENRE = new Message(
            "com.library.domain.book.Book.missing.genre"
    );

    private final static Message MISSING_CHARACTERISTICS = new Message(
            "com.library.domain.book.Book.missing.characteristics"
    );

    private final static Message MISSING_STOCK_SIGNATURE = new Message(
            "com.library.domain.book.Book.missing.stock.signature"
    );

    private final static Message MISSING_FORMAT_SIGNATURE = new Message(
            "com.library.domain.book.Book.missing.format.signature"
    );

    @Override
    public List<Message> validate() {
        List<Message> errors = new ArrayList<>();
        ValidationUtils.validate(title, MISSING_OR_EMPTY_TITLE, errors);
        ValidationUtils.validate(signature, MISSING_OR_EMPTY_SIGNATURE, errors);
        ValidationUtils.validate(inventoryNumber, MISSING_OR_EMPTY_INVENTORY_NUMBER, errors);
        ValidationUtils.validate(isbn, MISSING_OR_EMPTY_ISBN, errors);
        ValidationUtils.validate(publisher, MISSING_PUBLISHER, errors);
        ValidationUtils.validate(publishYear, MISSING_PUBLISH_YEAR, errors);
        ValidationUtils.validate(form, MISSING_FORM, errors);
        ValidationUtils.validate(authors, MISSING_AUTHOR, errors);
        ValidationUtils.validate(genres, MISSING_GENRE, errors);
        ValidationUtils.validate(characteristics, MISSING_CHARACTERISTICS, errors);
        return errors;
    }

}
