package com.library.domain.book.uml;

import com.library.domain.book.*;
import java.util.Objects;
import com.library.domain.NamedEntity;
import java.util.Calendar;
/**
 *
 * Generated file. Template : code.generator.templates.domain.DomainEntityRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class Author extends NamedEntity {

   private String birthPlace;

   private String biography;

   private Calendar birthDate;

   public Author(){
   }

   public Author(String name, String birthPlace, String biography, Calendar birthDate) {
      super(name);
      this.birthPlace = birthPlace;
      this.biography = biography;
      this.birthDate = birthDate;
   }

   public void setBirthPlace(String birthPlace) {
      this.birthPlace = birthPlace;
   }

   public String getBirthPlace() {
      return this.birthPlace;
   }

   public void setBiography(String biography) {
      this.biography = biography;
   }

   public String getBiography() {
      return this.biography;
   }

   public void setBirthDate(Calendar birthDate) {
      this.birthDate = birthDate;
   }

   public Calendar getBirthDate() {
      return this.birthDate;
   }

   @Override
   public int hashCode() {
      int hash = 1;
      hash = 11 * hash + Objects.hashCode(this.birthPlace);
      hash = 11 * hash + Objects.hashCode(this.biography);
      hash = 11 * hash + Objects.hashCode(this.birthDate);
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final Author other = (Author) obj;
      if (!Objects.equals(this.birthPlace, other.birthPlace)) {
         return false;
      }
      if (!Objects.equals(this.biography, other.biography)) {
         return false;
      }
      return Objects.equals(this.birthDate, other.birthDate);
   }

}
