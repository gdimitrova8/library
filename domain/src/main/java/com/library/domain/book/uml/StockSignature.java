package com.library.domain.book.uml;

import com.library.domain.book.signature.*;
import java.util.Objects;
/**
 *
 * Generated file. Template : code.generator.templates.domain.DomainEntityRenderer.java
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class StockSignature extends Signature {

   public StockSignature(){
   }

   public StockSignature(String abbreviation, String name) {
      super(abbreviation, name);
   }

   @Override
   public int hashCode() {
      int hash = 17;
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final StockSignature other = (StockSignature) obj;
      return super.equals(other);
   }

}
