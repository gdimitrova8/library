package com.library.domain.list;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author gdimitrova
 * @param <T>
 */
public abstract class EntityList<T> implements Serializable {

    private List<T> entities;

    public EntityList() {
    }

    public EntityList(List<T> entities) {
        this.entities = entities;
    }

    public List<T> getEntities() {
        return entities;
    }

    public void setEntities(List<T> entities) {
        this.entities = entities;
    }

}
