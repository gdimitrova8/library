package com.library.domain.validation;

import com.library.domain.message.Message;
import java.util.List;
import java.util.Calendar;

public class ValidationUtils {

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static void validate(String obj, Message missingMsg, List<Message> messages) {
        if (isNull(obj) || obj.isBlank() || obj.isEmpty()) {
            messages.add(missingMsg);
        }
    }

    public static void validate(Object obj, Message missingMsg, List<Message> messages) {
        if (isNull(obj)) {
            messages.add(missingMsg);
        }
    }

    public static void validate(Validatable obj, Message missingMsg, List<Message> messages) {
        if (isNull(obj)) {
            messages.add(missingMsg);
            return;
        }
        validate(obj, messages);
    }

    public static void validate(Calendar before, Calendar after, Message msg, List<Message> messages) {
        if (before.after(after)) {
            messages.add(msg);
        }
    }

    public static <E extends Validatable> void validate(List<E> list, List<Message> messages) {
        list.forEach(o -> validate(o, messages));
    }

    private static void validate(Validatable obj, List<Message> messages) {
        messages.addAll(obj.validate());
    }

}
