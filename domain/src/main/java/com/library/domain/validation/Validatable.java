package com.library.domain.validation;

import com.library.domain.message.Message;
import java.util.List;

public interface Validatable {

    public List<Message> validate();
}
