package com.library.domain.filter;

import com.library.domain.validation.Validatable;

/**
 *
 * @author gdimitrova
 */
public interface SearchCfg extends Validatable{

    public String getText();

    public void setText(String text);

    public String getOrderBy();

    public void setOrderBy(String orderBy);

    public String getSearchBy();

    public void setSearchBy(String searchBy);
}
