package com.library.domain.filter;

import com.library.domain.message.Message;
import com.library.domain.validation.ValidationUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author gdimitrova
 */
public class SearchConfig  implements SearchCfg {
    
    private final static Message MISSING_OR_EMPTY_TEXT = new Message(
            "com.library.domain.filter.SearchConfig.missing.or.empty.text"
    );
    private final static Message MISSING_OR_EMPTY_ORDER_BY= new Message(
            "com.library.domain.filter.SearchConfig.missing.or.empty.order.by"
    );
    private final static Message MISSING_OR_EMPTY_SEARCH_BY = new Message(
            "com.library.domain.filter.SearchConfig.missing.or.empty.search.by"
    );


    private String text;

    private String orderBy;

    private String searchBy;

    public SearchConfig() {
    }

    public SearchConfig(String text, String orderBy, String searchBy) {
        this.text = text;
        this.orderBy = orderBy;
        this.searchBy = searchBy;
    }

    @Override
    public int hashCode() {
        int hash = 16;
        hash = 67 * hash + Objects.hashCode(this.text);
        hash = 67 * hash + Objects.hashCode(this.orderBy);
        hash = 67 * hash + Objects.hashCode(this.searchBy);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SearchConfig other = (SearchConfig) obj;
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.orderBy, other.orderBy)) {
            return false;
        }
        return Objects.equals(this.searchBy, other.searchBy);
    }
    

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String getOrderBy() {
        return orderBy;
    }

    @Override
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public String getSearchBy() {
        return searchBy;
    }

    @Override
    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }

    @Override
    public List<Message> validate() {
        List<Message> errors =new ArrayList<>();
        ValidationUtils.validate(text, MISSING_OR_EMPTY_TEXT, errors);
        ValidationUtils.validate(orderBy, MISSING_OR_EMPTY_ORDER_BY, errors);
        ValidationUtils.validate(searchBy, MISSING_OR_EMPTY_SEARCH_BY, errors);
        return errors;
    }

}
