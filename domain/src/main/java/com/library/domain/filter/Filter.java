package com.library.domain.filter;

import com.library.domain.message.Message;
import java.util.Objects;
import com.library.domain.validation.Validatable;
import com.library.domain.validation.ValidationUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gergana Kuleva Dimitrova
 *
 */
public class Filter implements Validatable {

    private final static Message MISSING_OR_NEGATIVE_START_INDEX = new Message(
            "com.library.domain.filter.Filter.missing.or.negative.start.index"
    );

    private final static Message MISSING_OR_NEGATIVE_MAX_RESULTS = new Message(
            "com.library.domain.filter.Filter.missing.or.negative.max.results"
    );

    private final static Message MISSING_CFG = new Message(
            "com.library.domain.filter.Filter.missing.search.config"
    );

    private Integer startIndex;

    private Integer maxResults;

    private SearchConfig cfg;

    public Filter() {
    }

    public Filter(Integer startIndex, Integer maxResults, SearchConfig cfg) {
        this.startIndex = startIndex;
        this.maxResults = maxResults;
        this.cfg = cfg;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    public Integer getStartIndex() {
        return this.startIndex;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    public Integer getMaxResults() {
        return this.maxResults;
    }

    public SearchConfig getCfg() {
        return cfg;
    }

    public void setCfg(SearchConfig cfg) {
        this.cfg = cfg;
    }

    @Override
    public int hashCode() {
        int hash = 4;
        hash = 88 * hash + Objects.hashCode(this.startIndex);
        hash = 88 * hash + Objects.hashCode(this.maxResults);
        hash = 88 * hash + Objects.hashCode(this.cfg);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Filter other = (Filter) obj;
        if (!Objects.equals(this.startIndex, other.startIndex)) {
            return false;
        }
        if (!Objects.equals(this.maxResults, other.maxResults)) {
            return false;
        }
        return Objects.equals(this.cfg, other.cfg);
    }

    @Override
    public List<Message> validate() {
        List<Message> errors = new ArrayList<>();
        if (startIndex == null || startIndex < 0) {
            errors.add(MISSING_OR_NEGATIVE_START_INDEX);
        }
        if (maxResults == null || maxResults < 0) {
            errors.add(MISSING_OR_NEGATIVE_MAX_RESULTS);
        }
        ValidationUtils.validate(cfg, MISSING_CFG, errors);
        return errors;
    }

}
