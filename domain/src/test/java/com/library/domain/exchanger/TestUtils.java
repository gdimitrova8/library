package com.library.domain.exchanger;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 *
 * @author gdimitrova
 */
public class TestUtils {

    public static <T> List<T> fillList(Integer count, Function<Integer, T> creator) {
        List<T> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            list.add(creator.apply(i));
        }
        return list;
    }
}
