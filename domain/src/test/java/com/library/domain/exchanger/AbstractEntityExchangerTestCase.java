package com.library.domain.exchanger;

import com.library.domain.Entity;
import com.library.domain.validation.Validatable;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

/**
 *
 * @author gdimitrova
 * @param <E>
 * @param <T>
 * @param <X>
 */
public abstract class AbstractEntityExchangerTestCase<E extends Validatable, T, X extends ValidatableEntityExchanger<T, E>> {

    private final X exchanger;

    public AbstractEntityExchangerTestCase(X exchanger) {
        this.exchanger = exchanger;
    }

    @Test
    public void testExchange() {
        E entity = makeEntity();
        T otherEntity = exchanger.exchange(entity);
        assertNotNull(otherEntity, "\nOther entity have to be not null.\n");
        E exchangedEntity = exchanger.exchange(otherEntity);
        assertTrue(entity.equals(exchangedEntity), "\nEntities are not the same.\n");
    }
    
    protected abstract E makeEntity();

}
