package com.library.domain.exchanger;

import com.library.domain.book.Author;
import com.library.domain.book.Book;
import com.library.domain.book.BookRental;
import com.library.domain.book.BookSerie;
import com.library.domain.book.BookStates;
import com.library.domain.book.BookStatus;
import com.library.domain.book.Characteristic;
import com.library.domain.book.Genre;
import com.library.domain.book.Publisher;
import com.library.domain.book.WorkForm;
import com.library.domain.book.signature.FormatSignature;
import com.library.domain.book.signature.StockSignature;
import static com.library.domain.exchanger.TestUtils.fillList;
import com.library.domain.filter.Filter;
import com.library.domain.filter.SearchConfig;
import com.library.domain.user.FormStatus;
import com.library.domain.user.ReaderForm;
import com.library.domain.user.Roles;
import com.library.domain.user.User;
import java.util.Arrays;
import java.util.GregorianCalendar;

/**
 *
 * @author gdimitrova
 */
public class TestEnviroment {

    public static Genre makeGenre() {
        return new Genre("horror");
    }

    public static Characteristic makeCharacteristic() {
        return new Characteristic("novel");
    }

    public static Author makeAuthor() {
        return new Author("Anton White", null, null, null);
    }

    public static Publisher makePublisher() {
        return new Publisher("News");
    }

    public static WorkForm makeWorkForm() {
        return new WorkForm("novel");
    }

    public static Book makeBook() {
        Book book = new Book(
                "Title",
                "46352",
                BookStates.NEW,
                BookStatus.AVAILABLE,
                makePublisher(),
                2010,
                makeWorkForm(),
                Arrays.asList(makeAuthor()),
                null,
                "46352",
                "46234865", null,
                new FormatSignature("A1", "A1 format"));
        book.setGenres(fillList(5, (i) -> new Genre("Genre " + i.toString())));
        book.setCharacteristics(fillList(8, (i) -> new Characteristic("Characteristic " + i.toString())));
        return book;
    }

    public static BookRental makeBookRental() {
        return new BookRental(
                makeBook(),
                makeUser(),
                new GregorianCalendar(2010, 10, 10, 0, 0, 0),
                new GregorianCalendar(2010, 11, 10, 0, 0, 0),
                null);
    }

    public static User makeUser() {
        return new User(
                "firstName",
                "lastName",
                "password",
                Roles.READER,
                "8463",
                new GregorianCalendar(2005, 10, 10, 0, 0, 0),
                "surname",
                "username",
                "email@gmail.com");
    }

    public static FormatSignature makeFormatSignature() {
        return new FormatSignature("abbreviation", "formatSignature");
    }

    public static StockSignature makeStockSignature() {
        return new StockSignature("stock", "stockSignature");
    }

    public static BookSerie makeBookSerie() {
        return new BookSerie("Hunters");
    }

//    public static Filter makeFilter() {
//        return new Filter(2, 10, null);
//    }
    public static ReaderForm makeReaderForm() {
        return new ReaderForm(
                new GregorianCalendar(2010, 10, 10, 0, 0, 0),
                FormStatus.SEND,
                makeUser(),
                new GregorianCalendar(2010, 11, 10, 0, 0, 0)
        );
    }

    static Filter makeFilter() {
        return new Filter(0, 10, makeSearchConfig());
    }

    static SearchConfig makeSearchConfig() {
        return new SearchConfig("albala", "NAME", "NAME");
    }
}
