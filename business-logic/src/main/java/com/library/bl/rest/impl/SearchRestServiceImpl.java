package com.library.bl.rest.impl;

import com.library.bl.vo.exchanger.FilterVoExchanger;
import com.library.dao.DaoRegistry;
import com.library.dao.DaoRegistryFactory;
import com.library.dao.SearchDao;
import com.library.domain.Entity;
import com.library.domain.exchanger.EntityExchanger;
import com.library.domain.filter.Filter;
import com.library.domain.list.EntityList;
import com.library.rest.api.SearchRestService;
import com.library.rest.api.vo.AbstractVo;
import com.library.rest.api.vo.EntityListVo;
import com.library.rest.api.vo.filter.FilterVo;
import java.util.List;
import java.util.function.Function;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author gdimitrova
 * @param <Dao>
 * @param <L>
 * @param <Vo>
 * @param <E>
 * @param <ListVo>
 * @param <X>
 */
public abstract class SearchRestServiceImpl<
        E extends Entity,
        L extends EntityList<E>,
        Dao extends SearchDao< E, L>, 
        Vo extends AbstractVo, 
        ListVo extends EntityListVo<Vo>, 
        X extends EntityExchanger<Vo, E>>
        extends CrudRestServiceImpl<Dao, Vo, E, ListVo,L, X>
        implements SearchRestService< Vo, ListVo> {

    public SearchRestServiceImpl(
            DaoRegistryFactory factory,
            Function<List<Vo>, ListVo> listVoFactory,
            Function<DaoRegistry, Dao> daoGetter,
            X exchanger) {
        super(factory, listVoFactory, daoGetter, exchanger);
    }

    @POST
    @Path("/search")
    @Override
    public Response search(FilterVo filterVo, @Context SecurityContext sc) {
        Filter filter = FilterVoExchanger.INSTANCE.exchange(filterVo);
        return doInTransaction((daoRegistry) -> {
            L list = getDao(daoRegistry).search(filter);
            return buildResponse(exchange(list));
        });
    }
    
    @POST
    @Path("/searchCount")
    @Override
    public Response searchCount(FilterVo filterVo, @Context SecurityContext sc) {
        Filter filter = FilterVoExchanger.INSTANCE.exchange(filterVo);
        return doInTransaction((daoRegistry) -> {
            return buildResponse(getDao(daoRegistry).searchCount(filter));
        });
    }

}
