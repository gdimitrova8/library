package com.library.bl.rest.impl.service;

import com.library.bl.cache.CacheRegistry;
import static com.library.bl.cache.CacheRegistry.ADMIN_PASS;
import static com.library.bl.cache.CacheRegistry.ADMIN_USERNAME;
import com.library.bl.rest.impl.CrudRestServiceImpl;
import com.library.bl.rest.impl.ErrorCode;
import com.library.bl.rest.impl.SearchRestServiceImpl;
import com.library.bl.vo.exchanger.UserVoExchanger;
import com.library.dao.DaoRegistry;
import com.library.dao.DaoRegistryFactory;
import com.library.dao.UserDao;
import com.library.domain.list.UsersList;
import com.library.domain.message.Message;
import com.library.domain.user.User;
import com.library.rest.api.service.UserRestService;
import com.library.rest.api.vo.filter.FilterVo;
import com.library.rest.api.vo.list.UsersListVo;
import com.library.rest.api.vo.user.RolesVo;
import com.library.rest.api.vo.user.UserVo;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import javax.ws.rs.core.Response;

/**
 *
 * @author gdimitrova
 */
public class UserRestServiceImpl extends SearchRestServiceImpl<
        User, 
        UsersList, 
        UserDao, 
        UserVo, 
        UsersListVo, 
        UserVoExchanger> implements UserRestService {

    public UserRestServiceImpl(DaoRegistryFactory factory) {
        super(factory,
                UsersListVo::new,
                DaoRegistry::getUserDao,
                UserVoExchanger.INSTANCE
        );
    }

    @Override
    public Response load(String username) {
        if (username == null) {
            List<Message> errors = new ArrayList<>();
            errors.add(User.MISSING_OR_EMPTY_USERNAME);
            return buildResponse(ErrorCode.VALIDATION, errors);
        }
        ConcurrentHashMap<String, String> map = CacheRegistry.INSTANCE.get(CacheRegistry.ADMIN_INFO);
        if (map.get(ADMIN_USERNAME).equals(username)) {
            UserVo user = new UserVo();
            user.setUserName(username);
            user.setRole(RolesVo.ADMINISTRATOR);
            return Response.ok(user).build();
        }
        return doInTransaction((daoRegistry) -> {
            User loaded = daoRegistry.getUserDao().load(username);
            if (loaded == null) {
                return buildResponse(ErrorCode.NOT_FOUND, new ArrayList<>());
            }
            return Response.ok(exchange(loaded)).build();
        });
    }

    @Override
    public Response searchOperators(FilterVo filter) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Response searchReaders(FilterVo filter) {
        return null;
//        E entity = exchange(vo);
//        List<Message> errors = validate(entity);
//        if (!errors.isEmpty()) {
//            return buildResponse(ErrorCode.VALIDATION, errors);
//        }
//        return doInTransaction((daoRegistry) -> {
//            E saved = getDao(daoRegistry).save(entity);
//            return buildResponse(exchange(saved));
//        });
    }
}
