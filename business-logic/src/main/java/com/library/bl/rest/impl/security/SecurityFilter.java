package com.library.bl.rest.impl.security;

import com.library.bl.cache.CacheRegistry;
import static com.library.bl.cache.CacheRegistry.ADMIN_PASS;
import static com.library.bl.cache.CacheRegistry.ADMIN_USERNAME;
import static com.library.bl.cache.CacheRegistry.USER_TOKENS;
import static com.library.bl.system.support.SystemSupport.DEFAULT_LOCALE;
import com.library.dao.DaoRegistry;
import com.library.dao.DaoRegistryFactory;
import com.library.dao.UserDao;
import com.library.domain.message.Message;
import com.library.domain.user.Roles;
import com.library.domain.user.User;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author gdimitrova
 */
@Provider
public class SecurityFilter implements ContainerRequestFilter {

    private final static Logger LOGGER = Logger.getLogger(SecurityFilter.class.getName());

    private final static Message UNAUTHORIZED = new Message("unauthorized.access");

    @Context
    private ResourceInfo resourceInfo;

    private final DaoRegistryFactory factory;

    private final String adminUsername;

    private final String adminPassword;

    protected static final String AUTHORIZATION_PROPERTY = "Authorization";

    // private static final String AUTHENTICATION_SCHEME = UserSecurityContext.BASIC_AUTH;
    private static final String AUTHENTICATION_SCHEME = UserSecurityContext.AUTH_SCHEME;

    public SecurityFilter(DaoRegistryFactory factory, String adminUsername, String adminPassword) {
        this.factory = factory;
        this.adminUsername = adminUsername;
        this.adminPassword = adminPassword;
        ConcurrentHashMap<String, String> map = CacheRegistry.INSTANCE.get(CacheRegistry.ADMIN_INFO);
        map.put(ADMIN_USERNAME, this.adminUsername);
        map.put(ADMIN_PASS, this.adminPassword);
    }

    @Override
    public void filter(ContainerRequestContext reqCtx) throws IOException {

        Method method = resourceInfo.getResourceMethod();

        if (method.isAnnotationPresent(PermitAll.class)) {
            return;
        }

        if (method.isAnnotationPresent(DenyAll.class)) {
            reqCtx.abortWith(Response.status(Response.Status.FORBIDDEN)
                    .entity("Access blocked for all users !!").build());
            return;
        }

        String authorization = reqCtx.getHeaderString(AUTHORIZATION_PROPERTY);
        LOGGER.log(Level.ALL, authorization);

        if (authorization == null || authorization.isEmpty() || authorization.isBlank()) {
            unauhtorized(reqCtx);
            return;
        }
        String usernameAndPassword = authorization.replaceFirst(AUTHENTICATION_SCHEME + " ", "");
        usernameAndPassword = usernameAndPassword.replace("[", "");
        usernameAndPassword = usernameAndPassword.replace("]", "");
        String[] strBytes = usernameAndPassword.split(", ");
        List<Byte> bytes = new ArrayList<>();
        for (String strByte : strBytes) {
            bytes.add(Byte.valueOf(strByte));
        }
        byte[] b = new byte[bytes.size()];
        for (int i = 0; i < strBytes.length; i++) {
            b[i] = bytes.get(i);
        }
        byte[] decoded = Base64.decodeBase64(b);

        final StringTokenizer tokenizer = new StringTokenizer(new String(decoded), ":");
        final String username = tokenizer.nextToken();
        final String password = tokenizer.nextToken();
        if (isAdmin(adminUsername, adminPassword)) {
            setSecurityContext(reqCtx, Roles.ADMINISTRATOR);
            return;
        }

        if (method.isAnnotationPresent(RolesAllowed.class)) {
            RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
            Set<String> rolesSet = new HashSet<>(Arrays.asList(rolesAnnotation.value()));
            User loaded;
            try (DaoRegistry daoRegistry = factory.makeDaoRegistry()) {
                daoRegistry.beginTransaction();
                loaded = daoRegistry.getUserDao().load(username, password);
                daoRegistry.commitTransaction();

            } catch (Exception e) {
                reqCtx.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                        .entity(e.getMessage()).build());
                return;
            }
            if (!rolesSet.contains(loaded.getRole().name())) {
                unauhtorized(reqCtx);
            } else {
                setSecurityContext(reqCtx, loaded.getRole());
            }
        }
    }

    private void setSecurityContext(ContainerRequestContext reqCtx, Roles role) {
        final SecurityContext sc = reqCtx.getSecurityContext();
        Locale language = reqCtx.getLanguage();
        language = language != null ? language : DEFAULT_LOCALE;
        String token = UUID.randomUUID().toString();
        Map<String, UserToken> map = CacheRegistry.INSTANCE.get(USER_TOKENS);
        UserToken userToken = new UserToken(token, role, language);
        map.put(token, userToken);
        reqCtx.setSecurityContext(new UserSecurityContext(userToken, sc));
    }

    private boolean isLogin(ContainerRequestContext reqCtx) {

        Method method = resourceInfo.getResourceMethod();
        if (!method.getName().equals("login")) {
            return false;
        }
        UriInfo uriInfo = reqCtx.getUriInfo();
        String username = getParam(uriInfo, "username");
        String password = getParam(uriInfo, "password");
        // ValidationUtils.validate(password, UNAUTHORIZED, messages);
        if (isAdmin(username, password)) {
            //throw new NotAuthorizedException(LOGGER, moreChallenges);
        }
        final String token = UUID.randomUUID().toString();
        if (isAdmin(username, password)) {
            //           reqCtx.setSecurityContext(new UserSecurityContext(token, Roles.ADMINISTRATOR, Locale.ITALY, sc));
            //return;
        }

        final SecurityContext sc = reqCtx.getSecurityContext();
        reqCtx.setSecurityContext(new UserSecurityContext(token, Roles.ADMINISTRATOR, Locale.ITALY, sc));
        return true;
    }

    private String getParam(UriInfo uriInfo, String paramName) {
        String value = getParamValue(uriInfo.getPathParameters(), paramName);
        if (value != null) {
            return value;
        }
        return getParamValue(uriInfo.getQueryParameters(), paramName);
    }

    private String getParamValue(MultivaluedMap<String, String> params, String paramName) {
        List<String> values = params.get(paramName);
        if (!values.isEmpty()) {
            return values.get(0);
        }
        return null;
    }

    private User loadUser(ContainerRequestContext reqCtx, Function<UserDao, User> userLoader) {
        User loaded = null;
        try (DaoRegistry daoRegistry = factory.makeDaoRegistry()) {
            daoRegistry.beginTransaction();
            loaded = userLoader.apply(daoRegistry.getUserDao());
            daoRegistry.commitTransaction();
        } catch (Exception e) {
            reqCtx.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                    .entity(e.getMessage()).build());
        }
        return loaded;
    }

    private boolean isAdmin(String username, String password) {
        return username.equals(adminUsername) && password.equals(adminPassword);
    }

    private void unauhtorized(ContainerRequestContext reqCtx) {
        reqCtx.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                .entity("You cannot access this resource").build());
    }

}
