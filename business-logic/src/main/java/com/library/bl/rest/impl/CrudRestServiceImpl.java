package com.library.bl.rest.impl;

import com.library.dao.CrudDao;
import com.library.dao.DaoRegistry;
import com.library.dao.DaoRegistryFactory;
import com.library.domain.Entity;
import com.library.domain.exchanger.EntityExchanger;
import com.library.domain.list.EntityList;
import com.library.domain.message.Message;
import com.library.rest.api.CrudRestService;
import com.library.rest.api.vo.AbstractVo;
import com.library.rest.api.vo.EntityListVo;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author gdimitrova
 * @param <Dao>
 * @param <Vo>
 * @param <E>
 * @param <ListVo>
 * @param <X>
 */
public abstract class CrudRestServiceImpl<
        Dao extends CrudDao<E>, 
        Vo extends AbstractVo, 
        E extends Entity, 
        ListVo extends EntityListVo<Vo>,
        L extends EntityList<E>,
        X extends EntityExchanger<Vo, E>>
        extends AbstractRestService<Dao, Vo, E, ListVo,L, X>
        implements CrudRestService<Vo, ListVo> {


    public CrudRestServiceImpl(
            DaoRegistryFactory factory,
            Function< List<Vo>, ListVo> listVoFactory,
            Function<DaoRegistry, Dao> daoGetter,
            X exchanger) {
        super(factory, exchanger,listVoFactory, daoGetter);
    }

    @POST
    @RolesAllowed({"ADMINISTRATOR", "OPERATOR"})
    @Path("/save")
    @Override
    public Response save(Vo vo, @Context SecurityContext sc) {
        E entity = exchange(vo);
        List<Message> errors = validate(entity);
        if (!errors.isEmpty()) {
            return buildResponse(ErrorCode.VALIDATION, errors, sc);
        }
        return doInTransaction((daoRegistry) -> {
            E saved = getDao(daoRegistry).save(entity);
            return buildResponse(exchange(saved));
        });
    }

    @Override
    public Response update(Vo vo) {
        E entity = exchange(vo);
        List<Message> errors = validate(entity);
        if (!errors.isEmpty()) {
            return buildResponse(ErrorCode.VALIDATION, errors);
        }
        return doInTransaction((daoRegistry) -> {
            getDao(daoRegistry).update(entity);
            return buildResponse();
        });
    }

    @Override
    public Response loadById(String id) {
        if (id == null) {
            List<Message> errors = new ArrayList<>();
            errors.add(MISSING_ID);
            return buildResponse(ErrorCode.VALIDATION, errors);
        }
        return doInTransaction((daoRegistry) -> {
            E loaded = getDao(daoRegistry).loadById(id);
            if (loaded == null) {
                return buildResponse(ErrorCode.NOT_FOUND, new ArrayList<>());
            }
            return buildResponse(exchange(loaded));
        });
    }

    @Override
    public Response loadAll() {
        return doInTransaction((daoRegistry) -> {
            List<E> entities = getDao(daoRegistry).loadAll();
            return buildResponse(makeListVo(exchangeAll(entities)));
        });
    }

    @Override
    public Response delete(String id) {
        if (id == null) {
            List<Message> errors = new ArrayList<>();
            errors.add(MISSING_ID);
            return buildResponse(ErrorCode.VALIDATION, errors);
        }
        return doInTransaction((daoRegistry) -> {
            getDao(daoRegistry).delete(id);
            return buildResponse();
        });
    }

    @Override
    public Response saveAll(ListVo listVo) {
        List<E> list = exchange(listVo.getEntities());
        List<Message> errors = validate(list);
        if (!errors.isEmpty()) {
            return buildResponse(ErrorCode.VALIDATION, errors);
        }
        return doInTransaction((daoRegistry) -> {
            getDao(daoRegistry).saveAll(list);
            return buildResponse();
        });
    }

    @Override
    public Response deleteAll(ListVo listVo) {
        List<Vo> vos = listVo.getEntities();
        List<E> list = exchange(vos);
        List<Message> errors = validate(list);
        List<Vo> entities = vos.stream().filter((e) -> {
            return e.getId() == null;
        }).collect(Collectors.toList());
//        if (!entities.isEmpty()) {
//            errors.add("The list contains entities with null id.");
//        }
        if (!errors.isEmpty()) {
            return buildResponse(ErrorCode.VALIDATION, errors);
        }
        return doInTransaction((daoRegistry) -> {
            getDao(daoRegistry).deleteAll(list);
            return buildResponse();
        });
    }


}
