package com.library.bl.rest.impl;

import com.library.bl.i18n.I18n;
import com.library.dao.CrudDao;
import com.library.dao.DaoRegistry;
import com.library.dao.DaoRegistryFactory;
import com.library.domain.Entity;
import com.library.domain.exchanger.EntityExchanger;
import com.library.domain.list.EntityList;
import com.library.domain.message.Message;
import com.library.rest.api.response.SuccessResponse;
import com.library.rest.api.vo.AbstractVo;
import com.library.rest.api.vo.EntityListVo;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author gdimitrova
 * @param <Dao>
 * @param <Vo>
 * @param <E>
 * @param <ListVo>
 * @param <X>
 */
public abstract class AbstractRestService<
        Dao extends CrudDao<E>, Vo extends AbstractVo, E extends Entity, ListVo extends EntityListVo, L extends EntityList<E>, X extends EntityExchanger<Vo, E>> {

//    @Context
//    private SecurityContext sc;
    private final static Logger LOGGER = Logger.getLogger(AbstractRestService.class.getName());

    protected final static Message MISSING_ID = new Message("missing.id");

    protected final DaoRegistryFactory factory;

    private final X exchanger;

    private final Function< List<Vo>, ListVo> listVoFactory;

    private final Function<DaoRegistry, Dao> daoGetter;

    public AbstractRestService(DaoRegistryFactory factory, X exchanger,
            Function< List<Vo>, ListVo> listVoFactory, Function<DaoRegistry, Dao> daoGetter) {
        this.factory = factory;
        this.exchanger = exchanger;
        this.daoGetter = daoGetter;
        this.listVoFactory = listVoFactory;
    }

    protected synchronized final Response doInTransaction(Function<DaoRegistry, Response> work) {
        try (DaoRegistry daoRegistry = factory.makeDaoRegistry();) {
            daoRegistry.beginTransaction();
            Response resp = work.apply(daoRegistry);
            daoRegistry.commitTransaction();
            return resp;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
            List<Message> errors = new ArrayList<>();
            // errors.add(ex.getMessage());
            return buildResponse(ErrorCode.INTERNAL_ERROR, errors);
        }
    }

    protected synchronized final Response doInTransaction(Function<DaoRegistry, Response> work, @Context SecurityContext sc) {
        try (DaoRegistry daoRegistry = factory.makeDaoRegistry();) {
            daoRegistry.beginTransaction();
            Response resp = work.apply(daoRegistry);
            daoRegistry.commitTransaction();
            return resp;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
            List<Message> errors = new ArrayList<>();
            // errors.add(ex.getMessage());
            return buildResponse(ErrorCode.INTERNAL_ERROR, errors, sc);
        }
    }

    protected Response buildResponse(ErrorCode code, List<Message> errors) {
        switch (code) {
            case INTERNAL_ERROR:
                return buildResponse(Status.INTERNAL_SERVER_ERROR, errors);

            case NOT_FOUND:
                return buildResponse(Status.NOT_FOUND, errors);

            case VALIDATION:
                return buildResponse(Status.BAD_REQUEST, errors);
        }
        return buildResponse();
    }

    protected Response buildResponse(ErrorCode code, List<Message> errors, @Context SecurityContext sc) {
        switch (code) {
            case INTERNAL_ERROR:
                return buildResponse(Status.INTERNAL_SERVER_ERROR, errors, sc);

            case NOT_FOUND:
                return buildResponse(Status.NOT_FOUND, errors, sc);

            case VALIDATION:
                return buildResponse(Status.BAD_REQUEST, errors, sc);
        }
        return buildResponse();
    }

    protected Response buildResponse(Status code, List<Message> errors) {
        // return Response.status(code).entity(I18n.translate(errors, sc)).build();
        return Response.status(code).entity(errors).build();
    }

    protected Response buildResponse(Status code, List<Message> errors, @Context SecurityContext sc) {
        return Response.status(code).entity(I18n.translate(errors, sc)).build();
    }

    protected Response buildResponse() {
        return buildResponse(new SuccessResponse());
    }

    protected Response buildResponse(Object entity) {
        return Response.ok(entity).build();
    }

    protected List<Message> validate(List<E> entities) {
        return entities.stream().flatMap(e -> validate(e).stream()).collect(Collectors.toList());
    }

    protected List<Message> validate(E entity) {
        return entity.validate();
    }

    protected List<E> exchange(List<Vo> v) {
        return v.stream().map(vo -> exchange(vo)).collect(Collectors.toList());
    }

    protected List<Vo> exchangeAll(List<E> entities) {
        return entities.stream().map(e -> exchange(e)).collect(Collectors.toList());
    }

    protected E exchange(Vo v) {
        return exchanger.exchange(v);
    }

    protected Vo exchange(E e) {
        return exchanger.exchange(e);
    }

    protected Dao getDao(DaoRegistry registry) {
        return daoGetter.apply(registry);
    }

    protected < E extends Entity, Dao extends CrudDao<E>>
            void save(Consumer<E> setter, Supplier<E> getter, Function<DaoRegistry, Dao> daoGetter, DaoRegistry registry) {
        E entity = getter.get();
        if (entity != null) {
            setter.accept(daoGetter.apply(registry).save(entity));
            registry.commitTransaction();
            registry.beginTransaction();
        }
    }

    protected < E extends Entity, Dao extends CrudDao<E>>
            void saveAll(Consumer<List<E>> setter, Supplier<List<E>> getter, Function<DaoRegistry, Dao> daoGetter, DaoRegistry registry) {
        List<E> e = getter.get();
        if (e != null) {
            setter.accept(daoGetter.apply(registry).saveAll(e));
            registry.commitTransaction();
            registry.beginTransaction();
        }
    }

    protected ListVo exchange(L list) {
        return listVoFactory.apply(exchangeAll(list.getEntities()));
    }

    protected ListVo makeListVo(List<Vo> entities) {
        return listVoFactory.apply(entities);
    }

}
