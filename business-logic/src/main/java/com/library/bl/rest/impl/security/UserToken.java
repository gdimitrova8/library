package com.library.bl.rest.impl.security;

import com.library.domain.user.Roles;
import java.util.Calendar;
import java.util.Locale;

public class UserToken {

    private final String token;

    private final Roles role;

    private final Locale locale;

    private final Long expiredAt;

    public UserToken(String token, Roles role, Locale locale) {
        this.token = token;
        this.role = role;
        this.locale = locale;
        this.expiredAt = Calendar.getInstance().getTimeInMillis() + Long.valueOf(30 * 60 * 1000);
    }

    public String getToken() {
        return token;
    }

    public Roles getRole() {
        return role;
    }

    public Locale getLocale() {
        return locale;
    }

    public Long getExpiredAt() {
        return expiredAt;
    }

}
