package com.library.bl.rest.impl.security;

import com.library.bl.cache.CacheRegistry;
import static com.library.bl.cache.CacheRegistry.USER_TOKENS;
import java.util.Map;

public class TokenUtils {

    public static Map<String, UserToken> getTokens() {
        return CacheRegistry.INSTANCE.get(USER_TOKENS);
    }

    public static UserToken getToken(String token) {
        return getTokens().get(token);
    }
}
