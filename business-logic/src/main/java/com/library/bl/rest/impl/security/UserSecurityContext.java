package com.library.bl.rest.impl.security;

import com.library.domain.user.Roles;
import java.security.Principal;
import java.util.Locale;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author gdimitrova
 */
public class UserSecurityContext implements SecurityContext {

    public final static String AUTH_SCHEME = "Library-Token-Based-Auth-Scheme";

    private final UserPrincipal principal;

    private final UserToken userToken;

    private final boolean isSecure;

    public UserSecurityContext(String token, Roles role, Locale locale, SecurityContext sc) {
        this(new UserToken(token, role, locale), sc.isSecure());
    }

    public UserSecurityContext(String token, Roles role, Locale locale, boolean isSecure) {
        this(new UserToken(token, role, locale), isSecure);
    }

    public UserSecurityContext(UserToken userToken, SecurityContext sc) {
        this(userToken, sc.isSecure());
    }

    public UserSecurityContext(UserToken userToken, boolean isSecure) {
        this.principal = new UserPrincipal(userToken);
        this.userToken = userToken;
        this.isSecure = isSecure;
    }

    @Override
    public Principal getUserPrincipal() {
        return principal;
    }

    @Override
    public boolean isUserInRole(String role) {
        return userToken.getRole().name().contains(role);
    }

    @Override
    public boolean isSecure() {
        return isSecure;
    }

    @Override
    public String getAuthenticationScheme() {
        return AUTH_SCHEME;
    }

    private class UserPrincipal implements Principal {

        private final UserToken userToken;

        public UserPrincipal(UserToken userToken) {
            this.userToken = userToken;
        }

        @Override
        public String getName() {
            return userToken.getToken();
        }

    }
}
