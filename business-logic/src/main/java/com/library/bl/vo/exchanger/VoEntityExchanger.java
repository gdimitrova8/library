package com.library.bl.vo.exchanger;

import com.library.domain.Entity;
import com.library.domain.exchanger.AbstractEntityExchanger;
import com.library.rest.api.vo.AbstractVo;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 *
 * @author gdimitrova
 * @param <Vo>
 * @param <E>
 */
public abstract class VoEntityExchanger<Vo extends AbstractVo, E extends Entity>
        extends AbstractEntityExchanger<Vo, E> {

    protected VoExchangerRegistry registry = new VoExchangerRegistry();

    @Override
    public E exchangeId(Vo from, E to) {
        to.setId(from.getId());
        return to;
    }

    @Override
    public Vo exchangeId(E from, Vo to) {
        to.setId(from.getId());
        return to;
    }

    protected <V extends AbstractVo, F extends Entity, X extends VoEntityExchanger<V, F>>
            void exchange(Supplier<X> valueGetter, Consumer<V> valueSetter, F from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchange(from));
    }

    protected <V extends AbstractVo, F extends Entity, X extends VoEntityExchanger<V, F>>
            void exchange(Supplier<X> valueGetter, Consumer<F> valueSetter, V from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchange(from));
    }

    protected <V extends AbstractVo, F extends Entity, X extends VoEntityExchanger<V, F>>
            void exchangeAll(Supplier<X> valueGetter, Consumer<List<V>> valueSetter, List<F> from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchangeAllFrom(from));
    }

    protected <V extends AbstractVo, F extends Entity, X extends VoEntityExchanger<V, F>>
            void exchange(Supplier<X> valueGetter, Consumer<List<F>> valueSetter, List<V> from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchangeAll(from));
    }

}
