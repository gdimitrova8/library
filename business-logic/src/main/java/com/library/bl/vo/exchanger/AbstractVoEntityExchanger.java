package com.library.bl.vo.exchanger;

import com.library.domain.Entity;
import com.library.domain.exchanger.ValidatableExchanger;
import com.library.domain.validation.Validatable;
import java.io.Serializable;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 *
 * @author gdimitrova
 * @param <Vo>
 * @param <E>
 */
public abstract class AbstractVoEntityExchanger<Vo extends Serializable, E extends Validatable>
        extends ValidatableExchanger<Vo, E> {

    protected VoExchangerRegistry registry = new VoExchangerRegistry();

    protected <V extends Serializable, F extends Validatable, X extends AbstractVoEntityExchanger<V, F>>
            void exchange(Supplier<X> valueGetter, Consumer<V> valueSetter, F from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchange(from));
    }

    protected <V extends Serializable, F extends Validatable, X extends AbstractVoEntityExchanger<V, F>>
            void exchange(Supplier<X> valueGetter, Consumer<F> valueSetter, V from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchange(from));
    }

    protected <V extends Serializable, F extends Validatable, X extends AbstractVoEntityExchanger<V, F>>
            void exchangeAll(Supplier<X> valueGetter, Consumer<List<V>> valueSetter, List<F> from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchangeAllFrom(from));
    }

    protected <V extends Serializable, F extends Validatable, X extends AbstractVoEntityExchanger<V, F>>
            void exchange(Supplier<X> valueGetter, Consumer<List<F>> valueSetter, List<V> from) {
        X exchanger = valueGetter.get();
        valueSetter.accept(exchanger.exchangeAll(from));
    }

}
