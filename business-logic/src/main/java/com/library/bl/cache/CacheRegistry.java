package com.library.bl.cache;

import com.library.bl.rest.impl.security.UserToken;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author gdimitrova
 */
public class CacheRegistry {

    public final static CacheRegistry INSTANCE = new CacheRegistry();

    public final static String USER_TOKENS = "user.tokens";

    public final static String ADMIN_INFO = "admin.info";

    public final static String ADMIN_USERNAME = "admin.username";

    public final static String ADMIN_PASS = "admin.pass";

    private final Map<String, Object> cache = new ConcurrentHashMap<>() {
        {
            put(USER_TOKENS, new ConcurrentHashMap<String, UserToken>());
            put(ADMIN_INFO, new ConcurrentHashMap<String, String>() {
                {
                    put(ADMIN_USERNAME, "");
                    put(ADMIN_PASS, "");
                }
            });
        }
    };

    private CacheRegistry() {
        //singleton
    }

    public <T> void add(String cacheRegion, T o) {
        cache.putIfAbsent(cacheRegion, o);
    }

    public <T> T get(String cacheRegion) {
        return (T) cache.get(cacheRegion);
    }

}
