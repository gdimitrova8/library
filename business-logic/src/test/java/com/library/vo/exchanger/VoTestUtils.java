package com.library.vo.exchanger;

import com.library.domain.Entity;
import com.library.domain.NamedEntity;
import com.library.domain.book.Author;
import com.library.domain.book.Book;
import com.library.domain.book.BookRental;
import com.library.domain.book.signature.Signature;
import com.library.domain.user.User;
import com.library.rest.api.vo.AbstractVo;
import com.library.rest.api.vo.NamedEntityVo;
import com.library.rest.api.vo.book.AuthorVo;
import com.library.rest.api.vo.book.BookRentalVo;
import com.library.rest.api.vo.book.BookVo;
import com.library.rest.api.vo.book.CharacteristicVo;
import com.library.rest.api.vo.book.GenreVo;
import com.library.rest.api.vo.book.signature.SignatureVo;
import com.library.rest.api.vo.user.UserVo;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.function.Function;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author gdimitrova
 */
public class VoTestUtils {

    public static void assertVo(BookRental e, BookRentalVo t) {
        assertVo(e.getBook(), t.getBook());
        assertVo(e.getUser(), t.getUser());
        assertDates(e.getReceivableDate(), t.getReceivableDate());
        assertDates(e.getReturnDate(), t.getReturnDate());
        assertDates(e.getReturnDeadLine(), t.getReturnDeadLine());
    }

    public static void assertVo(Book e, BookVo vo) {
        assertEntities(e, vo);
        if (e == null || vo == null) {
            return;
        }
        assertIds(e, vo);
        assertEquals(e.getTitle(), vo.getTitle(), "\nNot equal titles.\n");
        assertEquals(e.getSignature(), vo.getSignature(), "\nNot equal signatures.\n");
        assertEquals(e.getInventoryNumber(), vo.getInventoryNumber(), "\nNot equal inventory numbers.\n");
        assertEquals(e.getIsbn(), vo.getIsbn(), "\nNot equal ISBN.\n");
        assertNamedEntities(e.getPublisher(), vo.getPublisher());
        assertEquals(e.getPublishYear(), vo.getPublishYear(), "\nNot equal publish years.\n");
        assertNamedEntities(e.getForm(), vo.getForm());
        assertLists(e.getAuthors(), vo.getAuthors());
        assertNamedEntities(e.getSerie(), vo.getSerie());
        assertSignatures(e.getStockSignature(), vo.getStockSignature());
        assertSignatures(e.getFormatSignature(), vo.getFormatSignature());
        List<GenreVo> genres = new ArrayList<>();
        genres.addAll(vo.getGenres());
        assertLists(e.getGenres(), genres);
        List<CharacteristicVo> characteristics = new ArrayList<>();
        characteristics.addAll(vo.getCharacteristics());
        assertLists(e.getCharacteristics(), characteristics);
    }

    public static void assertVo(User e, UserVo vo) {
        assertEntities(e, vo);
        if (e == null || vo == null) {
            return;
        }
        assertIds(e, vo);
        assertEquals(e.getUserName(), vo.getUserName(), "\nNot equal username.\n");
        assertEquals(e.getPassword(), vo.getPassword(), "\nNot equal passwords.\n");
        assertEquals(e.getEmail(), vo.getEmail(), "\nNot equal email.\n");
        assertEquals(e.getFirstName(), vo.getFirstName(), "\nNot equal first name.\n");
        assertEquals(e.getSurname(), vo.getSurname(), "\nNot equal surname.\n");
        assertEquals(e.getLastName(), vo.getLastName(), "\nNot equal last name.\n");
        assertEquals(e.getPhoneNumber(), vo.getPhoneNumber(), "\nNot equal phone number.\n");
        assertEquals(e.getRole().name(), vo.getRole().name(), "\nNot equal user role.\n");
        assertDates(e.getCreatedDate(), vo.getCreatedDate(), "\nNot equal created date.\n");

    }

    public static void assertDates(Calendar c1, Long c2, String msg) {
        if (c1 != null && c2 != null) {
            assertEquals(c1.getTimeInMillis(), c2, msg);
            return;
        }
        assertTrue((c1 == null && c2 == null) || (c1 != null && c1.getTimeInMillis() != c2), msg);
    }

    public static void assertDates(Calendar c1, Long c2) {
        assertDates(c1, c2, "\nNot equal dates.\n");
    }

    public static void assertVo(Author e, AuthorVo vo) {
        assertEntities(e, vo);
        if (e == null || vo == null) {
            return;
        }
        assertNamedEntities(e, vo);
        assertEquals(e.getBiography(), vo.getBiography(), "\nNot equal biographies.\n");
        assertEquals(e.getBirthPlace(), vo.getBirthPlace(), "\nNot equal birth places.\n");
        assertEquals(e.getBirthDate(), vo.getBirthDate(), "\nNot equal birth dates.\n");
    }

    public static <E extends Entity, Vo extends AbstractVo> void assertIds(E e, Vo vo) {
        assertEquals(e.getId(), vo.getId(), "\nNot equal id.\n");
    }

    public static <E extends Entity, Vo extends AbstractVo> void assertEntities(E e, Vo vo) {
        assertTrue((e == null && vo == null) || (e != null && vo != null),
                "\nThere is null entity.\n");
    }

    public static <E extends NamedEntity, Vo extends NamedEntityVo> void assertNamedEntities(E e, Vo vo) {
        assertEntities(e, vo);
        if (e == null || vo == null) {
            return;
        }
        assertIds(e, vo);
        assertEquals(e.getName(), vo.getName(), "\nNot equal names.\n");
    }

    public static <E extends NamedEntity, Vo extends NamedEntityVo> void assertLists(List<E> e, List<Vo> vo) {
        e.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
        vo.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
        for (int i = 0; i < e.size(); i++) {
            assertNamedEntities(e.get(i), vo.get(i));
        }
    }

    public static <E extends Signature, Vo extends SignatureVo> void assertSignatures(E e, Vo vo) {
        assertEntities(e, vo);
        if (e == null || vo == null) {
            return;
        }
        assertNamedEntities(e, vo);
        assertEquals(e.getAbbreviation(), vo.getAbbreviation(), "\nNot equal abbreviation.\n");
    }

    public static <T> List<T> fillList(Integer count, Function<Integer, T> creator) {
        List<T> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            list.add(creator.apply(i));
        }
        return list;
    }
}
